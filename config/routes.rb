Btc1k::Application.routes.draw do

  get '/status', to: 'status#index', as: 'status_index'
  get '/status/:btc_address', to: 'status#show', as: 'status_request'
  post '/verify', to: 'status#verify', as: 'status_verify'

  resources :payment_requests, only: [:create, :new]

  get '/how_to', to: 'payment_requests#how_to', as: 'how_to'
  get '/imprint', to: 'payment_requests#impress', as: 'impress'
  root 'payment_requests#how_to'
end
