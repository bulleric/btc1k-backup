# application configuration

Btc1k::Application.configure do
  config.bits_of_proof_username = ENV["BITS_OF_PROOF_USERNAME"]
  config.bits_of_proof_pass = ENV["BITS_OF_PROOF_PASS"]
end
