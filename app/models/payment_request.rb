class PaymentRequest < ActiveRecord::Base
  validates :btc_address, bitcoin_address: true



  def self.get_payment_request_struct(payment_id)
    bop_api_call = BitsOfProof::Payment.get_payment_request(payment_id)
    request_struct = Struct.new(:id, :created_at, :address, :confirmations,
                                :timeout, :paid, :state, :provision_address,
                                :title, :payment_qr_link)


    request_struct.new(bop_api_call["id"], bop_api_call["createdAt"], bop_api_call["address"],
                       bop_api_call["confirmations"], bop_api_call["timeout"], 
                       bop_api_call["paid"], bop_api_call["state"], bop_api_call["provisionAddress"], 
                       bop_api_call["title"], bop_api_call["_links"]["bip72"]["href"])

  end
end
