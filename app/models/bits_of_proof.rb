require 'rest_client'
module BitsOfProof

  class Payment
    @username= Btc1k::Application.config.bits_of_proof_username
    @pass= Btc1k::Application.config.bits_of_proof_pass
    API_BASE_URI = 'https://api.bitsofproof.com/mbs/1/'

    def self.init(bitcoin_address)
      RestClient.log = '/tmp/restclient.log'
      uri = 'paymentRequest'

      params = {
        amount: 100000000,
        confirmations: 1,
        timeout: "P2D",
        title: bitcoin_address,
        callbackUrl: "http://api.bitsofproof.com:8280/btc1k/payment"
      }.to_json

      begin
        resource = RestClient::Resource.new(API_BASE_URI + uri, user: @username, password: @pass)
        response = JSON.load(resource.post params, content_type: :json, accept: :json, user_agent: 'btc1k')
      rescue  => e
        e.response
      end
    end

    def self.get_payment_requests
      uri = "/paymentRequest"

      begin
        resource = RestClient::Resource.new(API_BASE_URI + uri , user: @username, password: @pass)
        response = JSON.load(resource.get)
      rescue  => e
        e.response
      end
    end

    def self.get_payment_request(payment_id)
      uri = "/paymentRequest"

      begin
        resource = RestClient::Resource.new(API_BASE_URI + uri , user: @username, password: @pass)
        response = JSON.load(resource[payment_id].get)
      rescue  => e
        e.response
      end
    end
  end
end
