var ninja = { wallets: {} };

ninja.privateKey = {
	isPrivateKey: function (key) {
		return (
					Bitcoin.ECKey.isWalletImportFormat(key) ||
					Bitcoin.ECKey.isCompressedWalletImportFormat(key) ||
					Bitcoin.ECKey.isHexFormat(key) ||
					Bitcoin.ECKey.isBase64Format(key) ||
					Bitcoin.ECKey.isMiniFormat(key)
				);
	},
	getECKeyFromAdding: function (privKey1, privKey2) {
		var n = EllipticCurve.getSECCurveByName("secp256k1").getN();
		var ecKey1 = new Bitcoin.ECKey(privKey1);
		var ecKey2 = new Bitcoin.ECKey(privKey2);
		// if both keys are the same return null
		if (ecKey1.getBitcoinHexFormat() == ecKey2.getBitcoinHexFormat()) return null;
		if (ecKey1 == null || ecKey2 == null) return null;
		var combinedPrivateKey = new Bitcoin.ECKey(ecKey1.priv.add(ecKey2.priv).mod(n));
		// compressed when both keys are compressed
		if (ecKey1.compressed && ecKey2.compressed) combinedPrivateKey.setCompressed(true);
		return combinedPrivateKey;
	},
	getECKeyFromMultiplying: function (privKey1, privKey2) {
		var n = EllipticCurve.getSECCurveByName("secp256k1").getN();
		var ecKey1 = new Bitcoin.ECKey(privKey1);
		var ecKey2 = new Bitcoin.ECKey(privKey2);
		// if both keys are the same return null
		if (ecKey1.getBitcoinHexFormat() == ecKey2.getBitcoinHexFormat()) return null;
		if (ecKey1 == null || ecKey2 == null) return null;
		var combinedPrivateKey = new Bitcoin.ECKey(ecKey1.priv.multiply(ecKey2.priv).mod(n));
		// compressed when both keys are compressed
		if (ecKey1.compressed && ecKey2.compressed) combinedPrivateKey.setCompressed(true);
		return combinedPrivateKey;
	},
	// 58 base58 characters starting with 6P
	isBIP38Format: function (key) {
		key = key.toString();
		return (/^6P[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{56}$/.test(key));
	},
	BIP38EncryptedKeyToByteArrayAsync: function (base58Encrypted, passphrase, callback) {
		var hex;
		try {
			hex = Bitcoin.Base58.decode(base58Encrypted);
		} catch (e) {
			callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
			return;
		}

		// 43 bytes: 2 bytes prefix, 37 bytes payload, 4 bytes checksum
		if (hex.length != 43) {
			callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
			return;
		}
		// first byte is always 0x01 
		else if (hex[0] != 0x01) {
			callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
			return;
		}

		var expChecksum = hex.slice(-4);
		hex = hex.slice(0, -4);
		var checksum = Bitcoin.Util.dsha256(hex);
		if (checksum[0] != expChecksum[0] || checksum[1] != expChecksum[1] || checksum[2] != expChecksum[2] || checksum[3] != expChecksum[3]) {
			callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
			return;
		}

		var isCompPoint = false;
		var isECMult = false;
		var hasLotSeq = false;
		// second byte for non-EC-multiplied key
		if (hex[1] == 0x42) {
			// key should use compression
			if (hex[2] == 0xe0) {
				isCompPoint = true;
			}
			// key should NOT use compression
			else if (hex[2] != 0xc0) {
				callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
				return;
			}
		}
		// second byte for EC-multiplied key 
		else if (hex[1] == 0x43) {
			isECMult = true;
			isCompPoint = (hex[2] & 0x20) != 0;
			hasLotSeq = (hex[2] & 0x04) != 0;
			if ((hex[2] & 0x24) != hex[2]) {
				callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
				return;
			}
		}
		else {
			callback(new Error(ninja.translator.get("detailalertnotvalidprivatekey")));
			return;
		}

		var decrypted;
		var AES_opts = { mode: new Crypto.mode.ECB(Crypto.pad.NoPadding), asBytes: true };

		var verifyHashAndReturn = function () {
			var tmpkey = new Bitcoin.ECKey(decrypted); // decrypted using closure
			var base58AddrText = tmpkey.setCompressed(isCompPoint).getBitcoinAddress(); // isCompPoint using closure
			checksum = Bitcoin.Util.dsha256(base58AddrText); // checksum using closure

			if (checksum[0] != hex[3] || checksum[1] != hex[4] || checksum[2] != hex[5] || checksum[3] != hex[6]) {
				callback(new Error(ninja.translator.get("bip38alertincorrectpassphrase"))); // callback using closure
				return;
			}
			callback(tmpkey.getBitcoinPrivateKeyByteArray()); // callback using closure
		};

		if (!isECMult) {
			var addresshash = hex.slice(3, 7);
			Crypto_scrypt(passphrase, addresshash, 16384, 8, 8, 64, function (derivedBytes) {
				var k = derivedBytes.slice(32, 32 + 32);
				decrypted = Crypto.AES.decrypt(hex.slice(7, 7 + 32), k, AES_opts);
				for (var x = 0; x < 32; x++) decrypted[x] ^= derivedBytes[x];
				verifyHashAndReturn(); //TODO: pass in 'decrypted' as a param
			});
		}
		else {
			var ownerentropy = hex.slice(7, 7 + 8);
			var ownersalt = !hasLotSeq ? ownerentropy : ownerentropy.slice(0, 4);
			Crypto_scrypt(passphrase, ownersalt, 16384, 8, 8, 32, function (prefactorA) {
				var passfactor;
				if (!hasLotSeq) { // hasLotSeq using closure
					passfactor = prefactorA;
				} else {
					var prefactorB = prefactorA.concat(ownerentropy); // ownerentropy using closure
					passfactor = Bitcoin.Util.dsha256(prefactorB);
				}
				var kp = new Bitcoin.ECKey(passfactor);
				var passpoint = kp.setCompressed(true).getPub();

				var encryptedpart2 = hex.slice(23, 23 + 16);

				var addresshashplusownerentropy = hex.slice(3, 3 + 12);
				Crypto_scrypt(passpoint, addresshashplusownerentropy, 1024, 1, 1, 64, function (derived) {
					var k = derived.slice(32);

					var unencryptedpart2 = Crypto.AES.decrypt(encryptedpart2, k, AES_opts);
					for (var i = 0; i < 16; i++) { unencryptedpart2[i] ^= derived[i + 16]; }

					var encryptedpart1 = hex.slice(15, 15 + 8).concat(unencryptedpart2.slice(0, 0 + 8));
					var unencryptedpart1 = Crypto.AES.decrypt(encryptedpart1, k, AES_opts);
					for (var i = 0; i < 16; i++) { unencryptedpart1[i] ^= derived[i]; }

					var seedb = unencryptedpart1.slice(0, 0 + 16).concat(unencryptedpart2.slice(8, 8 + 8));

					var factorb = Bitcoin.Util.dsha256(seedb);

					var ps = EllipticCurve.getSECCurveByName("secp256k1");
					var privateKey = BigInteger.fromByteArrayUnsigned(passfactor).multiply(BigInteger.fromByteArrayUnsigned(factorb)).remainder(ps.getN());

					decrypted = privateKey.toByteArrayUnsigned();
					verifyHashAndReturn();
				});
			});
		}
	},
	BIP38PrivateKeyToEncryptedKeyAsync: function (base58Key, passphrase, compressed, callback) {
		var privKey = new Bitcoin.ECKey(base58Key);
		var privKeyBytes = privKey.getBitcoinPrivateKeyByteArray();
		var address = privKey.setCompressed(compressed).getBitcoinAddress();

		// compute sha256(sha256(address)) and take first 4 bytes
		var salt = Bitcoin.Util.dsha256(address).slice(0, 4);

		// derive key using scrypt
		var AES_opts = { mode: new Crypto.mode.ECB(Crypto.pad.NoPadding), asBytes: true };

		Crypto_scrypt(passphrase, salt, 16384, 8, 8, 64, function (derivedBytes) {
			for (var i = 0; i < 32; ++i) {
				privKeyBytes[i] ^= derivedBytes[i];
			}

			// 0x01 0x42 + flagbyte + salt + encryptedhalf1 + encryptedhalf2
			var flagByte = compressed ? 0xe0 : 0xc0;
			var encryptedKey = [0x01, 0x42, flagByte].concat(salt);
			encryptedKey = encryptedKey.concat(Crypto.AES.encrypt(privKeyBytes, derivedBytes.slice(32), AES_opts));
			encryptedKey = encryptedKey.concat(Bitcoin.Util.dsha256(encryptedKey).slice(0, 4));
			callback(Bitcoin.Base58.encode(encryptedKey));
		});
	},
	BIP38GenerateIntermediatePointAsync: function (passphrase, lotNum, sequenceNum, callback) {
		var noNumbers = lotNum === null || sequenceNum === null;
		var rng = new SecureRandom();
		var ownerEntropy, ownerSalt;

		if (noNumbers) {
			ownerSalt = ownerEntropy = new Array(8);
			rng.nextBytes(ownerEntropy);
		}
		else {
			// 1) generate 4 random bytes
			ownerSalt = new Array(4);

			rng.nextBytes(ownerSalt);

			// 2)  Encode the lot and sequence numbers as a 4 byte quantity (big-endian):
			// lotnumber * 4096 + sequencenumber. Call these four bytes lotsequence.
			var lotSequence = BigInteger(4096 * lotNum + sequenceNum).toByteArrayUnsigned();

			// 3) Concatenate ownersalt + lotsequence and call this ownerentropy.
			var ownerEntropy = ownerSalt.concat(lotSequence);
		}


		// 4) Derive a key from the passphrase using scrypt
		Crypto_scrypt(passphrase, ownerSalt, 16384, 8, 8, 32, function (prefactor) {
			// Take SHA256(SHA256(prefactor + ownerentropy)) and call this passfactor
			var passfactorBytes = noNumbers ? prefactor : Bitcoin.Util.dsha256(prefactor.concat(ownerEntropy));
			var passfactor = BigInteger.fromByteArrayUnsigned(passfactorBytes);

			// 5) Compute the elliptic curve point G * passfactor, and convert the result to compressed notation (33 bytes)
			var ellipticCurve = EllipticCurve.getSECCurveByName("secp256k1");
			var passpoint = ellipticCurve.getG().multiply(passfactor).getEncoded(1);

			// 6) Convey ownersalt and passpoint to the party generating the keys, along with a checksum to ensure integrity.
			// magic bytes "2C E9 B3 E1 FF 39 E2 51" followed by ownerentropy, and then passpoint
			var magicBytes = [0x2C, 0xE9, 0xB3, 0xE1, 0xFF, 0x39, 0xE2, 0x51];
			if (noNumbers) magicBytes[7] = 0x53;

			var intermediate = magicBytes.concat(ownerEntropy).concat(passpoint);

			// base58check encode
			intermediate = intermediate.concat(Bitcoin.Util.dsha256(intermediate).slice(0, 4));
			callback(Bitcoin.Base58.encode(intermediate));
		});
	},
	BIP38GenerateECAddressAsync: function (intermediate, compressed, callback) {
		// decode IPS
		var x = Bitcoin.Base58.decode(intermediate);
		//if(x.slice(49, 4) !== Bitcoin.Util.dsha256(x.slice(0,49)).slice(0,4)) {
		//	callback({error: 'Invalid intermediate passphrase string'});
		//}
		var noNumbers = (x[7] === 0x53);
		var ownerEntropy = x.slice(8, 8 + 8);
		var passpoint = x.slice(16, 16 + 33);

		// 1) Set flagbyte.
		// set bit 0x20 for compressed key
		// set bit 0x04 if ownerentropy contains a value for lotsequence
		var flagByte = (compressed ? 0x20 : 0x00) | (noNumbers ? 0x00 : 0x04);


		// 2) Generate 24 random bytes, call this seedb.
		var seedB = new Array(24);
		var rng = new SecureRandom();
		rng.nextBytes(seedB);

		// Take SHA256(SHA256(seedb)) to yield 32 bytes, call this factorb.
		var factorB = Bitcoin.Util.dsha256(seedB);

		// 3) ECMultiply passpoint by factorb. Use the resulting EC point as a public key and hash it into a Bitcoin
		// address using either compressed or uncompressed public key methodology (specify which methodology is used
		// inside flagbyte). This is the generated Bitcoin address, call it generatedaddress.
		var ec = EllipticCurve.getSECCurveByName("secp256k1").getCurve();
		var generatedPoint = ec.decodePointHex(ninja.publicKey.getHexFromByteArray(passpoint));
		var generatedBytes = generatedPoint.multiply(BigInteger.fromByteArrayUnsigned(factorB)).getEncoded(compressed);
		var generatedAddress = (new Bitcoin.Address(Bitcoin.Util.sha256ripe160(generatedBytes))).toString();

		// 4) Take the first four bytes of SHA256(SHA256(generatedaddress)) and call it addresshash.
		var addressHash = Bitcoin.Util.dsha256(generatedAddress).slice(0, 4);

		// 5) Now we will encrypt seedb. Derive a second key from passpoint using scrypt
		Crypto_scrypt(passpoint, addressHash.concat(ownerEntropy), 1024, 1, 1, 64, function (derivedBytes) {
			// 6) Do AES256Encrypt(seedb[0...15]] xor derivedhalf1[0...15], derivedhalf2), call the 16-byte result encryptedpart1
			for (var i = 0; i < 16; ++i) {
				seedB[i] ^= derivedBytes[i];
			}
			var AES_opts = { mode: new Crypto.mode.ECB(Crypto.pad.NoPadding), asBytes: true };
			var encryptedPart1 = Crypto.AES.encrypt(seedB.slice(0, 16), derivedBytes.slice(32), AES_opts);

			// 7) Do AES256Encrypt((encryptedpart1[8...15] + seedb[16...23]) xor derivedhalf1[16...31], derivedhalf2), call the 16-byte result encryptedseedb.
			var message2 = encryptedPart1.slice(8, 8 + 8).concat(seedB.slice(16, 16 + 8));
			for (var i = 0; i < 16; ++i) {
				message2[i] ^= derivedBytes[i + 16];
			}
			var encryptedSeedB = Crypto.AES.encrypt(message2, derivedBytes.slice(32), AES_opts);

			// 0x01 0x43 + flagbyte + addresshash + ownerentropy + encryptedpart1[0...7] + encryptedpart2
			var encryptedKey = [0x01, 0x43, flagByte].concat(addressHash).concat(ownerEntropy).concat(encryptedPart1.slice(0, 8)).concat(encryptedSeedB);

			// base58check encode
			encryptedKey = encryptedKey.concat(Bitcoin.Util.dsha256(encryptedKey).slice(0, 4));
			callback(generatedAddress, Bitcoin.Base58.encode(encryptedKey));
		});
	}
};

ninja.publicKey = {
	isPublicKeyHexFormat: function (key) {
		key = key.toString();
		return ninja.publicKey.isUncompressedPublicKeyHexFormat(key) || ninja.publicKey.isCompressedPublicKeyHexFormat(key);
	},
	// 130 characters [0-9A-F] starts with 04
	isUncompressedPublicKeyHexFormat: function (key) {
		key = key.toString();
		return /^04[A-Fa-f0-9]{128}$/.test(key);
	},
	// 66 characters [0-9A-F] starts with 02 or 03
	isCompressedPublicKeyHexFormat: function (key) {
		key = key.toString();
		return /^0[2-3][A-Fa-f0-9]{64}$/.test(key);
	},
	getBitcoinAddressFromByteArray: function (pubKeyByteArray) {
		var pubKeyHash = Bitcoin.Util.sha256ripe160(pubKeyByteArray);
		var addr = new Bitcoin.Address(pubKeyHash);
		return addr.toString();
	},
	getHexFromByteArray: function (pubKeyByteArray) {
		return Crypto.util.bytesToHex(pubKeyByteArray).toString().toUpperCase();
	},
	getByteArrayFromAdding: function (pubKeyHex1, pubKeyHex2) {
		var ecparams = EllipticCurve.getSECCurveByName("secp256k1");
		var curve = ecparams.getCurve();
		var ecPoint1 = curve.decodePointHex(pubKeyHex1);
		var ecPoint2 = curve.decodePointHex(pubKeyHex2);
		// if both points are the same return null
		if (ecPoint1.equals(ecPoint2)) return null;
		var compressed = (ecPoint1.compressed && ecPoint2.compressed);
		var pubKey = ecPoint1.add(ecPoint2).getEncoded(compressed);
		return pubKey;
	},
	getByteArrayFromMultiplying: function (pubKeyHex, ecKey) {
		var ecparams = EllipticCurve.getSECCurveByName("secp256k1");
		var ecPoint = ecparams.getCurve().decodePointHex(pubKeyHex);
		var compressed = (ecPoint.compressed && ecKey.compressed);
		// if both points are the same return null
		ecKey.setCompressed(false);
		if (ecPoint.equals(ecKey.getPubPoint())) {
			return null;
		}
		var bigInt = ecKey.priv;
		var pubKey = ecPoint.multiply(bigInt).getEncoded(compressed);
		return pubKey;
	},
	// used by unit test
	getDecompressedPubKeyHex: function (pubKeyHexComp) {
		var ecparams = EllipticCurve.getSECCurveByName("secp256k1");
		var ecPoint = ecparams.getCurve().decodePointHex(pubKeyHexComp);
		var pubByteArray = ecPoint.getEncoded(0);
		var pubHexUncompressed = ninja.publicKey.getHexFromByteArray(pubByteArray);
		return pubHexUncompressed;
	}
};
ninja.seeder = {
	// number of mouse movements to wait for
	seedLimit: (function () {
		var num = Crypto.util.randomBytes(12)[11];
		return 50 + Math.floor(num);
	})(),

	seedCount: 0, // counter

	// seed function exists to wait for mouse movement to add more entropy before generating an address
	seed: function (evt) {
		if (!evt) var evt = window.event;

		// seed a bunch (minimum seedLimit) of times based on mouse moves
		SecureRandom.seedTime();
		// seed mouse position X and Y
		if (evt) SecureRandom.seedInt((evt.clientX * evt.clientY));

		ninja.seeder.seedCount++;
		// seeding is over now we generate and display the address
		if (ninja.seeder.seedCount == ninja.seeder.seedLimit) {
			ninja.wallets.paperwallet.open();
			// UI
			document.getElementById("generate").style.display = "none";
		}
	},

	// If user has not moved the mouse or if they are on a mobile device
	// we will force the generation after a random period of time.
	forceGenerate: function () {
		// if the mouse has not moved enough
		if (ninja.seeder.seedCount < ninja.seeder.seedLimit) {
			SecureRandom.seedTime();
			ninja.seeder.seedCount = ninja.seeder.seedLimit - 1;
			ninja.seeder.seed();
		}
	}
};

ninja.qrCode = {
	// determine which type number is big enough for the input text length
	getTypeNumber: function (text) {
		var lengthCalculation = text.length * 8 + 12; // length as calculated by the QRCode
		if (lengthCalculation < 72) { return 1; }
		else if (lengthCalculation < 128) { return 2; }
		else if (lengthCalculation < 208) { return 3; }
		else if (lengthCalculation < 288) { return 4; }
		else if (lengthCalculation < 368) { return 5; }
		else if (lengthCalculation < 480) { return 6; }
		else if (lengthCalculation < 528) { return 7; }
		else if (lengthCalculation < 688) { return 8; }
		else if (lengthCalculation < 800) { return 9; }
		else if (lengthCalculation < 976) { return 10; }
		return null;
	},

	createCanvas: function (text, sizeMultiplier) {
		sizeMultiplier = (sizeMultiplier == undefined) ? 2 : sizeMultiplier; // default 2
		// create the qrcode itself
		var typeNumber = ninja.qrCode.getTypeNumber(text);
		var qrcode = new QRCode(typeNumber, QRCode.ErrorCorrectLevel.H);
		qrcode.addData(text);
		qrcode.make();
		var width = qrcode.getModuleCount() * sizeMultiplier;
		var height = qrcode.getModuleCount() * sizeMultiplier;
		// create canvas element
		var canvas = document.createElement('canvas');
		var scale = 10.0;
		canvas.width = width * scale;
		canvas.height = height * scale;
		// canvas.style.width = width + 'px';
		// canvas.style.height = height + 'px';
    canvas.style.width = '83px';
		canvas.style.height = '83px';

		var ctx = canvas.getContext('2d');
		ctx.scale(scale, scale);
		// compute tileW/tileH based on width/height
		var tileW = width / qrcode.getModuleCount();
		var tileH = height / qrcode.getModuleCount();
		// draw in the canvas
		for (var row = 0; row < qrcode.getModuleCount(); row++) {
			for (var col = 0; col < qrcode.getModuleCount(); col++) {
				ctx.fillStyle = qrcode.isDark(row, col) ? "#000000" : "#ffffff";
				ctx.fillRect(col * tileW, row * tileH, tileW, tileH);
			}
		}
		// return just built canvas
		return canvas;
	},

	// generate a QRCode and return it's representation as an Html table 
	createTableHtml: function (text) {
		var typeNumber = ninja.qrCode.getTypeNumber(text);
		var qr = new QRCode(typeNumber, QRCode.ErrorCorrectLevel.H);
		qr.addData(text);
		qr.make();
		var tableHtml = "<table class='qrcodetable'>";
		for (var r = 0; r < qr.getModuleCount(); r++) {
			tableHtml += "<tr>";
			for (var c = 0; c < qr.getModuleCount(); c++) {
				if (qr.isDark(r, c)) {
					tableHtml += "<td class='qrcodetddark'/>";
				} else {
					tableHtml += "<td class='qrcodetdlight'/>";
				}
			}
			tableHtml += "</tr>";
		}
		tableHtml += "</table>";
		return tableHtml;
	},

	// show QRCodes with canvas OR table (IE8)
	// parameter: keyValuePair 
	// example: { "id1": "string1", "id2": "string2"}
	//		"id1" is the id of a div element where you want a QRCode inserted.
	//		"string1" is the string you want encoded into the QRCode.
	showQrCode: function (keyValuePair, sizeMultiplier) {
		for (var key in keyValuePair) {
			var value = keyValuePair[key];
			try {
				if (document.getElementById(key)) {
					document.getElementById(key).innerHTML = "";
					document.getElementById(key).appendChild(ninja.qrCode.createCanvas(value, sizeMultiplier));
				}
			}
			catch (e) {
				// for browsers that do not support canvas (IE8)
				document.getElementById(key).innerHTML = ninja.qrCode.createTableHtml(value);
			}
		}
	}
};

ninja.tabSwitch = function (walletTab) {
	if (walletTab.className.indexOf("selected") == -1) {
		// unselect all tabs
		for (var wType in ninja.wallets) {
			document.getElementById(wType).className = "tab";
			ninja.wallets[wType].close();
		}
		walletTab.className += " selected";
		ninja.wallets[walletTab.getAttribute("id")].open();
	}
};

ninja.getQueryString = function () {
	var result = {}, queryString = location.search.substring(1), re = /([^&=]+)=([^&]*)/g, m;
	while (m = re.exec(queryString)) {
		result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}
	return result;
};

// use when passing an Array of Functions
ninja.runSerialized = function (functions, onComplete) {
	onComplete = onComplete || function () { };

	if (functions.length === 0) onComplete();
	else {
		// run the first function, and make it call this
		// function when finished with the rest of the list
		var f = functions.shift();
		f(function () { ninja.runSerialized(functions, onComplete); });
	}
};

ninja.forSerialized = function (initial, max, whatToDo, onComplete) {
	onComplete = onComplete || function () { };

	if (initial === max) { onComplete(); }
	else {
		// same idea as runSerialized
		whatToDo(initial, function () { ninja.forSerialized(++initial, max, whatToDo, onComplete); });
	}
};

// use when passing an Object (dictionary) of Functions
ninja.foreachSerialized = function (collection, whatToDo, onComplete) {
	var keys = [];
	for (var name in collection) {
		keys.push(name);
	}
	ninja.forSerialized(0, keys.length, function (i, callback) {
		whatToDo(keys[i], callback);
	}, onComplete);
};
ninja.wallets.singlewallet = {
	open: function () {
		if (document.getElementById("btcaddress").innerHTML == "") {
			ninja.wallets.singlewallet.generateNewAddressAndKey();
		}
		document.getElementById("singlearea").style.display = "block";
	},

	close: function () {
		document.getElementById("singlearea").style.display = "none";
	},

	// generate bitcoin address and private key and update information in the HTML
	generateNewAddressAndKey: function () {
		try {
			var key = new Bitcoin.ECKey(false);
			var bitcoinAddress = key.getBitcoinAddress();
			var privateKeyWif = key.getBitcoinWalletImportFormat();
			document.getElementById("btcaddress").innerHTML = bitcoinAddress;
			document.getElementById("btcprivwif").innerHTML = privateKeyWif;
			var keyValuePair = {
				"qrcode_public": bitcoinAddress,
				"qrcode_private": privateKeyWif
			};
			ninja.qrCode.showQrCode(keyValuePair, 1);
		}
		catch (e) {
			// browser does not have sufficient JavaScript support to generate a bitcoin address
			alert(e);
			document.getElementById("btcaddress").innerHTML = "error";
			document.getElementById("btcprivwif").innerHTML = "error";
			document.getElementById("qrcode_public").innerHTML = "";
			document.getElementById("qrcode_private").innerHTML = "";
		}
	}
};
ninja.wallets.paperwallet = {
	open: function () {
		document.getElementById("main").setAttribute("class", "paper"); // add 'paper' class to main div
		var paperArea = document.getElementById("paperarea");
		paperArea.style.display = "block";
		var perPageLimitElement = document.getElementById("paperlimitperpage");
		var limitElement = document.getElementById("paperlimit");
		var pageBreakAt = (ninja.wallets.paperwallet.useArtisticWallet) ? ninja.wallets.paperwallet.pageBreakAtArtisticDefault : ninja.wallets.paperwallet.pageBreakAtDefault;
		if (perPageLimitElement && perPageLimitElement.value < 1) {
			perPageLimitElement.value = pageBreakAt;
		}
		if (limitElement && limitElement.value < 1) {
			limitElement.value = pageBreakAt;
		}
		if (document.getElementById("paperkeyarea").innerHTML == "") {
			document.getElementById("paperpassphrase").disabled = true;
			document.getElementById("paperencrypt").checked = false;
			ninja.wallets.paperwallet.encrypt = false;
			ninja.wallets.paperwallet.build(pageBreakAt, pageBreakAt, !document.getElementById('paperart').checked, document.getElementById('paperpassphrase').value);
		}
	},

	close: function () {
		document.getElementById("paperarea").style.display = "none";
		document.getElementById("main").setAttribute("class", ""); // remove 'paper' class from main div
	},

	remaining: null, // use to keep track of how many addresses are left to process when building the paper wallet
	count: 0,
	pageBreakAtDefault: 7,
	pageBreakAtArtisticDefault: 1,
	useArtisticWallet: true,
	pageBreakAt: null,

	build: function (numWallets, pageBreakAt, useArtisticWallet, passphrase) {
		if (numWallets < 1) numWallets = 1;
		if (pageBreakAt < 1) pageBreakAt = 1;
		ninja.wallets.paperwallet.remaining = numWallets;
		ninja.wallets.paperwallet.count = 0;
		ninja.wallets.paperwallet.useArtisticWallet = useArtisticWallet;
		ninja.wallets.paperwallet.pageBreakAt = pageBreakAt;
		document.getElementById("paperkeyarea").innerHTML = "";
		if (ninja.wallets.paperwallet.encrypt) {
			if (passphrase == "") {
				alert(ninja.translator.get("bip38alertpassphraserequired"));
				return;
			}
			document.getElementById("busyblock").className = "busy";
			ninja.privateKey.BIP38GenerateIntermediatePointAsync(passphrase, null, null, function (intermediate) {
				ninja.wallets.paperwallet.intermediatePoint = intermediate;
				document.getElementById("busyblock").className = "";
				setTimeout(ninja.wallets.paperwallet.batch, 0);
			});
		}
		else {
			setTimeout(ninja.wallets.paperwallet.batch, 0);
		}
	},

	batch: function () {
		if (ninja.wallets.paperwallet.remaining > 0) {
			var paperArea = document.getElementById("paperkeyarea");
			ninja.wallets.paperwallet.count++;
			var i = ninja.wallets.paperwallet.count;
			var pageBreakAt = ninja.wallets.paperwallet.pageBreakAt;
			var div = document.createElement("div");
			div.setAttribute("id", "keyarea" + i);
			if (ninja.wallets.paperwallet.useArtisticWallet) {
				div.innerHTML = ninja.wallets.paperwallet.templateArtisticHtml(i);
				div.setAttribute("class", "keyarea art");
			}
			else {
				div.innerHTML = ninja.wallets.paperwallet.templateHtml(i);
				div.setAttribute("class", "keyarea");
			}
			if (paperArea.innerHTML != "") {
				// page break
				if ((i - 1) % pageBreakAt == 0 && i >= pageBreakAt) {
					var pBreak = document.createElement("div");
					pBreak.setAttribute("class", "pagebreak");
					document.getElementById("paperkeyarea").appendChild(pBreak);
					div.style.pageBreakBefore = "always";
					if (!ninja.wallets.paperwallet.useArtisticWallet) {
						div.style.borderTop = "2px solid green";
					}
				}
			}
			document.getElementById("paperkeyarea").appendChild(div);
			ninja.wallets.paperwallet.generateNewWallet(i);
			ninja.wallets.paperwallet.remaining--;
			setTimeout(ninja.wallets.paperwallet.batch, 0);
      document.getElementById("payment_request_btc_address").value = document.getElementById("btcaddress1").childNodes[0].data;
      document.getElementById("paperprint").style.display = "inline";
		}
	},

	// generate bitcoin address, private key, QR Code and update information in the HTML
	// idPostFix: 1, 2, 3, etc.
	generateNewWallet: function (idPostFix) {
		if (ninja.wallets.paperwallet.encrypt) {
			ninja.privateKey.BIP38GenerateECAddressAsync(ninja.wallets.paperwallet.intermediatePoint, false, function (address, encryptedKey) {
				if (ninja.wallets.paperwallet.useArtisticWallet) {
					ninja.wallets.paperwallet.showArtisticWallet(idPostFix, address, encryptedKey);
				}
				else {
					ninja.wallets.paperwallet.showWallet(idPostFix, address, encryptedKey);
				}
			});
		}
		else {
			var key = new Bitcoin.ECKey(false);
			var bitcoinAddress = key.getBitcoinAddress();
			var privateKeyWif = key.getBitcoinWalletImportFormat();
			if (ninja.wallets.paperwallet.useArtisticWallet) {
				ninja.wallets.paperwallet.showArtisticWallet(idPostFix, bitcoinAddress, privateKeyWif);
			}
			else {
				ninja.wallets.paperwallet.showWallet(idPostFix, bitcoinAddress, privateKeyWif);
			}
		}
	},

	templateHtml: function (i) {
		var privateKeyLabel = ninja.translator.get("paperlabelprivatekey");
		if (ninja.wallets.paperwallet.encrypt) {
			privateKeyLabel = ninja.translator.get("paperlabelencryptedkey");
		}

		var walletHtml =
							"<div class='public'>" +
								"<div id='qrcode_public" + i + "' class='qrcode_public'></div>" +
								"<div class='pubaddress'>" +
									"<span class='label'>" + ninja.translator.get("paperlabelbitcoinaddress") + "</span>" +
									"<span class='output' id='btcaddress" + i + "'></span>" +
								"</div>" +
							"</div>" +
							"<div class='private'>" +
								"<div id='qrcode_private" + i + "' class='qrcode_private'></div>" +
								"<div class='privwif'>" +
									"<span class='label'>" + privateKeyLabel + "</span>" +
									"<span class='output' id='btcprivwif" + i + "'></span>" +
								"</div>" +
							"</div>";
		return walletHtml;
	},

	showWallet: function (idPostFix, bitcoinAddress, privateKey) {
		document.getElementById("btcaddress" + idPostFix).innerHTML = bitcoinAddress;
		document.getElementById("btcprivwif" + idPostFix).innerHTML = privateKey;
		var keyValuePair = {};
		keyValuePair["qrcode_public" + idPostFix] = bitcoinAddress;
		keyValuePair["qrcode_private" + idPostFix] = privateKey;
		ninja.qrCode.showQrCode(keyValuePair);
		document.getElementById("keyarea" + idPostFix).style.display = "block";
	},

	templateArtisticHtml: function (i) {
		var keyelement = 'btcprivwif';
		var image;
		if (ninja.wallets.paperwallet.encrypt) {
			keyelement = 'btcencryptedkey'
      image = 'data:image/png;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAyAAD/4QNxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDM4MDExNzQwNzIwNjgxMTgyMkFGN0Q2REJBQjU4MEMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkREMjhDMDQ1QTA4MTFFMzk5QThCQ0Q5MEFGMDMyNzkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NkREMjhDMDM1QTA4MTFFMzk5QThCQ0Q5MEFGMDMyNzkiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjVGNDVBMDcxNUYyMTY4MTE4MjJBRjdENkRCQUI1ODBDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MjJBRjdENkRCQUI1ODBDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQACAYGBgYGCAYGCAwIBwgMDgoICAoOEA0NDg0NEBEMDg0NDgwRDxITFBMSDxgYGhoYGCMiIiIjJycnJycnJycnJwEJCAgJCgkLCQkLDgsNCw4RDg4ODhETDQ0ODQ0TGBEPDw8PERgWFxQUFBcWGhoYGBoaISEgISEnJycnJycnJycn/8AAEQgBrgMgAwEiAAIRAQMRAf/EALEAAAIDAQEBAAAAAAAAAAAAAAECAAMEBQYHAQEBAQEBAAAAAAAAAAAAAAAAAQIEAxAAAgECBAMGAwUGAwUEBgcJAQIDABEhMRIEQVEFYXEiMhMGgZFCobFSIxTB0WIzFQfhcoLwkkNTFvGycySiwqOzNBdjk9N0JTUm0oNUZMTURVU2EQEBAAEEAQEGBQQDAQAAAAAAAREhMQISUUFhcYGRQgOh0SIyE/DBgqLxciME/9oADAMBAAIRAxEAPwD7VPO7Exx4fiP7KpSJbgXsfxU0egkKWCg1fLCttUfDMVlpGhBXUmBAsVquOJ3GGC8SeyngZxe/kyvyq9nVBdjagpO3QC+JIpXgBXVH8qtSZHOkXB7aIwJXhmKgwN4gb1BioJq2RGMjaVJF+FVhSj2cEA4igCkRyK1sDga1izaWVjbKssq6b9mIPYKZHCuLG6k50FjIxVgbHG4PbSbdyrFSM/vrSv1DtrJMDHL2HEGitWsccO+gbMM6YG6g53oFQcxUAU3wOYzo0jJ9S3BFEXIuDQRh9QzFS4IoXbiL91KG0mxBAOVBHQEXGBGItUGq1wb99NqXnSjAkcDiKBdRViCLXogjVgcxRfK/Kg4BsbcaihHxpiAc6rClS1ibXprsOF+6gyzRkswU27DVayFTpkFiOIyNWu66yCbd9Ky6hhmMjWWkuDkb0siB1sfgaACt/CwztU8a/wAQ+2oFUuBhjbNTnR1K91+w0pYA6hgfqHZTlVYY/A0CLxRsx91I6so1JiBjp/dRfWhDjxAZ87UwYNiD8KigGWRcOIqtgHiscx+ymCi5XJlyPZShirMj4XxB4GgVWKzENxAINW1WBc8/Da3O1QHT2r9ooI68v9u0VFbVgcxnT51U6keJcxwqKLqT4l8wpb28YyyYU6sGFxSMNPiGIPmFAWGoW+Rqu9iGOY8LU6mx08M1PZSuLeLhkwqBiAQQcjVURIBQ5obfDhTocLHMUj+CRX4Hwt+yiiMHI541WMBInI3HcasfAq3w+dVy+Fi3AixoJCfCedzemycjmL0qg6Q4z49ookjwsMqgD4EOPj3UGzU86c1UfD4TwN17qKEqEjUnmGNudKrB108Dl2HlV5rO6WJtgcx21BGubH6hgac+NLcxSatS6sjkw/bRQ49jYigkRugvmMD8KWPDWvJqKeF3T/UPjQGErDmARUUrYX+NK1vUF8lovl/qtUQag7H6jYfCgZPLSTeTtBBpojdMcxgaj5qO2oDe4vVUuQ7706ZEcjalbFrch99FNVUmdvxC1WL5RVbYyoOVzQVjBSvFT99OuZ7hSzeE6uDCx7+FMua9q1AE8zn+KgMZWPIAUYsmPNjSocHfmTRQY3JPDKkdiAFHmei2BCngNTfGoRpHqNmTQRFAJOfAmmfymiosLUkx8NhmSBUEXElvgKj+U01rC1JIbL3m1RQONkHxpJcCtuOFWKLDHM51XNmnY16CZIRQl8oQfVhR4KOZoeaTuoL9vYTRjgDXSAuftrlxm00Y5tXWAsK3w2Z5blbK3OgoGdsOFTzGmJAF+ArSFYhRljwoKoUcyc6igsdZ/wBIonkKgB5ChYHDgMzRJtgKAFxjlyoBbVlgKTAA/ICrLk4LlzqtbZDE3oCVuAD8qz7iQhtIFltw41bM4UEk3tgAOZqlF9SUWxVRdjnc0FkMIjS7+ZsT+6mkdVGVMbg/eaySN6hOo4DEc6BHcufEbjgBwpS+ldIUDl8aNlAxoWAxwJOXZVC3HE3PG1PEsbOAw7u2oBaoLkgLnfMcKgaaMrigwOeGVVojOQguRW/G1rd5NJpVFJ+kZ2qZVkljWI4C4OFCCfQSpXw8CeFOSZX5fsFJImi5GIJtVR73wlRatG38S2IuBx/ZVEcd3/gzatmqOOy3A5CuxzFkVVQkLfsrBiWIYm/Durp51nn2+s6kwYcDQZQpBFib3wrayta4OIxpIIxYOwx4UZZwuC4t9goHGoi4P2UrhrXNjbOsyTyK9ibjO1bAQy3GRqDDKjIbWuDkb1UCxXLy/srXN/LF81Nqy8SvA4ig2RPqW4BsaSdwNLkHA/fR2v8ALt/tnTbj+We8UUI3XSBc24E0+pedVQWMZB51YLZEY1AbjnS+U3+k502leVKUU5igNBhcdvClCgHSfgaOntNBBZhiMeNI6DzAYioQVN74HOms3P7KAabjAmxpPHoIwNqK6wSuGGVTxBshjUUFY3NxmL4VNQXu+6gpYNa3ZTaj+E0GeQqznEGqzGvC691SXSHYlTbjhS3j4MR3XrLQMsnmUgkc8L1BKCMQRzqauTj40pZgdS2POxzqB/A/I1UFaNtKnwnyg/dTao2F7W7aBAIsH7r0B12wcW+6lCqSV5eUjlRV2tZluRgbUjhT4kOlhUVHLxkMcRkSOVFipKsMRkfjUuxXEalPEUllkQgGzjD4igUApIQuK44fuqy4bxLnxFV62Eil8iMxVhUN4lNjzFAuK4riOK8qYEEXGVLcg44N9hoW+pM+K1FKwMZ1r5fqH7ae4IuMjUDBh94qs/lH/wCjP2GgDDSbDLNTyPKmBDD7xTEBhY5GqrlGscj/ALXqBfI3Z+ynddaledR11Dt4UsbalscxgaKVSXiIPmGfwoSeJR2g/dR8kh5GlbBCPwk0BhN4lPZjSuNIJ+k/YaMOCheYBFOQCLHjUABuAaSVboSMxiKEZ0/lnh5TTnEEUUqtqUNzpZR4dQzGNBTptyb76dsjUFWm6h1ztiOYpFwuozXxL3VZFgoU8Bcd1VzXjZZR5QbMO+gLGzo4yOBqPhKh5gig1ihA4eJe6o5uqsM6ikmOlGPJhViDSgHG2NU7jFDyLCr8xcUFa+E9jffRbzD51LXTDMYigDqYHsoIcH7xQXEse21GTBdX4caCeUduNRQXC45Gk46uZosbFu0YUWFlA5EUCTrqibmBcfCkja6I38Jq2Q2Rj2VRGNKaTmCftqB0wjJ76UDwKvPE0T5LczVcjeE2wuLD9tFGP8xmf6b/AHU0uOle0E0yKEQKOApDiGb5UD1U3iOrtsKdzhYZnAUGFgo7agJqs+KQch99O5+kZmlAs1uAFFNVEpwJ7QBVzEAEnhVFiYy5zOIHZUDXsB2D7aiDP5UL49gF6JOiO/H9poH2/j3aHgp+2us5sLcTlXK2i6ZIwc73NdRfExfhkK3w2Z5CBYUh8Zt9IzpmPAVALCwzrSITwFKx0iwxJyFEkKLDOkva5zbiaAgWxbFjQJH1f7oqBWOeA+2jpC5C5qBSSf3UiJfM4Z2FWEcPiaA8vfhQUSnT4VwBF27qbbjTFq4sap3J/MI7hWhWVUXHICwFAk7aU08WzrN9tXTEsyqRYZkmqJQNVlwHG9BHK6VYDDj2mlxz50AvbbupvTxAxJPxqgAaiBe5OQFaoYxEv8RzNGKBYxc4sczTm3LDgKgSSUKLsQBwHOkjmSQEMceXCqZG1MWOWQvSjAhuIyoNI9ONSQthnWV5tbasuQvW0MrKGAxIyrE6BHYWB4g8qRXvBIBYg40uvxWON8jT8BQIBFq7HKsi3OmykXXLDOtWsHEAmueDwOYq+GUodJ8p+ygeZnCnQpsc6yajyNdE2Ydh41mkix1AXI8w/bQZjqOAU3BwFbo7hFAHClhi0eJvMcuyi+oKypmcVqDNuJCZAoGHHHjVLFgQdPZnTOCRfiKB8S4Zmg07csFsRnlR3Bf08hiRTgLlwsLVn3EoGlCb8b0D7fXoOWdWkOeXfSwm0YFjjj86a7fhooAvkSL1LNzFQhjyB4UurgWseQqAsrEZ91KLnNrEZ0c+Z+ylZD5ha/HjQE2tYtSKw8uokinCKRfOoyDMDKgRr4ML4Z91Bg1rjhjnT4gfiX7aikEFc7VFV6TrGOdEocwSew1Dfw81NqcNfA4HlQZGVGds7jMXNIY1XEDDiL1ZKt3JGDc6QNjpbBvvrLQenGRewIpTGoxUC9OQQbr8RUuCLioKSqjxAYfUOVMY0PbTMMbjP76QHSf4T9hoEeADxqSCM7HhQAvgWvfInjV9VMoBt9JyPI1FVhHRiqmwOK0p1q/jQMG5Z3q43YWPnXGgfGlxnn8RQZmcoy4kANk3b21bdb3HgP2GpKAyhjkfvpQgF8MswPvFA+rCzjDnwpSGTxJ4l4ip6ZteNsORypCZI8Sv+7iPlUU+DjUhsf8AbOoCGurDHiKr1Kx1KdL/AGGmuHsGwbgaAAmM6T5fpNF11C3yNQ4+B8+DUFYr4W4VAI2JujeZfupH/LcSfScG/fTSKcJE8y8OY5UbrKn8LCilkGF+X3Uhx1KcyLH4U0ZJBifzLh3jgaRsLNyOlqAA6VVuAq2kjAaLScsRUQkDS3DC9QArdiOeINFWvgcxnUbAqfgaDAjxDMZjnRQABUr20Abgg5jOipBLWyONB8PGOGB7qgXJVbln3UWAZSOBFFLFOylBsCpzX7qDOhKqFP8Ap7uIp/pI+I+NEpqXTkfMp7aqRrob4FTpYUB8zBe/7BTxmy6TwGHdSJjO/YMPjT/TqGa3qKKeWkXCR17iKdDdQRSHi/EH7KAyeUjnh86CeUD8OFFsSo+NDJyOeNRSPjInZnRfymgcQX7cPhTP5STyoK3xw4AXNVyeGRf4rfZVg/lFjmwvVe4w9JuTCoIxspPKkC6lLc8F7qEh1aYxm7Y/5RV9hqCjJRQAt4L8xQYWQjsoDEheWJoyGyEcTgKigviOrgMBQc2K3yvTKLKB2VW/idV4DE0BUXuxzP3VB5mNNVY8Q0jicTRSv4h2ZDtNGX+W3dRI8SqMhjSbg2hbtwqBVxW/4zh3Ci3jkC8FxPfwqCygclWonhQu2ZxNBZGSdxEgzJx7BXWOAsK5G0wmSRs2bDurqnO3H6jyrfDZnlug/wCygzhcB8ahJYeHAc+dRVHDLiedaZKFLYtgOA/fTEC4Ud5p6QHjxOVFEnlnQwUXPxo2pG8RCcMzUEuACeNKuo24WxpnsFI54UL2W/E4Cgw7gXlfG/CtiKLLbIAfOssylXvzyrXF/KXuxpRRubhg4+kC5+6sy2LAvfE41fuhqKv9Iw76oA4mqNbwowBU27eBoxxBBfjzqnbuxOki6fdWk3tf5CoEkkVBqb4DnVKbg6vzMj9lVuWLEufFy5dlJ2mg1yQ+pjexGXKqRAxNiQBxIqyBy6lW+nj2VYSLclHGooWCrpXIC16z7gAKg4m9XSTRqtgb9grJJIXck8BYCkH0AkKClsRgT8aT/Y1fNE7i4PiGYAzrOF7STyrtcoMBfw8Miaqn3Cw7eae2r0kd9N7X0qWt9lXKkdzrBItwrL1IL+h3bItvyJQRz8DY0Hjtj/czqO6i9XZ+29xuYr6dUTs41Z2usJxxrSf7hdaUh5PaW+RR5m8eXxhFH+0oU+3ZSTiN1JYf6I69xNuDiiHvNKPLdH/uB0nr046cqybDftgNtuRYufwowwv2Gxrp9X6ru+k9Nk3m32j7+WIqE20V9Z1MFNrK2WeVeN/ujt4YtlsOqRKE6hFuQsU64OV0l7E8dLKCOVe+9YSRxSHCR0VpLfiKgn7aDxMnvvrUhv8A9K70c8X/APsaz7X+4u9k3Em3g9ubncTwX9WBJGZ0xt41EJIx5176JZHfQosM7mvDezrr7/8AdNjYAuD/APXCmgvHv3rakf8A6R3uGGb/AP2FeyaRZW1HAWuf4QBc37qs3Mqxm1yWavC/3D6rutv0/bdG2IK7nq7+lhgxiBClf9bMB3XqDVvf7iRvvG6Z7Y6fJ1ncJgZEusQthmASR24Cqm9wf3KW8p9uQNGMfTR9T/ZKfur0vt3oGy9udNi6fAg9SwO5mtjLJbxMTy5DgK63lyxX7qaDyPQPfWz6tvf6T1PbydK6pfSNvNfS7fhVmCkNyBFetMS8Bjzryf8AcH20/XOlrvenQl+r7NlbbtHYSOlxqS+GXmHK1eg6LNvp+k7OXqkLQb8xKN1E9gwkXwsTa/mtelGvxDLHsNcf3B7m2ft4bIbmJ5pN9MII44yARe13Or6RcVT7h3vu3bbiOP270yHeQtHeSeaQKVe5GnSXS+GNfMPcO392N1npvUfdKaDNuI4tsqshRQrqxREjZtOfHOkha+2nwMeRz765Hufrw9t9Ifqpg/UhHSP0g2i+s2vqs2XdXYZSCQMRfI14v+5rf/pKdDgRPBa/LVUm616np26/W7HbbzR6Y3MSTKl76dahtN8L2vV7rYhxhzPZ21g6CL9A6XwttITf/wDdrXlOq9X3/vLfSe2/bUhi6dHh1bqo8unjFEeN/wD0u6mB1Nr7tHVOtydK6PtP1e229v1fUvU0Qo3EJ4W1nljj3Y16cgN+w1z+ldI2PRenR9O6fEI4os/xO3F3PFjW5bhQVxHKlWM7sVkIf4NXkP7ge9k9kbHZbttgd/8ArJWhCCX0iuldd76HvXsHKszD5ivkH9+gV6R0MX8P617f/VisyZ5Ldmkf3V9zgY+w+okcDeX/APtaV/7xbzY/nda9odR2G3v4pTqtbn+bFEPtr6qrakWxuCB91VuvgZGHqRMLPGw1Ag5gg4EUzPBi+XK9ue5+je69h/UOi7j1Y1OmaNhplifPTIhy7DkeFdVlvln99fH/AGttYfbv95Or9F6SPT6duts0p2y+RCUTcBbfwsSByBr65ud5tdltpN5vJk223hGqaaVgiIBxZmwqWYunqsoq1jpOXD91MwBBB414Td/3d9hxSNGm8m3BU2MkG3kZP95tF/hXe9u+8vbvugMvR98k80Y1PAwMcoX8XpyAG3aKll8GZ5dnE4fWvHmKCkBuQbMcjRmZY42nY2ESl2P8Ki5+yvOn3v7Vk6J/1GOoonTfUaETSK6F5EAJRI2UOzY8BUwrvuMGX/UtZYeqdO3G7k2O33cUm/2yhtztFcGVFNsXTMeYVxdn7+9s9Q6Ju/cUO5dem9PkWHdTyROLM+kDSgBYg6xwr5v0D3z7W2P9xfcPXdzvinTd9Ase1n9KU62HpYaAhYeQ5irONudNkvKae19sI+pOPDgaIYN2HiK8/ufevtrYdK2fXdzvCnSuoMU2m59KUgsL4MAhZfKfMK79lcB0OYBVhkQcR8KmFJoXUUYYHFTSNE64xnUOKN+w07NgdVlZfFc5WGdc7oXuPo/uWCbc9F3B3MMEnoySaHQB7arL6irqwPCorYJR5XBA5HMfGvK+7Pd3Vfb27g2/T/b+56zHLF6jbjb69KHUV9NtEUmOF869e6A8L8xVKqykiI4Z6TSY9Zkr5pN/dbru1hk3G49l76KCIFpJZGkVUUZlmbbWAqraf3Y6xu4hvNl7O3m42sl9MsTyOhIOk6XXbkZ16/3y5Hs/roFxfZyhkPdmK5/9pzf2F01W8pbcD/2z1r9PXPX1xuzrnGfQnt/3t1nrfVo9pvPbG76VCUdm3c+vQNI1BTqhjGPDGtsvu5P+tl9m/pLmWD1/1nqYA+mZdPp6ey3mr06MReJzfkTxFcY9U6FH7hXojGP+uGL14h6J9T0gCf52m1rA4aqzpc6en9VrXz6urtmJjIOYJB76cga7HJvvFUqfTmb8D2PdeuD17377V6BP+k3+/DbtD4ttt1aZ17H0YKewmpi3aZXMm70bXsQe8HuojG3bXlOmf3J9m9XnXaQ7/wBCdyAke6Robk5AO3gv8a9TGSPA2YNu+pZZvMEsuzzHtv3YvXur9b6eNodsOkT/AKdpDJr9Q65E1W0rp/l9tenOVfNP7bW/6p97Xy/XW/8Aa7ivo7yxwI7zuI4o1LtI5AVVGJLE5Wq85i4nsONzM0I8BoPbbuqS+EF+yxrldF9x9H9xLO3Rp23K7ZtMkvpSJHqP0h5FVWPdXS3E8EO2k3G4kWKBFLSySEKqgZlicBWbLnFU7YBW5Z/Gs849OTWMnGl/2V5Sf+6fsuB2g/WSz6fC0sEDsnfqOm/wrt9J9w9F9x7YS9J3ibkKQJUF1kS+WuNwGHyq3jymtlMy7WN0cio8kkhCqFu5OAAUXJNTYb7ZdR2/6np+4j3W3LFRLCwdSRmLjlXnfcfubo3QNtLt+rbkwbjcxTJt10O+uy6TiitbFhnXkv7Z+8/bnSfb+36Nv94Yt/JuX0w+lI1/UYBPEqlce+r0t424qdpLjMfVI/D4eBxFFcj2k1Cp0dovbvoRm6A/OsNFTzlT9P7akp0jVy/bUybXzNj3VHGohPiaipp8OnspCdSqvPOnU3UX7j8KSPHUe2w7qCf8L4VVuMVUcc6sP8tuy9UzG0btxK2Ud9Am3GqTV2eGtC5saqiARIyPpWx+NWHBLDM/tqVSx5ueZwo+ZieAwHfUPgOHKw76IFhb51AuqyA8TlQtYqOOZoR+LxcBgtFiAwJ5GgDtYYZ8KCLpUDjmahB0ljmfupjgO6ilHmY/CqZfH/lBHzNWLitz5c6rlOlAeLG9QQ+I6eZxqP4iF+kZ1Bh3/wC16UHkL8hQWxSBZ4yfMTZFrrKhOL/L99cnZJfcJK2JLYV2SeAzrfHZnluRsWCjvNNQUZ0SeAzrSFc8BmaIFqAGJPKjUAJsL0i882NRiXbSuXE04AAwoEYWGOJNL5m7BUc3e3BRc0QCBb6mzoKpQbq6tptgDSwvqUxg+U3Y9hq2UXjKL5jlWSE+m9jlk1BpkTXGbDLFRWPBjYmy8TXQJ4DLjWOaLQdVvCchypBfdIYweHAczVUUzs/iF78OVU5kAm3LsrUNECYYk8edBJY1cXPn4GsxikU+JfliKtTcDUdfHI1f2nHkKCmGIoCz5nhVjeJSDgtqOfb2cKqlYDwA3c/IVFYzwAwH20MhpGLGmfFgFOHMUBbE5DhVR9OwYAj4GqmjDXIFpOdUxyyxnSVsOR4VeLSC4b5V2OZldWQ+PAmsu90tsd4Cbf8Al5v+41dYKpwIx4g41j6lBH/Tt6QLfkS5f5GoPkXsv3D1rpPSGg6f0ObqUDTM53EZYKGKqCnhRsRau+/vL3TpJHtXcA8CTIR9kdaf7URo/tiUubf+bkFh/kjr3AhxCm9vpOVKj4/H1aHrvXdufesr7BNu19rsDCyQ6rjGV3OrE5kj4ivrcSFxc4Ds491cL390vZbz2t1CTcIpl2sZn28hHiV1IyOdmGBpvYu73O79p9MmkxdEaJpGzKxu0an/AHRSq9GRoUaRptiL5mvnHtJh/wBee6icbl7d/rCvojzBBdQHbne9fOvaAMnvv3MMrlySf/GFJ6j3YQykOcSD4j2V4j3QBP8A3K9tbd7emiRuoPPXI33oK+hhFUFFyOPxr51/cK/SvcPtz3GQfRhkEUx5aHElv91mpCvpJxpCg4Yd1FXSRQ8bBkYBlYcQcQaNZVWQ6Ygal4iiHU8fnXO9xdah9vdI3HVZVEnogCOK+nW7HSqXxqdE6hN1fpO16pNt/wBM+6T1Bt9WqyEnSb2XMY0HSr59/c8fm+3T/wDzv7Y698BxU27DXgP7nX9T27f/APjc/jHVm5dn0Im5PfXjP7ni/tKbn68H/er2LEaiV54ivHf3P/8A+RmI/wCfB/3qk3hdnCTf9a92dJg9ve2fytptdpGnUd/JdA8gjH/lYznicD+7Pqf256ttP0L+35Nuuy6lsGYbiC2kyY2Mpv8AUMm/dXovbW2g2vt7pi7aNY0O2id1UWuzqGZjzJJrznvf29uVlj93dA8HU9lZ9yiD+bGo81uJUYMOK91X2J7XuDg1+BwNLHhqXka5Htvr229ydLTfbYhZB4NzATcxyWy7jmDyrqg3YHysRb4istKpUDOTkeYr4/8A38Zl6N0QuLhd5IbjjaMV9fd7OQ+Hbwr5H/fz/wDKOh//AH1//dipx/dC7Lv/AJ5e3QAU6T1MGwB/Lisf/a1Yf7vz9RT0vbftXqO93b4RequmMHmxiD4fEd9fTQpVVKchdeGVQtrwJKsKZnj8VxfL577B9m9Z2HU9/wC8vdTqev8AU7j9OhBWGNyCwJW41HSFAGQFXe/PbHWfd/VeidPQafbsEnrdXAkCMxBwXTfUbKLC34q92GIOl8+B4GvnnvD3b1+X3Lt/Y3sxI16tJGJd3v5gCsCFdeAIIwTxEkHMAC9Jbbkskj3ey2Gx2O3TabDaxbaCMBUhiRVUAdwr5Z/cuDY9A92+1eu9KEe16rPuvS3SRWQyxao11SIvMOy3410j/bf3Dvseu+9eoyyNiV214o+4eP8A9WvIe9vY/SPaW69tT7Obc7nebvqKpuJ93IHYqjRkWChQMTTjjO+S5xs+275r7HeqcGWGYEf6Gr4p/aL2ttvcGzPUuvIN303pkrw9O2UmMXrSBZJppFybDSBfD5V9s6ql9nvGXBhDNY/6GwrwP9kwv/Q4AzO8nLfKMfsqS4434Fmse5/Q7LbIYY9rCm1kt6kCxoI7jIlANNfNfbmz2h/u17pgbbQvEm3jKRNGhQYQeVSLCvqYF7xt8DzFfL/bp9L+7vuq+Q28YJ+EFTj6+5b6e9633n7fh9w+1N/0aCJUlEfq7ONQFCzRfmIFAwGrFfjWD+13Xj132htBM19306+x3IPm/KA9NjfnGR8a9kVBsciMQRXyzowPs7+6HUeinwdO9xR/rNkPpEo1OVHx9RflSayzxqXSy/B6P+53XP6F7Q3skbW3W9tstrbPVMCHZf8ALGGrT7G6IfbPtnp/S5V0zNH6+5P/ANNL43U/5cF+FeV9xW94f3M6T7dX8zp3Qk/Xb8DymQ6ZNLf+zX4mvp0g1g886XTjJ51Wa23xolVyAqRIvDBhzFQEgXzX7RTmzDsNYV5z32Ff2Z102vbZy2+VcP8AtWZYvYvTXtriZtxqXiPznrt++L/9F9d5jZyg/KuZ/aax9hdNBy1bj/3z1r6Pin1fB64lZFDKb8j+yvnO4Jb+8+2J/wD9Zn/oevogj0kqpsRlyI5GvnM1/wD50bYHwn+mkWP+R6cfX3U5envjrf3H9x7j257ekk2R07/euNntmGaFgWeRe0KMO01q9mez9h7Y6ZEWhWbqkyh+obuQBpDI3iZVZsQq3t2515j+8Foo/bu8kH5G33xE3yRxf4I1fUCw161N0kxBGWORpdOMx65yT919jznur2l0v3V0qfazwxjeqrHZ7wKBJHIBdbsMSpOBBpPYm36/tPb+32HuSMx77asYkYushaFbemxZC2QOn4V6NgUfUuTYEVl2HVumdWUz9L3cW7jif05XhYOFbPSxHGs5uMejWJnL53/b7cQbT3B763O6lWGCHe+pLK50qqrLuLsSaq3G96l/dbqEvTemSPsfaOykA3m6yl3LDEKB28FOWZxsK8/0b2vvPdfu33Nsn3bbfosPUHm6lEhs0zCaX0UH/pYnLvru73b/APyv90Q9X2MbL7W6sVg323W7CCQZML3OHmXs1CvS4zcfuxo85nGu2dX0npXT9l0rYx9O6fCu32238KRL9rE5kniTXz/3R6vvH3vB7LEjp0fp6Lu+rLGSPUewYISP86qO0k19IRkkQTQsHRgGR1NwysLgg9or48nQv63/AHO9xbGTqW66bIV9eN9m+h3X8o6WPEWa9Y4b230jfLaTzX1va9N2XTYU22x2ke32yjQIo0Crbtwx+NfOv7gdKj9q7zZ++OhRDazwTLH1CGIaI5o5PxKMMbaT33zFdIf21c3B909ZuMx69Zd3/bDbbqBouoe5OpywXGtJ5lZCQbjCTCnHrLntnzoXNmOvu1eo6im03nT9xvTEk0cu2d42kRWIDxFgRqBtXl/7TbXav7Sgll28TuZ5rSPGrMCGFvERevV/o02ft+bp8bmRdrs2hSRramEcRVWNsMQK83/aTH2bADkdxuB9oqfRffF+qe6vcg/lk99L5O4j7aCk6Chz4doppBdNPO1YaQLdLHjSRnVdjmPD8qbV4DzGHxpVGggcG++ooE2JQfVlRA0sQMrUD/MD8B4ajnSQ3woK2xDJ23NVuNcmngov8hVoFg98ziaqjI1SschlQBMYiON7D4Y1b5mHIC9VweQE8bsaZDpRic7/APZUVGxkB4Lh8TRfHwjjn3VG8CXPDE1FHE5moAmC24Ck88gJyAwojxEqMgcT+yj9fwoI/AdtI/iFhkTbvonxOBwGdQ4uByxooPwUcfuqmYgyIOC+I9/CrSwBLnhgBWVyS5v5hiRyoHJLERr5mxY8hVhUAaRy8RqRqI0vmzfbUbwIb5nOoLdtZZYu/AV1cgSc65e2H58bHngOVdRzkozNb4bM8gGAsM6hwF6a1qRjiB8a0gjAY0pJI5CjYnFvgKU+I1BEFsaJNu85UTYDsqpmJBIwvlQBRqYngDj8Ke9vFxOVKAFULzxNE5/soAAb/wAR+wVn3CaDqQZ5nka0k6cM2NTSLHVjfOgpgkuNL+YZVYwDCzC/YayyI0LBgcPpNXxSCQY4N+HnQZ5IWW5XFPxVXcgaR8q3k8suJqhoA/8AL8PbzpkZSbdrUyNIuRw5U527pwv2ikIfkQOdqBmnktpB+AqvHzMcqYK4yUgHOj+nkIvkOZzoKTcnDjgDWpNuqgFsW7eFGKJI8R4iOJyqwY+I4nhypar30kYkAF7EZGs7J6RUliL5kcDWganGJtzAohVHD4muxysv6hr2A1DIPlVG/nD9P3itcMYJRpAvc6GrdIsZGntvYVQ+2JF9ekDK9B8l9k+8ova/R36duul7yeV5mmDxIALMqrbx2N/DXon/ALqbRhYdF3/Z4Vr2qxub2fT2m96sWBuEn30HzTqnVvdHveD+kdO6VJ07p8zD9Vu9zcXUG9rkLhxstya9103YJ0rp226TASYtrGI15kjFmPaSb10zt1NiSSRlc1Yum1wLc6DLFAwNnwU5DjXhPaEEqe/PdDmNlS76GZSFP5wyJFjX0VhcdvCoGLDHhwqCtlUgMOHKuV1/oW39xdJn6VuG0hrNDLa5jkXFHH3HsrrDAlTkcRQXBrfD5UV8y6Z7m617IVei+6tlLLtIfBtOoQjUNAyW5srryxDDKu0/90/aYjLJJuJZOESwkEnldiBXtWVXUo6hlOasAQfgazr0/YRtrj2kCNnqSJFPzC00R87Oz67/AHG3+33HUds/TPbe2bXHA9xJMfja5YYarWUZV9KREjRY41CIgCoowAUCwA7hSkac725ij4swb99FQjG4zrwP9ykklk9venGzld7dgqlrC8eJtXvrkZj5UNQORsflUBdQWOHHOvG/3Likf2pOsQaQmeE6FBY+bsr2Ia+Bzo3IyoOX0Eleh9MVwVI2sIII/gFdAHScPKaJwx4HOgUU9l+VFfOesbDeexuvL7i6LC0vSN62jf7KME6SxubAZDih4HDKvewbiDe7eLd7c64J1EkZIINjzBxBrQpdTpz7cjalLA3Q4HMXpajPIrajpNweBr5J/fSCabpPRVgikkK7xyyIpaw0DHwg19bDXLDkbUazLi5axmK0YFVGRsLg91FlDZ/OlaM/Se4H99KJCps4+NZVDcCzeJefEV8t9zbXqns/34vv/abKXqfSN3ANt1JIBqlh8KxlrcvArA5Zg2r6rcHEYilKkHUh0mrLgsy+ft/eL2dKmnbJvt1uOG0i2zepq/Di1r/GvA+/ZvdnUpele8ut9Ofp3Rdnuo49nsGu06R6hK084sNJfRbG3Lv+8qBG5ZFCOc7AC/xp7rICCLg+ZWF/mDScpNollu9cbY+4One5+hbvqnSjI+2eOdEMiGNmKobkK2Nsa8n/AGb280HspPURopRu57q4KkjwcDX0IpoA0CyjJRhbuphJrzNzUzpYuNckv6guMGGIr5BvOsbf2b/c/rXU+vRTR9N6ttkXb7mOMupbTFy5NGQbV9eddJ1L8aVtLYsoZfqUi4/zC9JcfFbMsPQ+r7brfTIOo7TX6E2r0jKhjchGKXKtiPLXjv7tdPnXpWx91bAf+f8Ab25TcBucLMoYHsDhT3Xr2u63MHT4dxut5II9rAhmllOSIguWw5CvmXvz3VtPeUWx9k+0twN9uepzod5PEG9OOGM6vExA4+JuQFOO+ZsnLbDof2m2O5m2fUvd++UDedf3LuvZEjHBb8C5NuwCvo4YHDI8jWfp/Ttv0zp216XtRp2+0iSCLnZBbV3nM1dgws3mHH9tTlc3KyYmE8r24N99Agqbr8VoNqAs2I4MKZWDKD86yrzvvcNJ7Q64sYLNJs5QEAuS1srCuZ/aqKWH2L01ZEZHDbjUjgqf5z8DXspFJGpcHGINRZNeLebjernTHtyY1z7CmzN2MPtFfOd1tpZf7x7d3RvT/ppX1QDpuEf6sq+jSAga1zBuRRN2UFT3Ulxn2zBZn55eY93+3l9z9E3HSJ2CTG0m3nOSyp5GPYb2PYa8p0T35u/bGzj6D762O5282zURQdSijMsUsaYISQcSBxW9+Nq+mS2YB7Yg2YVRgrGCTFD4oycu6kumLMws1zNHgOrf3Ek9wbR+kexthut9v92pj/WNEY4oVYaWe7cbcTYCvTezfbCe1vb+36arh9ySZ95KuTzPbUB2KAFHdXeVvSwsBG3IWx7hTrxXhw7jUt0xJiEmubcvnH9t4pU9ye9WljZBJvboWUgEeruPKTnXuOrdJ2fXelz9K36atvuE0NzUjyuv8SnEVusWGJ8S4A0sbYFTgwNLyzc7LJiYeB/t/vupdH3e69idcVjP08lumbrSdEu382kNlgDqXsuOFL739u9Y23W9l729rx+t1HaD093s/wDnRgEYD6vCSpGeVsq+gY3YA5GgSGIHfcU7a5xvuddMfJ4OL+7PtsIp6lBvOnbwYS7SSBmIPEKwtf4gVxd8eq/3Q6jtdtDsp+ne1dtKJtxudwNDzsBayDuuBa9r3PKvqhC3tIoYgeFmAJ+2lUHT6ZPC607SayYvvOtulujHuYNW3n20ACatu8MaDADwFFFfMP7fe7une2+mf9N9ai3MHUYtzJphELMT6hA4ZWNfVHP56fxYHvqblQkqTqBqyJtjbvqSzFlmclmssuyx1OgW8wbD50b6ivzIoKQyoRkTeofDIWGWRrLRZMHXkfNRl8htnw76IAYseGVKp1NpP0VFS147Dl9tIx12PBbE99MToJXn5fjQVbKyfH50AkOkMeyqIx+WH4C9+29HcMWAUZlbmrSAsFuAWgrj8luZ0im/4tuAH20kJshJ+jD4mmfwKp+q/wB9RRbx3HAZ99QtgAMzRwRbf7E0sY8NznxqCINNx20vmdgMsLmje7MB2XNQAam+FAB5jyApC+nUcycAKJYjVpzv8qQ2jS5xsNTGilc+mBfFhjbtquJPBdsWc6nPZRIOgyNm+C93On0+BU4mxbuoHGJ1HIZUp8bKTkDcCmPiNh5RnQJ8dhmB8qgtgP58YGd66gHi7q5m3AEyd9dIEnLjma3w2Z5biTbLOlUfUczRI4DjRJArSAxsLcTSA8seAqG79gOApj4RZc+FApFzjiBnSscr8cfgKe308BnVQOtyx8owHbQN/EczkOVTyi/E5Cpf6j8BRAt4mz+6oIBbE58aUm5sKly5wy50fCgoBoBFmxvnWSWMpjHcjszrSSz4DAU3hTvoMscxwE2I4W/bWj1UPlNzyGdVvCJcho7RVLQGPyeJuzOg02ZsTgKn+X51k9WdcGv3Gj60n1kd1MDTqAP4mpW1NYHC/CqRuHNlQC5ysKVjM12JOrgoH7qgvYxp4WOWYqmXcM3gTwg/OlXbyfWbX+d6vSCKPGwLczRX0BiL4ebsoAs2DeE8hTXVRh8hSONWBz4AftrtcollXyj40AhY3f4Coo0nx58DwqygUgcM+FLZTmMaelI4jOoBoHC4pWVlOpW7xTg37+NSgW7WwINKxZTqthxtTeU/wn7KJoK2bC9iCMaVnUMrA4GrFwOk/DupGUYqR2iirLihSacLgXHLjUAByJHZUD0hW2K/EcKNm4NQ8fYaCBgew8qhANA3Oam/Ail9S2D4dtATGpyw5WpQXB0k35E0+pedQgEY0C6jxFAMBxwo3K4NlwNQgHMVFBhcYZjKlPjGWIo6QuFyBwoMHU6hjzFBhcaJ2ZSQr/EXFMHa17ahzWhO2lybGxNwe2iND+JTYniMKzWhDKcjjyqEAixF6VgfrXUOYzoC+aNfsNQKYmU3iNv4TlUEovpkGhu3I91NrI84t25ioQsgsbMKCEAixxFVshGK42+dQpJH/LOpfwH9horKrYHwnkaigsgbA50HS+K4NzpnjVuw8xVeto8JPLwcftoCr3OlhZuXOlYaf8pyPI07Irj7iKQMQdEvHJuBoM282sG828uy3cYmglQpLE3leNhZkPZasfTPbPQOjapei9Og2Mkgs0kK2Yj8JY3a3ZXTZclJsfobl2UInxMbYMOFAQzWxxAz5igxB8a4kZjspmBB1rnxHOgVVvEMDwIqKNwRfgaqKlH1JgGzHC9S7RNZsVORHA05s6kA1AAwOBwPKk0i5U8MVNMLOvizGB76rcshDHFRmeNqKa58r8cjwNKt9NxmMCO6nwYcwarBMbFTipxB5UAk8pdfiKonC+B80P3GtRHFeOY51QVDR2+k/YagS5jPpsbq2KtTm6kOvcy1XcNGYZMHQ4N9xpkYteNsG+886KdWBYj42oaQSwPeDUIDaWyOV6AYrJZ+IwPCoIrEOytnhY0GH5gIzAprAswNJcrJZsRbA/voJJZkPP8AbQxZbZOtFxipHPEVG/GuYzqKyzH8z1Bwtbv41dP4oiRwxFIwDRk8Hx7jQRy0IQ5nw/KgEXhCN9Jubcr1aD4Wbneq4jZSOAFHy6UPlP2UEU+muk94qWKgPx+qmkGsheWN6UtqGn6jgailPiPqDJcv20XbSQ34sKK2UFDkMu6qwCQSc18vdQVSLpC8y1m7qs3DBYj22ApJiCwvkbGllvdUPDH4UDILML8fE3fT21Asfh3UrAlQvHzGmvrsB5RnUUE8YDn4CgLlmUc8TRBxZB/tepYK3ZaoAAAxHdSXLMwX4mji78lt86mQa1AoACBRxNUyN6jEZIuJ7bcKM0pACpiwFviaYRiGElsWtcmgRj6kgBFlQZdpp2vdUGZN2NJCDbWfMxw/fVhFmCjE5k0UcvCtKthqPbnTEhB/tc0iKSAW+AqCyElpo+ALWHbXXtYWFcmEj9VEDwN66jXIxw5Ct8NmeW4XuSRjwqEfEnjTAAC1ITx4nAd1aZEWxbgMBQJsNRzOQqYtgMFFAC5w+dFKSxFhhzPbQAAAUZCixudC8KgAH7KAgW8TfClxkzwT76PmN2y4CldwuBz4KKgYsAMKS1zdseQoC7YkX7BkKJ1nAWHbQEm2Bw7BnQuBiftoBAOJJ4miIxnb4nOgUtfM/AVB2DSPtp/CuAzqY8MO00C3tzpbg5JfvpiR3mgT8ewVFIxIGGfAAUFsBjew++nIYm2VSwOBOAoF1Le5BPKpqvgot2mnJGQoE8FF6D3StcD0xfm1OLr9J76WKyr6eWnhVtdrlITfAqa5/WH3idH6j+gDnd/pZ/0np+cTem3p6e3Va1dKpQfNBsPcsvVY+iv7l6qofoP6901bZZhvfUEOn1BtwQtzlnfjWjpW89y9O69F0/09z1TeT9D2W73m13e7CQw7r1Hh3DB2EgUuVACopBN8q7J6T7lX3h/XlOw/pw2v9P8AQ1T+v6Jm9f1fJo18NN7dtOOne4191brrsJ2DbSXZrsYYnecSj0nlnjd7RlfE8lmAyGVEY997m3W82HQuqdNjm2u23fUIdtvrmL1Y29c7V9tJG6sGQuCGdGuLC2dWdW96ybHddV23T+lnqB6LGr79RuEhlJeMTgbeFldpbIcThyFzXPX2z7yXo+16Xr6UW23Uv6n6hfc2a25O9EdvTw8TFb8qbrHtDrfWpupLvE6VLHvHP6PfTJK+82SGKNT+mkQLfTIGdBqWx50V6P3H1pug9F3PWV2h3kW0QzbiFZFjYRKNTspYHUQMl41k2nuTcfrtnsuq9Nfp69TWRumymZJi7Rp6zQzJGB6cnp3YC7DAi96ze/ZItr7G6rtZJGeabZts9uSpeWWZk0qLICS7ab1TB0nqfuWPYdQ6h1DbDb7WGf8Ap246akqu888LbQbt/XtoMasxVBfxccKgu6b7wbf73p223XTX2CdXWVumvLKpmPoqZGXc7bSrwkqpIxbkbUkfuzq253XVNptfb0kkvRZFXeH9XEFZXiXcIICV8chRsUNgPxVwh/b7rcY2LdOk6X03e9PWWJd5topvXnM8DbZt1LM3jEiavUVbsC2bU3SN31Pee4/dMHQN7s/S3c0JWTdxT61Ee1h20m6h0BUlHqArpJHiXOqO6fem23D7aHocC7+Tc7SLqN5txHs449vPcRBnlDEyOVayheGJFVye9DND0fc9N6VJvIesyvtICJ443i3cQl9SGZSGAVPRa7qx7AcL439hQ9P3G0k6Zt9hvkh2MHTpIOsQ+qNO21enPE6KxVzrbWtrNhiLVom9tdf26dBh6bLsJE6RM+7maZX24kklWaNo4odrGUjjVZvDxwxvnU0NRi959QlbbRr0UltzvZOlB23SrGN1EryMBJ6XiiZUOlwMW8OkWquH38+8i9bYdIknDdQ/pkaNOkbs8lm286goR6UiXcm91AyNdz3J0nc9Y6W222W4Xbb+KSLc7HcyAskc0Lh1LBcbEXU99cnZeyv0HWum72Hcr+g2G1hjO20kO+628Mm1jnv5QPSma/G9NA/UveUnS5N2+46XIuw6fLDBvt28yxnXMEN9rE6j10T1MWDDI2Bpt5HOPfGz2g3u6XadQ6bvGn2yzMIg8Um3iSSFf+G4EjYjjXE6x7B6h1Buqpr6fI3UZ23EfVt3FJLvYhqEibZPoVF06AykWX6b10Nt0P3jD1jY9Wn3Ww3b7WDdwv6j7gMf1kybjSpEZ8EPprGnNeVBn6J7gfpXt3a/q5Jep77ddR32w2KbiZRJJ6G4nC+ruJsFWOKLxMfgCaHWfd2/fonU26XtUh6x0yXapu4xuY5ESPcOuiaGVUdZFfFLFVIx5YrD7J6s/TNtB1E9Nm3fTt/ueobNHjkn2so3jSvNBuY5kUj+b4WW5BAPZW1vaG7l6R1Xaa9lsdx1H0Whh2G3Ee1gO3IkQHSqSS6nF3ZrdgFNF1el2p3ku3V9/t12m4JbXt45fXVQCQLSaI73H8NWWdON1rn9H2vU9sN3N1WaOTd73cHcNHCXMMS6EiWKEy+K3g1HAYk4V0vFyFQQ6iMgRS6iMCD2Gp41xAuvLlRuSMsO+orHKyrIysbK2XYarjKklGtcVbIxLsjLnlWaQ6SGKeJM+1ay0v0D6SRSsjZ4E8xgaGqM4gG3MUNa8GI+FQTW6+dSRzqAxPipsezA1PU/iB+BpGKN5lF+YwNA9nGXiHbnStofBxpPM1Xr0ZEkUROjYFgew4Gip4043XnwptanBsL88qXUnA6fuoENmtmHIZfKoIUaPGLFeKH9lEMkoK/NTmKVXF7A6T+Fv2VHCsbnwPwNADdPC+KHJuIquRWuHU+NcjzFWByPDIPjSstvLiOFRRWUEDUNJOXKofCdQxU59lVxsNRjOF8V7DVmkEkeVuzI0BYBhb7aQANng44ihd4zZvEpyI4UWYHxLmMx2VAhLRtqbFTgSKfBgbYg0TZl7DSBb9jDAkUUqqbeHBhgRwoFvEuoWORHfR1Mj+IYNxHOi4DoePEUAIK4riOK/upFx1FcccRTAsAD5l+0UtvGWQ5i/fUFE35bCQYqRY91GWI6RLEfEBe3MVZKBJGwtjbFaTbPgImzAup5iilSUOpGRI1AVabEqeBrPKoilW/8tmv3XzqzxKot4gDl2UBsUc2xFsuNEEMzcRaoGBc2ztlQ03ZmGBwqBTdXHFQPlUkwF144Woq3iYNgcuylYFXGnFRiRUUGACm3kIx7KogP5rqcmGHeM60MQFLDEHMVlYGPRbNMQeYoJESuByvpv9taMCWJyGFZ4yGY2xFy5+FW4qoQ5NieygiHTfVxxB7Kljf1ePLspnAfwcMzSliPB9fDu51FK/jsVyXE9vZRJAIfgcDUA9Pw/Scj20trkqfKcV76DO+LKeT2A7KkxvMz8EFjQ3Den4zlbHvBowjUNTY3xtzJoLsbADzNiTQXw3jXMfcaKXW6ZtnfsoMRGQ3PA9tRUNkYHgcDSkFmVmwHKiykjU2YxA5VJGwGnE3FBCQHBOVqrN2B4At8TT6fGC2JqqRrLnbEk1AkYDzNbyIcO+puWv8Al8Bi37BURvSQkDMXJPM0IkNi7Yu2NzwoLEFlA+q2J5VCbOFXE2orioCZcWoYK1hibZcaioRZSScedC5IsvzqMC1tXE+Wi7BFJy5UDbRR66tmS2ddYkXx4Vydtq9WPgAcb511VAAuczjet8NqzyBiWwGF8+6hbG4zyFEmwvxNAsEFznwFaZRsLIuZpXYRrYYschRBIBJwJxJPCqNRkJYYjIUFiiw+1mPOhq1Hw4j76IjvjIcBw4USbjw+FBxopDrY6FNuZHD40yxpHwu3PjRFlGGHM0L8sBzOdQRiBi3yoAM3iOHKlB1HwKSBxNMQ+bECgOAyxNBmtmbdgpSDbM9lFYlGLYk50ADAeUfE0MD5iT3ZVZpXkKBIGAGNRSFlGQoF7C5P2VYBbE50jHUQBkM6BARxvc59lNqUYAUxOnDMmltfE5UA1XysBzqA3ysBTaQcxhwFQqvEUHvBZjjkwv8AGpcrniOfGkJKt5e0d1WXblXa5RBBxFSkKuMVsDy4VAznA2BoM+z6ht9/Ju49uJNWzmO2m9SNkBcKr3jLAa1sw8S4VeR9SEX+w18wl3XQ4Ok+99hsOpwdP2r7/bxpLtHVkgSePaRM+iN1KxGRismki12tjXS9lx7SD3N1GHaw7BGfp+3aSbocnqbHwTSWDqVBjmbXlqOpReg96GB4gd55UxsL3OWfZ318492bPpXtjrp68+wimg6xstzsjCyBlbqJ/NgGnK+7Vmjf8VhekX2/0r2/u/bvQ+uiIdHO03Mm9fcEDbbnqw9GzblnOlj6fqemHwwwyFB9IN1NwbN32vQ1g3JOPG5r5d1DbbKT211vd7ZPV6P0vrO2n6HuBd1hgSXaneNtnxPoK/qZeG1+Arqbjf8AtHee/wB0bdbCbb7no0w3zGWIxSn9TDpWRtWhmEad4Wpge8a3AjUMfhWWfquzj3m16bPKRuN4kskCHK0AX1Lnh5xbnXhv7fdA6JvOje3+udP3Knd7H1ju5IGWSSYya41g3Mt2f00UgqnYK2e8Np7ci9zdE6n13a7RoP02/SWbdpHpZo0jlijLSi2oeMoO+1B6leobabe7jpi6/wBTtY45nYowjKylgoWS2lm8OIGVXTblottJNFC+5kjW428On1GP4V9RkW5/iYV4GDqO2l611vde3xDtusb72/Du4dgroZv1rHcSaXjBxmXw6sL5XpejD2ttt/7Z6j0vqSQznbzN1RY9LTTloA0kvVZncNGsUgP8wecgC1MD3HR+r7brfStr1fapIkG7j9VElAV1FytnClgDcc626jcCxuchzr4t7Xk6K6+2977t/QSdKm28mx6XIkyELuI5Gk09SvbUXVrxg+BOPiYGtG+Mu6Xr273u86bB1ePezjaTTfqH6pCiv/5H9BHEw1K0egoIxpe/ivjTBl9Wj6ltZ9/uemRm+62iRSTocAFn1+nY8f5ZvVfVepQdG2EvUd0G9KLSPTjGt3Z2EaJGuF2ZmAFeNnboPRfeHVOob3YbeTqkkPS32S6I03Es88ku3ll2+rxFtTAvp5Y10f7kR9IfocI6wNqdO92Yg/VaL2O6hWX0/U4aPNb6c8KK73Serjq0Ur/pptlNBM22m2+6UK4kQKx0lGdHWzCzIxFb/FjcjDOvnDp7UM/uMe4X26PtX0dFQsFMXTv0yHat0wKRa7l8YcS1VSbbpu66t7LX3O23PV9xsJR1SDcyIrzSCCH0o9zGSNfjJKqw82VTBl9LYYeJhVZfRm5I7BWHoPRouidNi6ZHO86xNIytIApUSO0npoq4KiatKrwFdMKBkKKrD3yvjkThQKMMRlxF6sK8viOFKLjy8M1NQZJIg7N4jn8qrMCNgfMMrnOrpMZGKYNxU8aW4cciPsNZaZ0jVSQBgOAzFWjVa4sw+RoOCPGo8S+Yc6NgwDxm1+HA1Ada5HA9tQgEftqBg2BFjxBpdAHlJX7qCXxs2fA0GRG8yg1DrGDDUOzOlEgvpY9xOFRSmILkoYdudIFjJ0m6nhwrRSsqsLEXoKmh1YajbkcaqKbmPIiRfwnOrgHU2Bv2GiHGR8J5GgzCQMCCCpGYOIFESAcQL8eBq2RMda4MM7cRSMkbD8xRj9QqKrlTWM9EgxRqaKUSrpYaZFwYcjStDIg/KbWn4Gx+RrOzlCXIIYZ91Btz8LZ0thfQ+P4TzpFkLqDg6nyuKYuD4Xup4GoF0tG1lPhOQPOozFGDMLcGPCmBDgqcxn++oDqBVsxgaKDWZbqb2xFDSGGpcCeVBVGIyYcRQXWhK+YZjnQBGK3VxkcxQYeIMhsTh2U2sepjhcZHnSyJky4G/wAKgh8WB8LcDWZ420q6HTIuI5GtJPCQYc6RR4APMn2iiqnf14wCLG+Y4EUIpNSlb93eOFKxMUgYYrx7RUkRPVDqbJIMxzoLyoZ79mBpVLLqJ8QvnxoRs/hJ8WFrjsoq40sRnc4VBE0up4gm9Kt1LN5lOF+ItTMoC3GBtnQBKKA+Q4iopSLuCmIGJHA0koVoyfpGJ5inVcNa5tj2GlfxnSPCwF2HOgwbRtDtG/FsD2VvuMXOWQrnjL1F83+NbV8VjHigzXtpSGH5YLNiDieypp1DUfMcjyogiQ6vpHDtoG4Nh5ePZUUt/UBU8M6BxBBwZcjTMBbUuBGVITqHqHDTwP7aDFvW9QaBgGtfstWjbm63+rJR2c6z/wA12k4OcB3VbtyTCNGLqSGNKLn8NiuLce6oVGkkm5PGiCoGHiJzpVUg2fEjFeVqigC0i8hkTxqAD0yBwwqagrlRiTjYUApLEMcDiAKAM/lK4/dlWcJrdQ5uTiw4VoNlUcgbVjWTEoDiMGtn3UFjt6kotgi4Dvq23htkgHxNIF0lLjLJO/nVjDC7nuHCoApJUBcBbOgNKM3w76KliotgOZqKoBY5m+ZqKUlmYWwFszQKi4GZzJNHWNRticsKA1sxOQGFBbEf/MRLfNrmuoW1HSBfnXJ2qr66OcfFheuqxIFvqb7K3w2Z5blxYlmPhGAAoXGrAXIqF8NKcMj+2hcIAi4u2ZrTJGDStoJ8P1AU4CjDJVwAFAWRT99AMTgg1HnwoHOOLYDgKrLtIbILgfKiyHDWdTHIcKsGlBYfIUUojbNjjQbTfSM+NMxYC+XIVETSO05moJjawwFA2XE0xIGJqu+o3z5CgIBJ1NnwHKmoeI9lKQO+oqFxkMT2UAG7qYC1Qm2dArWAucaS9sszUJ1Hs4DnTWtni1ALWxPH5miBxPyo2tic+dLqLeXAfiNAWYL38qQ3Pmw5CiByz4saOC4mg943iXUOFFTdQamTW4N99LHgWTll3V2uU9AgHOiSBxry/vPfdb2cfSD0QwhpupbaGf1pHj1B2sI/y45Lo/1/toO2OmdPjBtsoNDEFisKC5Vg63suNmFx21fBDt9umjbRRwxk6ikSqikniQoGNeN6y/WNr7p9ubjaQK/U99t99FvNt+qmXY3ijidZGDKfCmo2Ii1G9a5fd42HRuq9T6rtl2266PuRsdzt45Q0Ukz+l6JimkEYCSesvicDTjfKgug9qT/rEfqXVJeo7Db71+p7PZzoC6bhixT1Jizakh1H01VVAwve1eimgi3EZhnjWWN80kUOptzVgQa8v0L3gvVOr/0TcybF91Jt33cEnS94u8i0xsqSRynSjI661Iws3DKsHvCPb7Xr/RGmbqUu26m25j3+z2Eu6YyiDbhobQ7ZwQFbElLdtB7UBYwIiAqAaVUCwAtlblVB6f00osf6LblF1aU9FCBq81hpsL8a+ebWLfdU/rXSejz9Q220j33S2h6fuZpoeobfb+op3s6tuW9VIZVB0eI5N3U0XQOq77edfh2fV+pzy9K6tso+nxS7+UIsAXbbmdZAWtJdZH83ZQfRIdtDt1YbTbpCpN2ESKik5XIQDGjJCm4XRPEkightMihwGGRswONeR6DtP+q+nye49/vN0m53U+5/pybfcSwJtIopXghWOKJlRm/L1OZFa5PLCsnuDadR2/8AbNp91vNxD1zpfTTI+82u5dWM6oBIWeJgsit237Kg9oNltY5zuVgiWYkkzrGgkucCdYGrHvoPsdqDKTt4z+o/nEov5nZJh4vjXB97rKPZnUd9DPNtt307Zybva7jbzSROsscRsSY2Gocw1xXneiHp/uHqsHT4d31HZwdP2se5DS7ndR7jqccxbVN+Y4/8uJL3IGom3lW1w99/TulaAh2W20AkhPRjIBNrm2njarGg27SJMYI/VjGmOYqupRyVrXA7q53UOpbT250bc76ZWGz6bAXIuXcqg0qoaQ3LE2F2PfXA2nvPebvewdMhTpe66jvo5JdlFtOoDcorQgSSQbpoo7o+i+llBUkWor1zRK8iSlEkkjv6bsoLJfPQ5FxfspZtptd1YbyGObTfSJUVwL521g15eD3d1rebToU+z6dtGm640qLt33EoaAwqzSGUiA4RaCslvqsBes26/uRsoP1e7B2LdN2E77eeN96idQk9J/Smmg2hXFVa+lWYMwFxwqYHtf0u1vEfQjJg/kEopMf/AIeHh+FJLsdlNKJ5dtE8wtaV40ZxpxHiYE4cK82Ove5JOpdW6bs+l7SRulCKZpX3cipNHOjSxpH+QSJNK46vCOdZ+p9e61vG9p77oKQpterOZHi3cskZbXtZZlil9GOTwra9x9QHCg9gQeOPaM6AY94+2mj1mNDKFEmkeoEJKhreLSSAbXywqFQc/nUVAQcqBUHv50pVhiMfvqCTg2HbQZZbayJPgwpGDDHO2TDP41fJYseIqqzJ5cV/D+6stFDB8sGFVqfTfScEbLsNOyLJ4kOlhxH7aRjfwSizcDwNQWMobvGRpdRU2f4NQQA4XII5USHyuGHI0DUrKGFiLikJdPpJXkMbUwkRhcH9lRSaSmAYgcONHU48wuOYp8CKTy/5fuoBqVsL2PDnUwbBhjRKq2YvSNGc0axGV6CaSvkPwNV6wjWYaVb5Xpw5+pbWztUJRwRmDwqKGkjFD8OFVOATqAs4zU5EU6gqdINiMr5EVGIOEgt20GMxCJ9URKK3mXhVut0FnGpMwR+ymK2vG3iTNSMxVYcw4ONUR+ocKCwqrgMPN9LDjQIbzocRmpqu2gn0jgfEEOXworOpN8nGYPGopmZhaTTccbcqjOtw17WzvyNMCAcMUfLvoAAgxtjb7qCSAEAnGxpXQ6ToNuQOVQICpGIIwNqC69Od+BBqAhjYah8RSIAQShtj8KZHIUBgRbC9Kuhi2ON+GFFVyA61NtLY48CKzSFogQRYeYDMfCthDBx9QscDnVcoUqbeE8jlQLtnDKtjcWJHdTkAoLjM1gQGJ7AYAkr+0Vsib1EQxtccQcxUoZwwsqm4PA9lR3vZDgTzqajrJYZYXGNRSrsTmMhUVGUKNSm33VU50xsZBZrXvTlLtZTYDEjheqt3IVi0sPMQMONBls6qLY2yPfnVkbiM+leynE9lNEukBlN7kjTStGunTbxjMdh4ig0stzZMDzoBgvhbA/fVEUxi/Klx/C3Pvq/SGxOPLsqKGkg6v/RrPuXyVMz5hzHKrJZvRFj4jwP76oRPFrbxM3iI591AhS4LjBMAe+n258bxpgpxv99OQAgdvKxuUFUPqjkEgwW+POxoNV1iNh9X30HDHxHC3AcqYiMLbn86VWdsLWIzJqKjABQwwtjSs+KlRfhfhRVACVbHiL1RJKdJijxYfVwFBXuJCCUvcg3IGWNLtEsCThc3vxNIbBS18cyTxNaYgEWPSNRIxPfQO19PhGkZ3OdEhVzNz9tRlZlOo27BUGgC+GVRSqWKiwsOZoKtwSxvicOFFWuuAJ7aUa9BOAzqArYKTwxNIWCpn4m/bRKDSFJJvUYqpJ4LgO+gaF7TxgDwqR866IDysSTZRmRXJgLSbmJR5dWPaa7Tusa6eA4Vvhszy3I2mNRhcnIUoIXE4s2YGZ7KVdcrluWF+Aq3SqDw4sc2rTJFjaQ3fBRkoq26rgo+VRVNvEfhQdhgi5nO3KgCgudZwGQAp8AOVDHICwpCuttOYHmqKgIY6zj+EUTq7hTYClJubcKBdOo44gU+AoXUUpYnIUBJvUAtQs2ZNAi/HDnUUSwGHHlVZa5suJ4nlRNgMBnlzNQYYDPjQQC2AxPE1LgZYmp2cKgHLDtoARfzYn8NG3FvlwoagMBUsTn9tBCeWA5mgOwX7TRwHaaBPM/AUHvSpdfN3WpCikhje+R76s8p/hP30rCx7G++u1yp5cxcc65vXelydX2cMe13C7bdbXcwb3ayuhkjEu3fWqyIGQlWxBswNdRTcX+dK2kY3seyg8jt/anXNt1TpnUU6ptZF2T7uXcJJtpS0r9QcSbnQw3FkVdNoxY243qD2h1Ldw9b23V+obeSPq24j30L7TbtG+33MHo+i49aWVXVfQUlSMceFdzrfV5Oi9Nm6n+il3sO1VpdykDIrrCis8kiiVlDaQvlvesEXu7ZCDpc3U0/pbdYY/o4tzLDrEfpNOs02l7IGVbWubGwoL+m9H6jBvf1/VN7DKyRNBFttjt/0sHiZWaaVWklZ5PCAMQFF7DGsXW+j+4N51vp3VendQ2cEXTPWMEE+1lmZjuIxFJ6jpPHhhddIHxru7zd7bY7KXqEza4YkMg0EEyYXVY7kBmfJRfE1I+obU7CHqMzjabeaNJAdyViKeoAyq+o6Q2NrXoOX0vou8g6lu+vdS3Ue66puoI9mphiMO3hgiZpFRI2eR2u7lmLN2C1UdA6H17pfU+p77qPUNpuYuqTDdSxbfbSQskqxRwKEd55Bo0xi4IvfjXeM+3MZnjmQxqCWkDApZfMS17YWxqr+pbE2B3cCXIUfmpiWGpQPFmQLioOFD7c6t0htzB7f6pFtem7qWTcDb7nbGeTbSTEvL+lcSxrpLksFdWseyl6l7Ym3HtZfaXRt0m16e23bZ7mfcRNPMY2zdCskShybklgRjlXfO+2C+if1MJO4/8AhyZE/M/8PHxf6a50vWJU9xbfoU20eNN1t5t1BvhIhjYbf0xKjR+dSDKMTQYus9F9wdY9t7nob9Q2abneRvttxuU20vp+g6aPBGZ7iTtLEdlZOpe1/cXUf6Nuo+p7HbdU6QxK71NpMSykaGh0ncfy5EA1qb44i1ei6Z1bb9Th3M0AaOLbTSQNJKFCOI7H1omViGjYG6tWPY+4V33W910hNt4Yduu7g3sc0c0U8TyNCCvpE6SGQ4Ggt6l0n+tdEn6T1V1L7mH0tzJtwUGo4+pErlyNLAEAk1m6b0nqkO9g3vVN/DKNsjpFFstr+lWUuAvq7r8yTUwGSrZQTeus+92Yk9NtxEkwZU9NnUNqbFU0k3u3AUo6hsBgN1AbqHUGVBcE6V+rIsLA1FcrpXtpOmdc3vV13PqwzGRtjs9NhtjuWWbdlWv4vVkQNkLViHtDdQjc7HZdQi2/St1PJuTbbA76H1pPWli2+61hQrMWsWQst8OFdHo3X9v1UTpuoR0ze7fdy7FtnNNGztJEFZjEUI1jxjKupPPDtVEm5njhQkKGmdUBY5KCxGNB56HofuPb9S6z1GPqGxP9VSNI422s35XoKYoix/UeLwMdWWOVsqq23trq226N0TZf1Db/ANR6DIrbPdfp3MDosLbbRNF62q5Rybq4xthXpX3e2if0ppo45NOvQzqDpx8Vib2wONPDPBuI1m28qTRPiskbB1Pcykg0HI6N0fe9KG3246j+o2EG1EXoNEA7bkyNJJuTLqJs2qwThzrr3YZi45imKqaWzDI376KIIORoEA540Db6hbtFC54HUPtqDLJG6uxiP+k5UgmF9Mg0N25fOrndS5GR5GlZQwswuKy0Vlv4lNjzpCQ3gkFjQ9N48Yjh+A5fCp6it4ZBpPbUFbIykFTZhlfI9lWJIH7DxU1DcDHxJz4iq2GnxeZDmaC6q2QE3GB+yh6mkXxZftplkRsmBqKQBSbHwtyo2YZNfvpmCsLH50lyuD5cG/fQLd0+m69lH1FPG3fT0hFsQLjiKCHHxLnS2Vhe1HShFx9mFKVZTqU94NRSsj5o2IyvUElxdltz4imu1srjmKRnCHVYgHzA/fQBkBGqM2YYilNmF2Gm+ZGXxqyyNiPmKrs8bWBurcDzqCgs0BBOMd8LY2q14op1DC1+DCiwUghgVvnyrOmuG4UFgM7YgigFvSbS91VsyMu+rbNgwa5XPtFRninjKkHu4iqdRhIJNwcicu40Vfd1e+BDfDGgGZWYFe0WpVlRlKg2IxWmLqSrg54GoIrgFgcMb49tAaGZsqa4155j7qFlLtcDIUUmjxjSSMKEmvTYgNc2ptCl8MLCgym6gMc7/KoMU6hZ8AVDC5HaKVPOXQ6WA4ZH4Vo3WsaWwOYqsLdSNNi2KkfbVDRbldNn8LnLkauKqqYj/trM8aSGxFtPmBHGqwJY2AjfUgyB51BrVCowbE4m+NZZWZ5lBF1TIjt4053QPhY6T9V+VIjXOtl8JPDHAcKimGggMMGvZRlTOChuRrDYG2dRSp8TeY5A8KayqLq2J+NBnKqwJvqK58DakE5QWc6QMn51ZONNpJQDwCrnWKTU7Wkxt5QeAoNCku2qTwg5XytTqGOowjwnMnl2VmRmVbsNaHBb8K2LpCgh9aj6alAOm14/EwzFVSKXDcFuMBwJq/UG/lizD4VTIxVmLEISMV50UYCFXSeGFzwppZUQa74jgOIrC02ltUakq2BLfuq5ExvIwJGIYnC1MASSSzC6+BB/vHsqCPSVxtqwPYKcFVOZK/So51NJUFdPibImorO4RQwUXvkewVrOsoLWFgO2s+41Fb4CwFagpKjxZilAKXGJJvQXSFGVRVFhe96iqouLZGopQ66c+dLq8AABxtTjBD8aBt4RyqBHZgb2yGHfVBEjvoDC+bEcKM8yrbHzG4HPlSayq6VsC2JJzqi2F1TdIFN9JAHK9dQIXN+GZJrkbGIybmNh5A3mPGu9Zb2v2WrfHZjkiqqqBn2Cgbs1gLAU5awJAypUDWvkTjVQSABdje1KlsWOBP3VGGpgl7jM0xAUXtRSs9sFxJyqKCBYDvNRRc6j8KjMPhUCm5wv8qOlVGNS6jEnE0C3YSeAoD/sBUwXPM0tzkoueJqXI76CHm3wFAkAanwHKh4s7jv/AHUtsbtieAoINTeI+EfbR7FGFS9zjieQo6SfMbDkKAXAwHiPIVNLN5jbsFEsiCw+VLqZshhUUfCuVAkmoEPE0bAUC93yFGx7qJYcKQtfjfsFB78spFs78qQa2BQixHE048J0nLgajDiMxXa5VajxEMT3dtWBQMhSPkJB8abWOGPdQcH3tvdpsvanWDu5khE+z3O3g1mxeWSCRUiQfUzHICvMbvc+39z032X1fcttpun7SVdvvt3IiyRwn9DInpzFgdH5thZsmtxr6J4z2ffUCgdpOd6DzW66psd77Mk6h1nbRbLZbjaO3oboD00DBlgBDqvm8JUW414w7/ou76f7MZ+qbOA7XpjNG3UIhuens6pt9vLFMmqP05kJ8LFsMRxr6uzC9szyqtoxfWbKbWwAyzteg837EWL/AKYTbrs0gg/UbxUiQEwSxvuJH9WFZFVhDJqJRWHl5515rc9D2v8AVtx7EGxi/R9R6hH1lHWFdKbHGbdR69OBG4j9MfwyWyr6R6jHDTZefOnBsLaSBUHyH3gm3G590wRw7Labn9Imz2W23EEm43c4ig1xP0uFNCRJqlK6kvZl1G1q6/Tvd/Rer+7OgMN3GrQbPf7JmkcfmyMNkUdeyUhtN89Jr6P6g7RalOnNSAaZMPlSbrou72nU4enPDP0eD3Km66rttoNS/wBOMcY9Voox4ofWClrCxAPC9dnoXUfbJ9/7wdGl2yR7zpkEcZ24CQzzxzSu4h0gI7LHbVp+Ne9Di9sjRIuLH4UyYeE9/baPp+42XuPbbRJd40e66Z6ixB39XdxD9HJcC90niVQ18NVczoHt7Zbf3FH0GTaI+29rHdbiOaSMG6btYjtBrcYhPUnIGNiOdfTLlMDlwNQ3zHypkw+SvuvaO86J786hJPtJt1Ju928G4JVpR+Sg2rQP5lvIp0FM2rVvt3Hveubfqu86j05emT9L26dN3XU9qd7tXlDON6kTCaJEmLadQN2YZZGvphsh1DynAjlT6hwNMrh8pkXoO22vsfZ9dkjaSPeyyD9dEsEv6Mpuv04eJ3kdYNRQIrt+G+OFek9sb3of/U/uXpnS59uq+ptpo9ptyqqHWER7lkRbAWcAPbjnjXsjjnQKg8KmTBbMMjfvoaiMx8qYpbmp5ULMMj86ioGByNAqDw+VAg8Vv3UL2yJHfQZpUbU2IYcjVV2TmOw4irndtZwuOYpQ6nj8DWWiCVeOH3UWVXGONFkU5j4iqzG64xt8DUAKMmKnCluRiB3gZfKm9Vl/mqR/EMRR8EniQ48xQVK4XFcV+peI7qcoj+IfAildB9QseDrVWqSA6ra4jmRw7aKsIC+Zf9Qo6ARgT86ZXVxdTcGgUtiht2cKgrKOnka4/Cf2UQWIwIP30wfGzeE0GW+IwbnQKdYNwt+YvUEgPA0Q1zpYWaoQb3Gf31FIXVTfIcRR1K2RBogg/tFIVAOXhP2UChVDaSMD5T+yo0ZIsGI5caLRKRYYciKCgnC5DDA1AFZyMQDbAiq30qwexXgbU7B0bVqwOBwosHIINiKKoePUdSG57MDVazFQYp1uv4q0AkizJcjA1W6gMCLjVgQcRQUE6LAEOoN0P7KvGllIFj9S1nl25IwYDtGFvhWeKeeIhXsSuFx9QoOiVQlGsMcKGhdeXCqUmV1sHsynykWNXFW1jxZg1FDQus55UCg1jE4DnR0nWfEchQCXc4nlnUFG8X8tcT5hQCnCzeXx/OjulGkYnAg50jKAo7To+FA/j0+oreJuB40rB0U3CsDmOZqwLFe2FkwHfSM8YBkK+FcsM6Kx7jACMgeLFmGYFVoull0EqBiOK1aNRLSkAXxYnhfgKjbhFJEdtAGI50Qybl1BWwY3xJwqt99EuCpqkP1UmppF0hhozKgZU6xIB4VuTwkOXbRVQkRyTIzA/UbXPcKYTIF0qPHzYY2o2QnQpUWzHb30PSibxSvp5Y3qBWljY6tLaRnpwsar9Vo21RLj28au9KJTeNvCeJP7KgcKdOphyci9BX+skktr8B4FabQWcFrPcZ3xHfROmQkIQ7fUSLX7qqaJxjEuWZvjQOYWAxIZfKWqQjV+SbWXyNz7KqDyeJLBb42PGg8i4eAoc7DnzFRXQAZgVNgRnakcOQGv4kOIqmPdRSDMrIufbTPNEV1I5vlaopJQShOrBvEMOeFXRAlB4jhhWZ5EAK68ssOBpo9wAFsS1xawHEUGgL4mFzzoBBqbE886pM02oWUC+GP+FIf1DvYtmMhhUFjtEkZ1NzwvVEkpe/prYWsCcz3Cho0qxAub4tQNzZbG7Z91AkMAMnqOL2GBPCncBn0Cy6Rw7eJqxtMf04gWUcr0Y00Kbr4mONFX7QIkkQGNjXWUjMCuXE1poxpzauqCQMq1w2rPLcrksQtu00WJUXwoKSSWtnl3UCSzWtgM60yKqwFycTiaW2s5mwosxyAqYjADHjRUIGXAZ0AAMbdwqXJyGHCoWIwAuxqAnDvoWJ7BQxGLYmgSzebAchmaAlvpT58KB0oLuaV5CgsAAeA41XmdUhx4XyFA2pnxAsOF6IUDM/Ol9Rfpux7BUz81+6xoG1qMEFz2VNLt5jpHIUDIq4Y9wFD1L5Ie9qBgqLln86hccBekvfzN8AKN1HH7KijdzkLUNBOZo6gciT3Ch8/jQHQvHHvqEqOIFKdHGpdBkPsoPoDabWakDMTptjwJ40ygDC2NRhcdvA12uVWUsfEbq2Y4UUBXwjhwo6xbS2fEUh18cLcBmRQeN3Purr/T5vcO36iuwifou0j323KpORuY5SxVgPUuuMbRWx8djlhWnb9Z9073dxdFWHY7fqibOPqHUnkEzwQLuHZINsiq6u8n5ban1AYZY1yngm95dX6Jvpum7rpX6ISjr0G5RlRo45Un2+z9R0VZiZ4kkvHdQt8ca9N1LoU+46kOtdM37dM6g0I2k7+km4SaEOZEDxOV8SMx0sDxN70Rytp7s6nPvOjxHa7eKLd77ddJ6lFqdpId1tI55GeGTBXjf0hbUoNj8rW611xj7mgRNmdx0P0pdszLKI5YpIW3RWQa7htA03GF8bcKtb2fHHsNlt9nvJYN7sN1J1GHqDKkrvuZhIJ5J420o4kEzXAtbha1Z4/aXVUfrMq+4GabrkaJu2k2cZ0sieheFVkUKvp3UKb87mir+g9Z9yb+fpzdR6fAmw6lsRvvV2wkttWIRkgmkkJSRnDnygabcRjVe56p7nPuif290+LYLCNmm/j3k/rNoRpWg9KSNGXW7FbgqygDnXb6J0/c9J6Vtem7nd/rm2sawpuPSEJMaKEQMiswuAMTxry++2/Wt179bc9OO86dCOnrsP17bNZ9rJIs0m4YNrZcArLocEC9xjUBm967z9Ds3Mez6fuH3u66b1Hdb2b/AMrtptnqDFBqiklErKNAFrXxqqX3n1eb2kfdPToNlJFtJ5tt1EN6zR6Ipxtzu9uRob01X8wqwvp44Y9L/oqLbvsd103fPB1DZjch93uIY916zb2RdxuJZI20BZGkW4ZLWGGVcUdFT2p1CWDeDqHW+mdY2+8imdYPUEb7iT15Ynj2yHW+4d20sQqoMKaDsN1D3Juut7/pGx/pzQbDbwzybySOdrSzqxSDQko/Brvq8pUWvjXK2vuf3ZL0zoPUn2ewA9wSR7aCEPNq28kiPIJpWxEiWiY6FAOQ1ca6ntToPU+k+112W43Xo9c3Y9Xd7yRFnKuVWJEZdSq/pwoseeYvVUfs3qUXT+hdPTrvg6DMs8DnZITIY0aKMP8Am8Eka9s/hQLuPd+46T03rs3WIon3nRJoICduzJBOd2sbbdvzdbRC8tnvqta+NU9I9277qPUH6Pt9z07ebmTbPudtu9ms7beJomRXh3KuQ2Ie6MrC9j4Ratj+zZtzuOtSb/qjTQdZaKUxRQJC8Eu20DbSxSh38UXprmuJx7K3RdJ6x6r7rd9ck3G/WFtvtHMCJt4g7KzSNtkcLLIdAGpmw4AY00HE6Z7o9xbjpnRuvdQ2myTYdWmh2sm2gaUzxmdzCkwd/Ay6gPBpuAfMau6DN19vcnuCPe7naybCHdxIIgkokAbaxtGsLPKUXMahpxa9qCey+qQ9E6b0SHr1o+lzx7iGU7KMs3oOJYVYerbBr3534WrbN7W9fqrdRO/f0JZ9tvd3sRElpN1tEEcUgk8yL4VJQcRnQXdZ6rvNtv8Ap/RulRRnqHURNIsu5LejDDtlUySOsZDOdUiqqgjPOvN+5Os9e3Ht/rnTlfb7TqXTZdvt97LH6xSWDdmMxy7ZldHjLByGDE2sbHI16vqnSD1ObZ77bbptj1Pp5k/S7pUWUaZlCSxSxPYOjgA2uDcAg1yt97O3G86bvNmnV3j3nU549z1PqDbeORpTDo9KOOPUixInpgAC+F+JvUV0+g9Fi6BtZNtFpBmk9WWKIv8Ap430hGG2jlZ2jQ6dWnUcbmuprFCFJVhjXcyCecKBLMEEYduLBAW035XptK8qCXByNSlMa0PTtkxFRVEiKXNxVZjvx+eNGRZg7WcEciKTVMM0B7jWWg0SL5TQ9SRfMlxzFN6tvMpFT1VPA1AomjOF7HkcKDRI3iAseDLhTFo282PeKT04/oYqew0AtMmR1jkc6TWt+KHiDkae0oyYOPtoF74SJRVBjQHUqjtW/wB1WKARdRqHEZEVCEOWXI1UyMG1RPpfk3H41BdpjfAjHkaHplcrkcr40izox0TLofkcvgae9vKwI5E0A0o4tc93Gl0lPMSV5/vol4283hbnUMoXzeIcwKioY74hjfnegFvhqN+INDUB4o8R+Gpr14hTcUA0lTbUbcKDow8QY3HDmKbUzeErjyoAyDwkDsN6gGksvmwNKofFS2I7KPjRuAU/YaDhx49QwzsOFFAh1fzYN2cajq+k+IYY5UWQsvmPMWoBdSg3J50EsxANwb9lZZtuSWZQNQx08DWhVQXUnI86U+mHFhe4tUGBgjLc2vl2gjhTjWpX0mOWV6eYem5ZU8LcDzpwCVS9gL2DcaCtZtzqPlbsxvRWeXG4tc8P8ackBm1ktbitIL6bhNS53aiqJ5rrirG7DjypBKCTaM5eHvozAn00DY3vp4VEjCASSXtcgac6Bv1LaQioB+Ik41mkn3G4cImCjIKPtp3fzRxkANmSMSeVEKqIbrYZeHM0FYhY2DXY3vp4DvNWLDGGLS3w/wBsBVioztYn0UUYKM6dVjQly3HC+JqCsjDVGhUDLVx+FKY4rapbmU8Dwq8NPIdSgBRkWw+NqURlj6kvi5Y8Kiqi0aqI9IF8NVqa8Mfl8Z5EXq3A5JpX8RFIssCm0Uikn6AQSaBC0L+YEHkBalMjMNLrgOyxNWtJHh6xAPBSQPvogsReOzL33+6oqkaZvDpCsOORo+lIuKSaSOBFxTsgfPBhwypDGoAZHIPI40FL3LDWMeOmqxZrx8RiFfD5GtDNIQDoxU5jCkb05SPW7sMKDLJEt9SWRswh/YaKlXKgAq2bBsj2irCqpdAdS5qx4VXJDKAJAdQzt/jQCRCpuDqBOGnKjEToK4eE3CjOgkl1CnwBji3I08JCS2cXDXBPaKgsYG1yhAz8PGibalzReVOqggqrWthSsXsgOIvj8Kiq2tpJDeEHwjnSRBj49WZzI+dCd10thZjfSP20QgVI0T5jiaBkDSSs+BVMB2nnTguSLgcTURGSMgHic6K6rnDLCopoWJ3US6eONdZ2wsAbnCuTtGB3KsQR4rCusCGYtfAYCt8NmeSMwVbD4UAQq2viaOZuchQOJtxOdaZQW4HHh++gcfCMuJoMyDD5mqx48EXw0FhYDBfiaUyKuV/2moIgMMz91KSqmy4mioBI3iI0jtpDqY2Vj2mmIZsXa/8ACMhU1AYAgUAESjIam4sabQg8xv2UNSZX+VHUALhTUBuckXDtoEMc277YUpaVslwojX+G1AwCr5RUue6lNxi17UuonJTbmaBiwyvc8hS2GZAogqOFu+jqHC1RQty+ygVY5E/E01zzFS55igTS45VPFxP2U1z+IVL9tB71ivHPhzpQXbA+H76ZQtrjHtqNa2PwrtcoFAMV8w40jNrHgHiH2Uw1Ng2HZzpWNsY+GdBwfdHuDde2ulydWTYf1CDbi+5UTCKSMMyojKrIwYXbxY3HbXeCte7HE/ZXkP7j7iKP2vvtiiyy73fxaNrtoYpJXkKujNb0lYCw52r1Ow6js+q7Zd7sJDJA5IVmR4zdTYjTKqN9lBwn677hfrG56HtOkQPutvEu79WbelIm28sskMR8G3kYSH0iStrDnWKP3rut2NjBsujPuN/ud1vdjuNmdxHH6MvTx+cVmYaJFPAi1cnqm99t7r3zvZeqT76KCHp0G0Wbbf1CBDPHuNw0sTPswmvSHXmOVdX2lBt+mdMn33UYJIul9L3c7e35t1E36qPZSqiuzIF9U63L2LLrK2LUFHTPfu83z9M3G46O3Tuk9RfdxfrtxuomMbbJJpJdUUak6f8Ay743rRuvePVtt0Z/cv8ARCejekJoXbcBd0Y3IEcz7f0yqI2oH+YWA+mub7X2209y/wBv9x0RFMe/EXUIkO5gkieB95JuVjcGVF8ySY6eBrcPcRk9uJ0cdLlk9xLtU2p6LudtI0XrqqxXlk0+idvqGrXrtp7aD2MjrEjyyyLHHGC0kjEBVAxJZjgAK5ae6PbjxNPH1rYtCpCvINxFpBbVpDHVhfQ1u6sPvpNs3tjcQ75Jmh3Eu1hmO0UvIpeeMeokel/U0t4tBXxDCvJ7jab/AK1sfdke2jHWhu+kJBD1JNm+xaWeJpWj2voy2SR1D6g6gWwXlUH0Zt9s/wBYOnNuof1rJ6i7QuvqlMfGI76rYZ1SOqdO/XnpSdQ2zdQA1HY+qnrAWv8Ay9WrLsryk3uLpu/92e2Nxtk3Ahij3kE25k2m4RY5p44kigcvGLNdTfgOdYembfajpvQug77ZbxvcHTeppu95DDG0QkmWZ2k30+6aNkeEq3qYPdsF7KYMvdHq/TV346U+/wBsnUWF12TSoJiLXwj1asqk/VemQbyPpu43+2i301jFtHlRZWvlpQtqx4V4SLb7b+lp7e6lst5L1+PrB30sW3jaM7lhvGmj3TbsxtH6IiZSTquNOnA1z9703fCL3D0je7rdfruo73czR7GDpyTPukkk1bWWDfOhVdCaQGZh6Wnspgy+k7zqvTumSxQ9Q3+220k5tDHPKkbvjbwq7AmqurdW23TY1D7raRb2bDZwbvcLtlla9tOohjbtCmvD9R2u52vW/cI6nvZ4B1NYl2qL01eofq9uu2SL9Okuh7MJA90Nhc6uNaOjpD7c3W6T3Jt9xJ+s2HT4NjLNA28d44dv6U2xdtuki+oJbkrgGvemFy9J033Jtd37e2XuPqDw9Li3UYdlnlUIpJK6RI+gN5cMK6m03+038A3Ox3MW5gYlRLCwkQlcGGpCRhXy/p282I6J7O28s256Xvelxbq25/RybhdtKgSN4dxtnia4ZJcHHlORxr1HtfqP9M6Uo3m0kUb7qk8W1k222lRJv1DmUbtoH1Nt43bV5sBnkaWGXr7nmKl27DUup4X+FC3JKyo+PkKHj5CppPK3xoaX/FagokL6zgKS8nIUJQ4dry2+FVM7D69XwrLS0l/wj50hB/CPnVf5xyj+Jap6ch8+HYtQMbjI2+NVlj/A330fTVMQpYcjTrJFlbSeRFqCouf+ST/lNIZbYMhW/M1p1rwNQ6WFjYg0VnKyHFRb/UKUruciiMO3OrigXIXXlxFQC48DEVBjkSX60w5A4UgMsYu0QkTgynEVvOvsNVMpU6ghHMDjQUrPqFwmpeRFEToDbydhxFRkVjrjbS3bh86BlkAtLFrX8Qop9anEHSeYyNEm+fhbgwyNVCON/KAAcjxoNtZB5ZLjipyqC4m+D+FuBoFl8rHGs5V48WQsOIvcfCnWWO38PaMRQWawbqQTSqz+QjuJ4imuGGfcwpWYHjZxlaooKHBKE2Ga25UAlmKkkg4ioWZ11IuI54VGDOoe+WNhQAhEcE2xoO4wKgmxpmVdOoC5GIJoO6lSBibYAVBXPG8sTLgDmOd6zRMugM+IuNQNbRrYA+UfbWEKIZGH+oE8eYoL1Y/mKowGRPbRZQEAc3vYVWHOpwvka1mPChJuI0bw+NlFyeAoqrdMGcL5VAtc9tUO5sViJ0jy3zJpFEm4d5ZDZbXueAq+yhwtj4h4UXl20CpAka+pMbu2Atib9lXJE+sECwUYBuFNEhB1MANGGPCioeUFibIccMzUCASSlrmy3z4URFCDpHDM8SacIoUIMSce6rAoUWFRVRDNgpIHG9ME4tieA4U9Yozu5tbrOEUOyqvphrBTbMtWeXOcZmq0yfy3/wAp+6uN1COOLpUUkSKjgxeJVAPzFdFot2wKndCxwNolvb/epdzso9ztRtCzKi6dLYFvDlevDn9zjeXCy7XVcXUqRxy7m8qK5EK21KG+o86zdMY/qt/GMEWZtKjIdwrbBt2hYvJK0rlQgJUKAoN8l41RHsptvPNPt5V/PYuySKczyZW/ZTj9zj/Jy5W6WaGLiNjKGzpPTCm6YXzpBO6ELuU9MnJwdSH44fbV1e0sszLkVkPe+F6qkj9QE6bMMbg8a0GlI4jP76ozGSwX1EseBtgRSuoiuYyQjZr29lXC5BjZbjlfhVLGREZCupRhniKCnSrkDBicxkTWeVJYSJIzcXw45cK2SL6wVkW0i8aqJDBtSWx8ajnzFEOm5jfS7jTqGLDK9F2J8KkMoIJPG1ZkAGqM3DX8B4HvoSoUsLFWbE2phTuwkfRkSbm/IVeqAuCMABhasKNKXe/iCi5uOdaRK6fTwx5VKLvGEHG/zqGTShZgQTVZ3KWUMCtR5ozoQE244cBUVo2xCNHjck/aa6pAChePGuJDPC27iGPhN8BxrqNMb4X7b1vhNGeSxrKMM+HfSXbyodTHM8KrDyOdRWy5KKbWwB8QXnW2TaAuDEE5k8BUadVFkBtzqpVZ8sb5k04jHFr8hUCGR38K5cQKdYpOQXvq0KiiwNAlTgDQJ6ZOBe/YMBTCONe3vphoA4UCyjLPgKioWRf8KGeLfAURbNjc0bjnQC/YaVnIwtc8BUL6sEy4tRAUZHHiaKXSxN2t2DhR8XMU1xQLDLM1ALHnSEXyx7aa18z8KOFBX6S8b3qenbI/OrKBIGZoK9JGag91Tw8rU9ycsO00NI440HuySD+XjzHCiunM58b0xso5CkZdXibw2y/xrtcqN4xyXnx+FRRbBs+FKrMx8Xh/CKYqxwLUEN1uR5T5gKW2NiT2G9GxyYnsNKyAYXNuB5GoH1SLk+A5m1IJ0uCGBJGoFSDgcjhwohVYYjsIr5l0nZ7ToHv7e73ZxJtdhuOono08UYCxoJ9jt97tyFGCgTRuMPx0H0wzxnOQEggWJ4nIfGj6mFsbcq+be1dns+q+826/vYVln6js5upbUyLcxxfqxtdo6A5H0IlIPaa0bH3vv5eq9Phl3W0lTf8AUJOnybDaxSSDboPV9Kb9ertC7ERqWSwzNsqYHvlmRywjYOVNmCkEg8jbKmLO2Y+Zr5l1TrA9ue6+ubfon6bb9S6n/Sv06PDfbyPL66PLuXRoxGpZ1vITcmwAJNdnq3uXqvSt/tOgbneRR72Tbvvd51KPp+53CInqelHDFtdu0jXJvd3e1hzNMGXtCZGz7szSWkAsHw5HKuV7X6tuutdM/U72EwTRzy7dm9KWFJVjaybiKPcBZFSRSDZssRXl9p7t67u+pbPaS7nbbHcbveybWXpO42kiSRQgS+nNDupZEj3LHQjEJmDhRXvdTgWYkDsyqDEWDG3IGvFTe5Ovxezeo9aM22/qPS95utvJaBjFNHttw22Fk9W6FhZr6jWzqu89yL7nXo/Tdzto9puNnNvUkfbNJNEYHji9MD1o1k1s/HTag9SVFrY49tKAU8uI486+fn3l199wem7sRdI3+y28Em/B2O66gH3E4aRYh+k1LEgQKWuxa5sMr17DoXUZ+r9H2XUdzt22W43EYebaSKysj3KsLSBWtcXFxe1QdHUG449udITpvc3Bz50xjU+bHtpL6chqHZnQDXpxF2WmuxFwPnSk3xBCc7/uqq7/AEgkduXwoq4tbNvgKS5PlBPaThUW3E/On8XAioMckJZ2Lt8FwoBWTIAj7atk16ziKT8zmKy0AdThkeRo0pVznY0miRfK/wACKgspWVWzFJqkHnNu22FHxnEMD8KAEMvDUPtoARtkBems/wCL7KRkY43APMCoolF7R3GkaLG6sQ3OofVXzNccwKI1HEPcd1AoL3szWPdgaP5nMGoUYixa/wAKQiVPquvdiKCMsnmAF/vpQb/RYjPGrBqIuHw7qVkY46rEZG1RVLxKcRqU/Ok9SaM2Y3HAsM/jWga72ZrH76DC4szi3bago/WKDZ1IPZiKQmNzgQgPHiDV2A8LMCOGFVtCjAgY9hFBW0MiY/zV5DA0Y5UBsMBwJzB5GlEbIdNmU/SRkarl28ji+oo34r3PyqDWWA8YOH1Chrs1lF1bInKucDOjWdrsMNVrX+dWa5wNPqYZqCMfsphWxUOKOcsgMrUUsqlcguFYjLIQHD3IwPCiYnLXkJIb5fKgu/URqNIYEg2vWKeW5DopYhrXOWPZWgRqhJYFVzHOq5QHjkAuq5jDE1BRd5Hs7EhjiqjAGjIvpoUIJkbBeVudWQ6AQS2a/I1nJaeVUViTjj2c6CxBpQ2UWUXN+Jq5A8f50ltbjL9lBowGMKEmwBbsFNKoDIpYk5t3UUFR2UlmwJwHbVpW40KSeBPCiVLMFOCrjpFWWtgKilChcqlGhUArjbnpO7kmZ4N28SMSwQEgAnurs0Klku8z7xg6es8cLw7iQyyROV1nMiwYffWbrrTrt4VglaJnlCllNjYg1ug/mbn/AMT/ANRaw9b/AJW2/wDHX7jXJJP5sY07NfSzjpHUhZhv5L/5j++tG1m3+3nXa760qSYRTjO/4WrqDyjurNI67mVI4TqWJw80gxVdOSX/ABMeFe/3OPHrcyTRIvZVdSji6tgRWHp8z6p9pIbtt3KBua5r99a554ttE00zaUXE9vYK5fRi88m43riwma4ry/8Anznl4x+K8vR1qFGhXQFZQc8+dVtqU3PiU4GraDC4IqDIV0yrpawP2UsisrMznA21ED7avZVkIVs7Y8waRC13jk8VgAe0VRjnjMTB73U4/HsoB0eElrlycuQ5Vaw1L6N7gcDyrMGKXlzU3jt++iGhj8UpscPq/fWg3F9YC4W1ChtcEkOYNj9lWkWvpxFx4alWKmALAE6hzWk0DW0i2AXDHOrb+J3jw058qokYBACPGxxbtPKijtiyzoz38TYdtdoISMbHnauRtFI3MR84U4Xrsgeoxtw41vjsxy3Qm2Cgg5ADhRWIMbscuHbTgFcsScqJKgYi1VEa4FhaoARiRjQVb+K/dRJYYDEmigWOQGNQFRRFxmMeJpS4J0r8TyqCFxkLE1AoGOZOZohVA+80pIOCjHnQFiq5/Kl0l8WwHKoIwMTnzprEcaCWHKlOkZ1LsfLj2mpYjE4nnUULX4WFTSo4UbnlQLjtvQTSOVA6RwqXvmbURYZUC6b9lQIo4fGmoEhRcmwoBpFIcTZfnUL6sMbchmalmt+EcqD3oJGMmfA8Kli5x8oom7G3H7hU0lPJlyrtcokAixpblc8RzpgwOGR5UaBTYih2NkeNA+DEHDlU1qwwuagU3U9+F68lL7Aj3UPVYd71vfblOsPHuNwdO2jZNxCI1i3ELQwqUZViA5dldjf+5uhdN3LbDqO/i225CeoIZCwcrhiihfHmMFuati610x+mt1Zd/AemoCW3msCMAHSQxORDYWON8KDnbf2iNr1mLq0PVd0scO2Xp8fT9G39AbNDqXb39L1LX+rVq7axy+wtrJsdr00dZ36bLprBulbdTt9O2srRi35N5bRuyj1NVgedbN/7lhm6D1Hq3tubb7+fp0bzSwzmRAFjQysjqoEisyr4brY1o6L7n6J1oQQbXeQPv5IVmfaoxJxVWcRlgA4Qmx03txoOUnsPapH1Hbjqm6k2vUtnF06TbSR7Z0SDbqY4AhaHUWjViASTfM41qj9qTRJtJU61vv6lso220PVG/TmZts2k/p50MJilUFNQLLqvjeultfcnQN9vT07Z9Rgm3YLKIkbzMnnWNraXK2xCk2rke7fee06Bs94djudnL1TZIZpdluXcXVUL+lriBCythpVjjTUdrpnSoel7U7aGSSQu7zTTzNrlkllOqSR2wFyeAAAyAri7D2Uu0h6ftN11fedQ2PTJI9xtdlP6KxiaIlo3LRxiUqhN1UvYV1G9y9Dj3q9Ln30cfUmCk7LxGTxIJLhdOK2Pmy+NP0zr3SOter/SN2m89CwmMd7IWvZWJAs2GIzHGiuJ1H2Ntt9t9/sB1LfbXpvUZX3U+xgMOgTyMJWkR5ImkCmQaimrTeti+3tx/Wtr1ybrm7ln20J2phaParHJExR5A4SFWu7Rg3Ui3C1TrPvDo/R5v0ckp3XUNaxjp+10tLqcawrM7JGh0i9mYE8MxVsnuf25Cdsu830e1m3UUc6Qbm6SIkwunrqR+Ve9vHbGmom96ENx1CTqfT+obrpm63MaQ7xtp6RWdI7+nrWeOQK6hiA62Nq37HYQdO2UHT9qpTb7ZBHEGYu1hxZ2xJJxJrkQ+7ekze4t37bWQrutsIRGbSESSS69Ufk0ro0ZlrG9X7j3R0Hbb7+mbjqUA3pkWD9OCb+s5CrDqAK6ySPDe9QdUqFyJJ5HGhrbK2ntFQaj5hoH4acE2wAt2UCCNG8R8R50xuMGFxUOnkVPOpqI4hvsNRSWIxTxDiKI0sLrgeNHwt2GkZSDqyP4h+2gpk1h2sQe+k128wIotKdZBXH7DQu/4R86y0IYHI3qUhRjnYd1L6cgyk+FqgsNIUGY8J5ilsw89z2ioBGe/toIWZc7MOYzoCWM/V8KfSvIUCinMVFLrHC5+FIcTdVIPOns6+U37DQ1j6vCe2gTXIPMot+Kj4ziCPvpi68xVZ5x3v8AZQAxte6tY9gwoDE2YkHkaOt8mAXtzFRk1DxNh2VFBo4yPF870mpFOlseR502kJ5hqXnnRLJa2BB4CgU3IsE+eFAepkxA5UNTrhbw8GPCiU1DxG/dUCuqEWLEnhSqS2S2IzvTBlU6QLt2caDK58WRHAcRRVcqrnJ4ua8xWeTblAWTCI4kcRWwMlvALk/7Y0irY2kNxmo4UGWJhfSgBvgdX3in8Awx9VTx5UJ4j/NiHhHmHMdlRGFgF8QOBBz7DQWFAHVmJdjkDUYW13xLLSxsApBN2Q/GjK4jOo+YghRUVzmaw9EYvIApPKxqyMaSZBgUwJ4WGYFUwgl3dsSLsB28K0AD10VsVABZRlqoiyM2jdxhqxLH7qKAJHrbztYnn3UPE7lLWTEkVaoFgq8vEaimW+LHAn7qNGhUUKFGhQChRoVFZYf5m5/8T/1FqvfzbGGEN1DSYi1gGGrxdgFWQfzNz/4n/qLWDrqq0O2VhcGdQR8DXJZn7uNs8l+lUN77dGIVPijH7xXR2292W5ATayoQMo18Nu5bCgOm7HSPyVy5Vn3HRdrINUAMMy4o6YEGvXl9iX6rn2mb4i3qHToeoKgkYqY7lLZY/iFNtmSMfpSojdB5RkRzFWbSR5dujSfzBdHI4spsay9WJhii3a4PDIovzVjZhXl9rnePLpds4+K3y3UKitqUNzF6ldSBQo1KgrdQTqt2VS6sG9RTcgXHdWmqiNL9hBuKDJJaSI4WkJuDVIAk8Oa2xtmGrQt45XNtSrj8DVAVQzlTYsQaor20jReopxXiK2h1ADg3Qn7hWNcZHFtRcEgjgb0CzRXN/FiCppRaxJQEYGQ5cwOdIpEkhZ8QmCrwvVfq+o1lOkAWYcfhVqp6gES4LxIqC3aK77iMr5dVv+yu9ZLaBhbOuTtbJPH+BTZTXW06j+2tcdk5IARjn2dlKCHOOA5GixJOnhxIpjpAAz5CtMgwAyz4WoAMuePM1NJXG+PbQvqz8IooatRsMBxNN4VFqBItYZc6AjvjlUA06uwUQpGR+dEkr291Le+eHZQQsRha/dQz83yp8AKFych8aCXFLq5Y1NAPmxqWtkbVFSxOZ+FS1qGo8Me6hduItQMbcaU6eAv3UCyDM3NDxNw0rQAk5LnQ0C93JY8qawGF/gKl7ZCggB4CwqWHH7amJ/wqaTy+eNB7xSAMLknjRu3AfOppI8pt2cKmq2DC33V2uUCpbM27qXSV812HOrKlAoC5i1ArjcYGoV4qbGoG4Ngag8l1jdD/AK29vs2y3cqbKLepJuY9rNJDFJulhSI+siFBfS1zfw8bVwNG63x3u423TN6Ytp7jj6021m2ssB3G1EaRs0InVFd1k/M0ZnT3V9LYrkTj2Z0up8iMPxGqPEb+Le9c3HuDrGy2W4hgk6DN0yJdxE8E263DGSRdMMgEmmMHSCwFyxtWaWDedf2XtzpOy6fuul7npKa91udzA0Cba2yl2noxOcJC7yD+XcWFzXvmRsyb066bXAqZHz/bQ7ze9H9s+24ek7rZ73o+42Em9mliMe3242VvWeKfyS+tpKr6ZN9XitjWPqsHUNn7T9ze2JOlb3edQ30u/n2zbfbtNDuV3UjTxTGYeBWjUhSrENdfCDhX02lLAYceQpkw8P7l6kmz2fSve2wjkml6K/obzaMjR7g7feKIGgkik0uret6ThWHbXovbfTZej9H2uy3Xj3bap9/IDi+63DGbcN/vuQOyqv8Apboz9Ul6sIn9eaZNzPD6rmBtzGAqTvBfQZFAFiRhnnjXZsF8ROPM0HyfZSbnc9P33VOpJ1CT23Pv5eq9Ql2kW0/SO23mDieFmcbz0R6CMw0XwNsK2bzp+8Tce44ZZerTL1yaTc7OLp0O3k2u8hngSNI5J5tvKYSttB9RgAMVr18ntD25LI8jbG0cjGSTb+rMu2d2OtmbarIIDdsTdMa7Nr4KuGV/3UyYeLj/AKz0Tqsu32Wynmm3uy6TtdrLpaeAHbM8W6/UTroCmNH1Xa2rhyrjwwb6UmPpe26iksvVzvJOkbralunEpvS8m7j3jKjxghfVH5hN8NNjX03SoFgpHdUJ7W+ONMrg5tc2ypCqnh8qGo8x8amv/a9QSzDI/Ogb/Ut+0Uda1NSnI1FJaNsMqmhh5G+BxpyAc8aXQOBIoMMwbW2pf9S0gldPMpZPxDG1aJPUDtkaqOm/iUqeYrLQrLG4urA01xwqho7nVGRf7+8Uuo5OoQ8xeoNFKwX6rfGk9MkXEhIqaCOAbvzoFOkeQn4Y1NUv4Ljne1PqIzX5UNa8cO+opNRPmOn4UdCnM6qa6kZ3FVsIxlgf4aCekq4p4TULMvmGHMUPzvptb+L/AApcf+Jf4ZUBMkZGd+wY0lpB/LFhyan0xny2B5jOgdSYkhh24GopRibOTflwqaAmKWHMcDQMgfAL/vYUPTYZnWPwmgglDeEC7cRSlGGJN1/COFOSjDHwkfAik1vl9P4/8KgYlAot8LZ0o1Pg3h7BxqBNPiTEnPtqFg+CeYceVFKxWE3HlOYHOgyFxduGKinUDHVi3GkuQfT4cG/ZQHVqFhnx7KyvEIiSMuP7602EZv8ASc++s88y38I1EZjhbmagqklVSJJM2Fltz7aqZmd1MuVsSOVVspRyrm7N4l44VcoZILqLAk6r+YVRVGSBcYAnUeZFXQ2Uyqo1O5wNVbRVkka5uoFaIjaP1zzIt2VKDCCA1/OcB++rYhZB9pquMaFErfX5vjlVq4KAaijQo0KihQrF1Ldvsxt3XFXlCSDsINbFZXUOhurC4I5UEqiXebWF/TlmVHtcqTjY1fXGk6vBsdzuEmWTW73GlcNKqFHKsc7ZMydr4Vu2x1etKAQkkl4yRa6hVXVY8LisfWkdoYCilgkys2kE2FjjhVZ9x7I5pL/u/wCNbdxvoNptRvJdQjOm1hdvFlhXLe85zleNltzIumMZQb/agDxNl/y5P/2aJnlmGnbRsL/8aVSiL22azN3AUmx6nB1DX6GsaLE6xbP41kn9wbSGaWAxyu8TFW0qLXHK5r0/l+5beM46mm+XTjjWGNYkuVUWucycyx7Sca5HWtwJXi6dF4pGYNIBwA4VRJ1fqe9/K6ftjCDgZXxb4cBWvpnSRtCZ529TcNiWONPt/Z5S9uemNcepbnSOii6UVeQAo0aBIUEsbAYkmvcChWLY739XPuVU3jQqY+diMR863VAtI5AIY5VZVU1tBvkMxRWdvBMrHJgdXfwoMgWdFPldT8xRmuiKTjYhr1JziGGKgi55XojCfy52UYHgeV6afAAkh2+qpMCWfTwUXb41WxSRksLBravhVQqoxUYETSHAjlWmN2gFiNQOGoffUjiNy4JDeWLuq3Agk2JXDSedSrGjbOjzRBDqAOPfXWN0FlxJ4VwdtFp3EcoNvFmD99dpXdfE4uTw41rjsnLdYLKvM/toWt4vqPCl9Rb6jg3bw76iuHyOPE1pkb3Pjw5CiccD8v31LXyy4k0PLgmNFTTpxv8AuoaieFu2jccTc8qJuewVAPCuP20Dc8MO2hZR5c6l24i3bQTQBjfGhqI4au6jdOJvQMijIE0E1E/w99TSMzjSlpGyUAdtKYyfM/woGaRFwvjyFLqZuwdlEKi5ECjdPxUUoFsgB2nOpa+ZvR1JwH2VNY4A/KoJYcAamPAWqaj+E1LvyoJY86FuZqeKhZqD6BQpRKpwGJo+M9ldrlArbFTb7qX1QMGHxGVNoHHHvo2FAt2OQt218+92e5+q9P3nWhst6dHSNqk8G02mzbea5gjSyp1CQppgW2m2mRTpOqvoJW2K4V5/qHtHpXUJd624k3UW36nj1LZbedo9vuW0LFrlVRquUUA6WF7Y3oMsvVt/1Xq+06J0feLsFOw/qW93UaR7iVSzpEm2jE2pFILEuWUm1ud6x7HrPubqb9J6Funj6V1TcQ7zc9R3KJFM6x7SdNvGiR6pYVlk9RWcHUFxFuXSh9k9H2sEQ2u43m33cLSuvU4p7btvW0CRZH0lHVhGnhKWGkEY41a3tLpP6babfZifZy7FpX22/wBvKV3IfcHVuGaV9fqeq2L6wbnGg85uPc3ubaw9O6TvI5Ieobvc76Obc7TbrvNwu12ZXRJ+mhMkayyiRL3uqjHTiBSN7k90DaGFYt14N8IH3/6ILvm2Rg9Yzx9PfzSJJZGIUi3i016Q+0ekrtIdspnhm280m7h6jFMw3Y3E1xNM0x1amkvZgwKkcMBVUnsrprGCaDdb2DdwzSbp99FPbczTSxDbl5pSpvZAAFtptha1NBwo/cfX5Oizvtd4d/uV6r+jaaPaqu8h2QjWR2/QMIy064nTp8p1gEYV2/aHVdxv16pHut8d7+k3SxwNLt/0m4SNoY307mDRGVfWWxtZhiKkntDpkkcSJud6m4i3Z6hJvknI3Um4MJ2weSbTkENgoGmwta1dTpfRNr0r9RJHJNudxu3WTd7vcyepLKyL6aa2sosqiwCgAVB5b29LL0nd+4d5vusSTwjqm5jXaSRwIJpRt4pRpZI1fXpUgKDbDKpD1rr0HR+ke695vY9xB1OXZ+v0lYUWOKHfuscYgmH5pkj9VSdZIaxwFeg/6b6YOrN1dTNrab9U22MhO1/UmL9Odx6Nv5hj8OduNr1RtPZ/SdrLt2Rty+02cnrbHpssxfawSC+l4oiPp1HQGJC/SBQZveu5610/pZ6x0zffpoNm8Z6hH6Mc19s0irNOnqA2eJCWH0nG4rndS691vpsvuPqe33o3fS+iiGDbbd4YlWXebhUuryxgN6UPqxsbY3JF8K7fXpesif8AR7XpCdU6Vu9pNDOFkRJBO/gRZPVdFEJQnUwDN2UOh+1dn032tD7a34G+RoWTqDyXPryS4zMSfFiTgc7AUGWZ/cHTOqdO6Tuurfqx1kbiFdyNtDG+23EEX6gPEijS8RCsulwSMPFWCHrvW5faftrrbbtV3e73e02+/VYo9Ey7jdDbvYEXQhctNeg6f7b2XT92m+O43e93MKNDtZd9OZzBG9tSQ3C21aRdjdjxNc2b2D0eWGParu+oQ7Tbzru9ltYd0Vi206v6okgUq1rMTYNqAvgKKzb2T3ZL17rPTem9THpbXZw77Yxfp9uZDNO86rtmeWyel+ViT4v4q9Xtf1J2m3O+VF3ZjQ7lYrmMS6R6gQnHTqvauZtfbUW16zJ1xeo76XczRrDLHNMjQtGmr00KCNcELsRjnXYs34vsqUQqDwpTGnKj4uYoXbsqKHpLwJHcaGgjJz8cabUez50NfZ9tBkkWYObMD3ikvKMxerJJQHa4+6k9ZKy0Qm+ai/xFA3IsDccib05mj5/ZSmSE8RUFRWRDdFK9gNx8qH6oLhMhX+K2FWaoeDW7qBdLW137xegn6iI5NfuqepqysP8ANVZWPNRY81uDQLyr9Osdoxop/TRsWOo/KiI1HlNqq/URZOhU9oo+pAchc8hUFlnHEHvpWcr5h8qQ3ONig78aT1IlPhLM3fegYsH+nT/ERUCC9w+o/wAWNJ+oP4Wv3UrSS5CO/LK9RVxJt4luOYqvWL2iOPJsqr9Pct5hYcgbVNO6UWUAr+E40FhUHGUXPAjKjcqL31L9tUapuAN+QP7KXRITqdSTwANBaLvcxmy8QczTAA4p4WGYP7azNOY/5lrc8iKVt2r+Uhv4lONQab+pgMCMzSySRIulzYjgM6ytNNIB4lRRky51ETU1rHXzPHvoomSXc+A/lpxvm1RAiqQQSBhYYaqt0FvKACvmPPsoWLWmOSZLQUNC8iFbAOviB425VQzaopGJN2sF4d4roMdKiXic/jXM3g9KdUHlPit2moLY10ksvh4sOy2Aq5D+WwbDC6g9vGqpBdGF/KFJbnVs3iVCRa1rCinTGEA8BYCrRgBekI8ajgRc/CrKgFCjQqKz7yJZobEamRhJGuV2TxACuL07qMm3iUzBmSbcSRpHbxL9QtftOVdDq7TIm2kg8yzrf4gjGqJ9mm/aLf7ZvHCxZ9vfwl1wI7G4XrNmum/9h00ljlF0a9sCOIINiCO+laGFzqZATzIrzsc7beHdTyD0N286tEj+fS0h1AcxY411v1ksLbaJvzn3EetS1ls11FrqMvFyqd/S6X8pmjRJt4NDflrkeFc3qg//AAaK+X5VdA7tC8m3dG9VbK0aeLBl1AjLhXIbo+1Mhdtw40vpMZ1EggarEYnKs8pOV43tP06/BXS20aJOQihQYVJt/mrH06ON991DWoJEzZin3+z228VGM7Jo/Luuq5IF7FbXpun7WDYKypI0rO1iLEte2rK18qs4yc+XPtNYeI6AVVwUAd1Sql3Akf0o1JlDFWRjpK2GrHPgaySb55NruZ4joO3RiVte7qxUi54YcqXlx8+PxG53VBdj3DMm5sLCuNveqkCGaEkw+qFkQDE6QHPxqvcbwetspo/zJgJJJ4kPiIDhlB+WFDYdNcCPd7ksq6mddqSMXc2F/nTW+yf8wy1dEgEe1MpXS8zFmU5gE3H2GulXL6RuH3M26lIspZbLwBUaTb5V1atWbBSsNQIPGjQJsCaKzjxRyA/SNNZ/E22YL5ePOr5D6asfxC5+NUNqjiKLiGWiKTpZDY2TR5u3lWfaeOVkbDl2catZgsY04qFIA5nnSQKVAkGeY7eyqjoAXWw8y4DvoFRIRpGK51NYsJBxw+NKx0nSMf8AmkVlpdtgsm4jcDwK1gvM11GA8wGHKuTt2ZtxGY1sgNj/AIV1ibYi1b4bM8i2FrsMOA4/GgUU5juAptTMbxi54m1AI7k2OjnatskOoYarjsP31PUfIN8BVohTJr350SqLmoPdnUFBkfnbtoepIe3ttWgLfEZfho4cVt2igz65TxPytUwPmJ+Jq84ZN8KBDHMYdmdFU+AZWv2GmFuFPoi/DY0DEhyuO6oBhyNTw/hNQoy5SfA0NUw+m9Abp+H7KmpeX2Uvq/iuD2im13yIqCa17flQ1r2/KmueVC/ZRQ1rU1rzo3FC60E1ihqFHw1LCg96QCLEUNJHlPwNNUrtcpNRHmHxFEMDkaBcZLiaUoW82HdQMWA7+QpfEewfbUClfKfnR1W8wt91QcbqfuLofRt3Ds+obh4d1uFLbdBDNJ6pH/DjMaMrSfwDxdlVS+8fb+36e/VNxuWj2cMv6fcyNBMG28lg2ndRmPXDgRi4AxHOud7u3Um09w+0dzDtJN+0e43zHbwafV0/pGVnjEjICVByvXI65ses7vpnujqCdH3bye4TtNvtOmqI2nWPaoAZtyPU0JrxFtRNrXqjtdV9+dK2uyml2Deru4nhBh3UO5gT0ppAgnZvRJEbDVoe2lm8N66Le4ulDfbrpytuDvNmrS7japtdwzCNdQDjTGdQfSQhW+o+W9cf3dtP1Xt/ebzpnRN1L1bq0cMLpHGn6lUikEirPrk0qqgHI5mj7lh63PLs+u+39rKN/udtN0zcwyaY5Iod0NcU8o1EX20y6iATgTag6EfvL25Ns4d7FuJWi3Ehh2YTbbgyTuq63G3iEeuUKAdRUEDjWfrHvDZdO6CnXtoG3u3lmjgiWKOVvE0whlEgRC0bJ4sGA8Q051i610fc7KToG16bDum6T0uCSIS9MSB9+koVIo9LbnyRvHr1lMb54Vwth0r3WvQOqdGl6XMI49w3UY23Esck88r79N4IkmEhV29BTqJt48L1B7ufr3S4NjF1LdSSwbed/S20UkEybiSS5AjTbFPWZjpJsEyxyrT07qWx6vA246fMZER2ilVlaOSORfNHLFIFdGF8mFeN6tsuv9X3Ow6/Ptt7Fttlu92Iths5E2/UI9pPDHGkuDlWcOjXXVfS3wrte1OnT7M9T3su33MJ6hNHIh6hP6+7kSKJYleexZYzhZVByAvjQZdn7sTYJ1Me6dxHH+h6i3Tot1ttvOIn/KhlTWqmfQzGbSLt4uFdbde4uj7SPavPJMJN4hl2+1XbzvuWjW2pztkjaVVW+JZRavCNsOvr1XqnufbdL6k2723VF33TelTxxfp54ZIY9rMdIkJWaykq5PgsLZtXX6lsOobrrye5TtesRbXebGPaNt9hIkG8gkhlke08XqWZJBJcFWwIxzoO8Pdnt5tzstpHunll6iA2w9KGZ1mU+YxuiFTo+vHwfVatHTeu9J6xuNztdg8kk2zOndK8M0Qjf/ls0qINdjfTnbHKvHbrbr0X2vHvYwqdW9sbw9a3Gw/UDcbhY91LIZYZpL+fcQSt/CXyr0XQYeo9J2fTtvPs5J9z1WWfe9X3aMgTbzzD1z6oY6mGIiXT+GivQ6F5VNC8qlm/FQt/HUB0ryoaV5ULj8RoYdpqKbSvKhpHKhbkD86Gk93xoKJFXWcBS+HsqSRXdrn5UnorxJNZaMSg5UhaLiV+yp6UQ+kfGhaEHBRfsFQAyQ9h+FKZI+C/IU9/wpYduFVvKqDxuF7BiaAFlP0/fSF0A8wHzND1g+Qa34mp1CnGxY/IUVQ0mrBNbnsGH20npS2xBHMjE1t8eQUAULPzHwFQYxt1OTOTyamCTR+VVI7RV7Ip87XpNIHkJbsONBWX3BFiqgcccaQPInkj7xer7sM0C9udQkcXv2Coqn9U97NHpPaaBnma+hB351dp1YBBbm1IUC4KxY/g4UFBMxNxg/MC5pS0t7SyEdwrTq4P+WOX+NEC4sosvE8TQZfQ1ebG+QzJ76V9ml7gDV+BcK06SpIh/wBV/wBlFSPKvm43zqDJ+mfOOS7ZaWGVVvrj8LJY8WGXzra4t5f5hyP76GoKNJHjPA8TRWZXRwFW4t9QP2GmZgGupNl86GjJtUALp4JOJGRNVK8kItKCObZj40F2pS1r+C2ofurnTK0t3+pSTfkuQq6VrJYC4kN8OFBQhR21EAgKRxNQBSHXRkukae8VdjOlstC/M1m29zNok5C3761xE6ZB9SsR86VRQl01fV9Pwq0G4vzqmP8AlaMmB091Wp5bcsKgNCjQNRWfdNHoETtpaU6Yzn4wNQP2V5+CTc9OeOAlV3Uu4YSIcQUYYG3K+Vdjq23eeKEobGKZHJ5Dyk/bWeI7bqqhn8O727MsctsdSnzLz5kVm4t8UWetsOokxbiMF4XKXIOkOGK+Fu21Vt0qVdwNzDuC2hT6EUniVTcNYdmFYJ4N3sNrLDMrSybmZX9aPygh9R1DhnWoTqs+z2+0kC+rHaf0yCQwZBiMbNieFZvKzTlPOvw1AiTqUO4n3+524dzoAjhOYClML99Jt9y8cu56hNt5Y0MtylrsAY9GXfW5d1N+rk2SkM0ZQiRxmGUsbhbY4U67xnlk26oGljfQ2Nl8vqXvY8Kn/nfZnj/qv5/i5u23yepuuoFJfR9ZWtpOq3plPL30sG9Lz7rqEcErxhkIjIs5/LKYA9prpruy7vCkYM0b+m6arAXXXfVp5dlRd0XmbbIoEyMFcEkqAUMl7gdlMfb8/Rj/ABPzcyBuou+532323pzSONEUx4FQhOHdUTpG6l24SfcGEuzNuEi8raiWt9tdCLcPPuZNpfQ0TFWdRmNIcW1X51z5d2r7LeNuHUSRq8cJYi5ZXYAgfiwp24TaZ04/L0GpI9htZ19JB+olsiuRYEghDj2XxtXJ3O9l3Z27xEDcrMVVOYQcr5FhVhbc7ubaxbdGWbaq7GZx4NTtrWtO32u26eYfXIbfOdIkAwBc/wCNXW68tJ4+Yv6VGIduYGIM6G89vxN4/wBtbq5PRBM53G4lx1tpB56CVvXWqrNkpH8pvT1VN5PjRWecn0WvmLAnnc1XPf0wOJIUnu4VZufHptgLgHu51VKRqT8IP3VUZd3YFdOAxA7qsS3pLYeNcxzvWfdHU9swp0jvzrRLKUQxm1za1s6IHraPDwOC34HnViAlbSGyjNRmT21nWO3mGuQi6nMgVfCLuBMbnhytUqr9prfcRquCg4MeXZXbESpicR91c6EATR2wscK6Y8YucuVa47Jy3S18sB99SwIwwtUvpwPwoEE4/ZWmQ1X8P28KgUriMfvpjYjHKk1HLh+KiidJxypbv8OfGm0DPjzqarebDtqBQq5jPnR8Q7aBx8o+NCzfViOyghdcjnyqaSezuoixGGVTTbI2oF0kZG/fUueI+VQtbPHuqar5D51FS6nA/bSFFGINqcqTmaGhRlhQV5fTcc1ogg+Vrdhp/EO2kYA5ix5igPioXPEULkdo5iiGJyxoJccqnh5WqXXiLVLDgaD3fqE5KR2nKiAGza9PQKg5iu1ypUpdNsiRSlnyWzdtA5IGeVJct5cBzNTtcEn7KOoHI1BzN57c6H1DeRdR32wi3G9h0+juXBLpoOpdBvhjXRJYYk3HbTFrYDE8qGm+LYnlQJq1Z3UdvGjcDwriaJN/CM+J5VWVA8KYMczQHQCc8eLUblc8RzFDQVFj4h30Ro5WPbRSlkbFWseyl16Tkf2GrCqnsPMUPEMxqFQAOWyFTx8gKXTxjOPKiJAcG8LcjQcvce2+jbrqP9W3G0R96fTLyXcK7Q/ymliDenI0f0llJHCuoVJzYmiWXv7qGrkDQTQKmleVAseQHeaXUx/woHoUtnPGh6d8yTUUxZRmbUhlThj3UwjQcKhKjsoMckjl20oe80n5xzstWyMxc6V+Jqth+NvgKy0Qi3mYX5Z0DqAz0jmcKjyBPCgAJyHGkCEnU7Fj+EC4qAaTJgl2/ibKnTbxqdRGpudN+dwCgdv+FD8z6rnuoGJAzwpCUOQv2ip4BmpvzIvR1pzAqKTTJ9J09+NC0v1HUOzCnMgPl8XdQs5zOkdlAmqNfNh2tU9TV5Bft4U2hRjmeZxpG0XsBdv4aA6WPmPwFIVjU+EeL+HOjokObWH4f8aGr0xiuHZUUumXibjkM/nU9RR4VFm7f30devPwDkcDROhVta4OQ50ClRa8hv8AcKTSx8hsnI8ab0iTqJtyXhULODa1+ZHCoBrA8FtJ+ygwUC5z4HjejdLG57750oRr6x8FNFAakxkxv9Q4VLCTxnL6f31C+o6D4fxVGGgXTDkvCgXUdYV8l+rmaWfTpxy435U66baWwPEGsTsZJDbGFP2fsqCloydUqjPAR8bdlRH0Aq/iubaTwrUF1FAuZxL0kkAaPUg8d/EDxoM1tExPldb3HZV6F/UFrMbaj21mDn9QUbMjQQe2rdRjYSqbEH02U0F6MGnZhyyPGrUNwbczWdyYnUkdtx251epGLLiCcbVFPQo3viKFRVUzRWEUxsJroAeNxlXnZ9rJsJtrs11lGnMnr3sCGGnSxGRr0MxQFWktoActfKwXG9Zf1O2k07c/nw7hNcJHiumOrvtXneU7dbpiZlMM+36m9pzOAYYJfSL38WLlFJ4Grf03Tdw7TquiWRcXxRrH6sbfOsk3SZE2xh6a6tBI6tKr4nwtq8LfvquWcPvdtDNE8abWMrMZB4CAyYg8RhVzyntmvyk/uNMfSp9v6ssG6Zp30lJJQGtYEWPPA0kW06nB624LRS7p31gWIQjT6fwowTpP1CaGCW+2BRl9JrC+hibFe0VbFNNJuptqJGVY5bK+BbT6eu3iB41ntwu/H6c3/r4PzZ4IOqRjcbpo4v1UkiusdzoICGOhDtuq3n3T+lFupGUoB4kwUp9xrVHPNLuJtrrK+lKE9Sy6ipjL2ytn2UI5pZN5Js2dtMTr4xYMQYy9jpA4inbh4+jP+PhfzZo+l7to5m3G6K7iZgzSw+HC2kj5Uw6d03bwos/53pXk1uNRsTixtwvTJuY13k8e6dRtoHOkyG4xRWxLZ41zf1ttjuIlR5v1RkjgMYuoGtiPhY0nK/Tx9JfhTR1Jd5pl28KgKm5YqjKcbI6qewXGVcQNP1BotqdesSu/rcAqjQMeeFbv6duZ2iTeMqbaJGWILfWS/iNX/l7Pbxw7UD0/VG2cHFwz8SeGPClxP33X0nzGjYJBBD+khbV6NgxPEkar/GtVYdifV3G+3HB5yi/5YxoH3VtrUuZLfWZWBVM7G2lc+NXMbC9ZmZUjLNi3GgrmJ1JEuZxJrPMESb0ib6Vv8at8RUSMbHPurKx1AuP5jmxY8BxqjPOxAi1YBvzCONa4I3eTVbxEYX+kVicevPZMUvcE8hXZQKoQJxGJpUir0hGAUwINnahKgAsRY5ratDWBKWvqGVVaSi6ibsuYPLlWWj7CQtuI0mwkU4ciOddomx8PxrgwpfcxG2kXwHOu0smkaWFxwP763x2Z5LdI4586UMTgM+fCoBwY35cqhN/LmMq0ymnG5xNTUP8ACoLtmbcwKBsuIwooeIZYCiADjn31NV8hegVbMG3ZUEIK5ZcqGsHLE8qIAPf21CB3dtApBOWHbQsR5saOq2fzFHVfIXoILcKBAOdAgnECxoeIZnDsqKNiMj86GsDzYUbA9tSw5UA1A5Y0LnlRKjhhQ8Q7aAEE52pTGOBINPcd1Ggqu64HHvqXXiCpqwgGkIK5YjlQfQKUsMhieVLqZhiCo7KIZBgMK7XKmkt5j8BTZUNS86BdRhe55DOgJqs+PIYc6HqIT4mH+Wm9SP8AEPnUACBfKSKBLnBSCOJo4vl5efOicPCv/ZQIW0i1iOZqJbO+JoqL48Bl++iQONAaBAOdKdIyNu6heTgL9+FFHTbym3ZQuwzF+0VLvxFu7Gh4Tmb99QAtG3HH7aVrn6dQ55GrNCngKGkjI/A0CBm42tz401r43vQP8S/EUpBGKtegfSBwo1WJQcLY/ZUJJ4/AUDEgZml1E+UX7agB4L8TQJ4E3PIVFEg/U1h2UuqNcBiezGpoJzwH21Lqo8I7zQZJWlLtgEXmc6pCu5shsOL8fhVro0kjEmy8L8aOlxgGFu6stFSGOPIXJzY4k09KxZc2FJqlPlUW5nCoLKQuowvc8hSlZT5rHsBsKI1LkgoBd2yGkdtD01Pm8R7abU34aQy42CEnsqKPppwFu6kNgbKSTyGNS7N5wQOQohlUWAt8KBdEp8z4ch++oNS/SPhRaZBzvytS3DYubD8P76AGVjgqnv4VAUBuTc8zT6k4EVWXD4KRbi37qiozqTpWzGgIlGP1cxTBI7WFqUqCbIbHiRwoAdd7K1+d6AJUYr8RTBCPK3zxpD6jHSLFRmcvhUC3SU3PlGV8L0W1ILqb8gaJvbxJ8qrGknXiv4aKIIUWcZ5nhS2u10PhXhwvRkl0KTrB5XwrI8xYaYwUX6pOfdQNuZ9f5K4E4E/uoInpBUbxA5KM7DnVSt4glvD2Z2q9GC6mjsoyAc8qBgDd5VytYL3UxsyqVwItVRNk85VjmBkajyEFQylWvgwyNQZN7HqnMiizgC4qKyyBQ2II8fMMONPIzNOxsAykH5VTPG8bHT9R1W5Gg0xMxazeNbWXnamh0h3jBsQbqeyq1cOiSx2EiHLn2U5KSguDpcYrUVahYYNxvY09UqxMQbzWxuOdWhgwBBqKqmVWsr+Qq4bhgVxrjvA+33ezXYqPR20DsrOdSsrMNQBHGuvuE9QenlrWRb962rlGH9BH0/Zq2pl9ZcBbwlbnDle1eHLH8mL6zb4XVfRc+7lfq/6PbxlfSDjcSsBpIsrKcMyK0fq9uzpEZY5fVLINJBOoC5VgCaqiF+p75Sba2Ed+RaJbfdVPT+mbPafp9cIXdKpX1ODMPNY8edPt/TO2P0y4853PzXSbDp4WSH0vTEgDOUBFrXsbrlVK9K2sMTQwzvG0rB1kD+PK3hPHCtsyj1QwJ1GORSBfEFTa478q5buq9FDu7JaKANIouy2fH4it3nrylmcY/wBkXRdKRIpoU3btK7BjKGGtSBp4dlVp0vaJHNDJuXkeQhnct4xpwzFX7eKU9RmkYhYY0RNsinBkYatZ53NJu91Hs5NpPLumgjVpS0CqW9XxHDDlUvPSWcZby/rC4ROm9P223ki9MyxC0spbxWA+on4VbJLttnEp1LBBrWNCq3JZgCLcBnWLZfqpV2UjkLFPLNJMjMFPpMCiJpOedGKXb/poId4nqBN0sGPB1XSrHnlWeX3LczxfTf2kXxbt13272bIXeFDIkxAtpCBghtxN+FVdKRzBLut4FZpyN6FXIFOXdhVmzhkjbePO+vcO0wkIFh4Vsth/ltR2f/5bF/8AdX/7orN4zrzu9xx1VOkIV6fEzeaS8jd7nVWyqNh/8Dtv/DX7qseQLgMTXvdybFkIN7nwgfbWZyZX9JR4AdTgfcask/LjLucTkO+qFbRGDYgHFjzNAJT6jJF9Ixa32Csu4c+oQmYGkAcCauMnpxlrEu7XqnbJrcysNXbwueNEFYvRMcYxONz3itsbflqFzU2J5Vj3BRGXQTa+fberYzg6MSq31BeNKsaS6qw04tkTUuA+eotwFVu4CW1AAY2tjVbzKR4ALjG6msq1wC86K5tZrqK6WBGAsOJrjbdwdxE2Nw3K9dgut7kXBzrfDZnluBumKnVb6atSQOPCLHkaq1qML25YY0hsTdbjnf8AZW2Whgc748qgKcKqEjLmARzvTCQZiwvmKgck5gVPEeypieIoEHMHGoqFb43x7KAAvYjGja/E0CoOBoDYUpwyPwqWAzxHOjYcqAB1PHHlU1CoVB4UMVzxHOooG3C96GphmL9opyQBcnDnVLbhBl4u6gs1E5Ch4uQrO07HEALyNL683HLmBTA1WJ5UuluDWrP+ok5j5U6TuxtpuezCgt8XH7KlgeJpP1Ed7E6TyNEnVkPjQe+LhPEcBxH7qqfcfhGBFwTVFzO174/dViQAfzMuVdrlVMZJbkDVY2J4UybZwDgRfPG1bAAososOQqMQBc0GQ7ZhfxeEZY/bVf6adhiRYZKeNbMzdj3CiTyxNBgPrRHAEGiN1ICEK6vxEZ1sY6R2mqv0yEahgx41AyyBxcNYcrU3g5376zOskZF8LYAiro5A/hcDV99BYLcKNLpXlU0gZEiijQIBzpCTwa9T808hUBKgcbUuo/T4qljxW/xo6gOBFAt3OYsOzGhpjPfzNPqXnSllOA8VACpI/EO2k1lMAL9n+NP6d8zbsFSxUZi3bQKDrzNuyjdVwGfIUhBf6dI5jOl1FL6DccS2dFO38XwUVAt8XwHAcKQMScQV7+NPYZ2J76gzyP42Cgn7qSznzGw5CnkZtZsv20t3/D9tZaAIoyGPOjQ/M5AUhd72WxNQOaQuL2XxHkKUpK3nYW5CjoYYBrdwoBpZvOcOQpgABgLUNLfjNI1r2BLNyqKcm2JqvUzYLgPxH9lD0QcXJPZfCm0L2/OggCrxx4mgzIBckWoMEXhcnIUulAdT2vwHKgHhfzWC8udS8Y5fKjqTgL9wpSzE2Re81FKWjJsvxNsqlowLBTTElRmFFZ5dzbwxks5ysKB20k6VVr8TypS0aC1j3XpFglbPwXxJJuTRaCOIama54asr0FTza20C5HEKf20HaVRcBkHAXpxKUXTEoP8AFlfupfNdnuTxbgKgyFd1MdTWCjALmxNWpt2J0s2k/UTn3VrXSqhltYYKKYrgEzLZ0yrMNuQrPrvfKlbaOqgI4BPC160sg1BVwAxNqBLhwCNQAvhUGV03KWU2YX4ZfKlZ5PCGQlb443FbC4LgHDC9jST2VdYwIviKDlB0ZmYHDUTYmryVkQB18b+EG/yo+kPT/MUEAhmYZ91H9LrDNG3cjcqCmMaCVKkMvmHdxq1XRHZHXwvirdtUT+pEVkYEdtW60kjfHwsBcH6Tz7qKtDegRYXU8KdTHjpaxGRFUh5FVVYhgp8wq1tQKyxi18wMjUCbiM7mMxrM0MgOEkZscf31m2/SEhlaeaeTcSlSitIfKDnprZ6iM1mBUkWINMri+knHgedTE3wrJt+nekk6zTvO87BmkbBgV8tiOVqrj6buBuk3G53kk4jJMcZAUAnC5tma6VCpieJptoMm8224nsdvun2xybSAdVss+V6xt0iZofRbeO2ty8xIHj8WuxHfXWoVMTfE+SsUWymi3QmG4ZoVUokBAsqngDnS/wBPJ3o3UkxkjTV6cDAFVL4m1bqFNPEHLPR0k3X6meZpCg07cHD0rG4025VWOiyF7y7ySRTJ6zoQAGe1tWHGuvQNNPE+Rhzf6buCs2rfSmSYi8uAYC2nTh2VZJsNWzTaRzPFoXR6iYEraxHxraSBmaraQE6VxPEjhUxPE+QrRRBCkCXYIAgPdSMwjTI3Y/E00kughVUlrYL++kRGUmWVhdRnwHdVUjLJK5MgsFxC3yFUSyGQoBYRIcTzq1m1m7MdLDLiay61SMvYWVvy1PHtNEV7liPDfxsfkKuRhGiopIUDE8SazIkm4mLsbLe9+2uikSL5F1N+I0pGOVJHUgA2wszcDTIpJDsxJODWwFapF1K5bHEd1VWUB4xjY3W3I0yq9dvHYFhq78qKqirbSBbCk27tJGBkVwPOnCAMb48cayp9qR66KBezV1gCbqcB2Vy4CF3MRvYE2rqM3EC/Ot8NmeW4BFsVIF+dDQrYEC4pm1ZjC1ArfG9zwrTKtoo8xgeIqsxE+XHmDWgAEXAsahxxyIoMlpE7B3Xp1mPFQe0VfmccDVbRoc/C3MUA9RM7EUQ8TZGkOpM/EOYzoXV+34WNFW2Q/wDbQ02yxHKk0LyqaF5kfGoLLA4iqpJUQ2uSapme11ic9pBpYtsx8TlrcAeNAPTllNwf3Vau3QedjfllVoFhYEd1K2lRdwPnQTTEuRAo6uVzVR3CL5UJ7cqT9UfwD50FxVj2UDEWsWbEZVX+p5phViTRvwscrGoqptt4fAw1Zm/GkUyxkgtYD6Tl8K1gocRaodJpke6WFVGHmHGo0iLhKdJ4UJZTCLeY8OYrI8ms4+I8q7XKd906HSBYHLnSLKwbW7ajwBOFKxObeK+ZzoXUDHEcDQMzDvJoAlcQceyrodvG6lm83ZwrPJHobEf5O3toLFme41eLi1/urUkqSZYHkayRQq1y3xPM0jIytp06SONQdEgEWOIrHMgjOpTcdmYqyIiQWcnUPtq301tbgc6CqKVpBY4MOPOrdF/Mb1leL0ZAQTbMY/ZWhbOoYE2NFPlQoEWzal8R8p+JqBjhS6r+UX7eFDQ2Za/wo2b8X2UA038xv2cKmleQqEsONzypdLt5iLcqCGxwW9+fChot4mbHtoksuGHYKBD5tbsFADrIvcBftNKAzYlfCMhR8bnECwp/FyqKQgcVI7qW1vK1uw1ZqYcKUv2X7qDJJI6u11DDmppfXU4AG/I4U0r3dvyiarNzlER8ay0fSWxY4chlRsALDCqTG3Ale7GhpkH/ABGPYRaoLzSNIi4swqq7DAoCeeNRWVcWQ352+6gOsvkCF7sTRFwLKlhU9ZONx3ilO5gH1iopvGeIFIdROkMSePIUpm9TBMF/EcL0bgC2sAchQQRouJNzxJNTUg8ov3CqzNtl+rUfmaR90tvwrztjQWsztgAFHEmqmkx0pdz2YCqvUkl/lxsI/wATHE91OvrAWjUL250UGjkOMgsOCrmfjUV4YAbizHO2Jqeizm8jFzyBsopliQeVQTz4VBW27Lfyxp/ibP5VWsYkbXK+tj5V4AdtarC9kxbi3KppHlAx4tQIsajKxPFv3UbajYeRftNRkXyKLczQZBhGhI545CoqaVc6yMB5f30qpclwSL5d1FlIARWz+6o+tVsCMcBQKuvFsDf7qisbsSp5YY5U12VchgOFKrEKLqagGpGc3tgMjWfdKPAqmxY5VeGUlrjjyrI5WSa6mwFwP30DEMEYEXVbEkcTVvgZscCR3GhpNmsw0jIGozsdLaLrlfvopJEYxunnAxHOsKx2DNE1iBfSeI41taQBgVBW+BJyrFdQ5B8bK2BHKgiTqsgLDC1jw+dX3EdxE10fEIc/hSSosgZ9QAOGnjhVSXQ6LaGzs2V6g0lnmjDqLFfq44UTKLaZh3NVNn1kq+hmHkOR+NMJlKWeMh1455UVcro+Ci7DlhRBkB0m3ZVelZhdAFccQcahWci2sEjgRiPjUFoYHA4HkahZRmaoEqnCRTcZ1PWRcE8X8NsRUVbrXnU1Lzqn1Ij4mJ1d2XdQMwI8WC/itiaC0uBhmeQpG9Q4A2J4dlVmWNRdU+3GgNTAuX034ZnuoGdhEMQCTlaqzMbaUBBOJYimASLxMwZzlfE1W8slnIQE5XNQLpaNGkLAE4EnE0moNb1b6R4rcPjSFA5XVdjmQMFFKzl2CRkMxzJwUCqEnmec2jFkBxth8KRlCFi4uxA0/hFXaYwrJ5mGJZcqomlZxhjYAWoLNumKFzixLVtDEkaRgTfHsrHCpWWwIQolvFjiav8A87kEDhljUqwWHgcseN7cKrbSknhF1IsQKa4ETXW/8VM+osLAC9RVKM0chbJXz/fWhlOoEm98MKp0lgQSAMiKkbXXQ7XZcjzFBrgVfWSwxvXV1AjPOuVt/T9eM3411AVFxWuGzPLdA3DiKFyDa2ByolgDf4GoSCPurTJTqB1YW41CCcQaIYEZd9C+k2tgcqKGkNUsMmHxoknMDGhfUPLUExXLEUjRq+IwPMU12GYw4GoQ2YsDQVHUmDjD8QquaUqulCDfPuq9nIBLWFvtrDokkfwkAtwqh4Y0c63GkDIczWrSw8rfOlRgBoItbgaqmfT4IiVP1VAZdwUOkC54kcKzalYkvmeedQE3AbLmK0mKN1BTMD50FAK6SvPEEcDUDuo0kBhxBGPdRTbs19PhGRNXLtwvHV30GT/TQ8PaK1PECPCLH7KzFsccxQFXZTdW7LVpjfXgbBuINZbKeVFSUYMOFB72R9bal8xzHKk9Nb4GxObVFQLYDAcaL+BiGGkcCa7HMUtp83+8ONDTfxD5cKIxx+VFYmc2jNj9lA231GQjEKB4r1oZFmax8i5WoEKiCPI2z/xpGl/TgDzXz7KCyR1hUBRicqojjaUlicOfM1fqimS97j7RQkmSJQAMeAFQZyrRtY4EcRWqNw6g8eNYjKL4gm+fOnhkf1NKrpVufOg0TAMhxsRlVG3kYkoMAcQTWgAjG1zzJrK2uOa4AABv86K1BRmcT20aHj5ilJY4A3NQMSBnS3ZssBzoemc2Yk0SoGJJoIFA/fQLY2XPiaGnVzC/fU0ouAHwoJdVxJuaRmBOkHxceyi1l4AsaKIFHac6Bb2wvh2CphzJqyhUUmHBTUueC09KXUZmgyyF9ZsB86X8zsFSSYa2CqW7hSapmyUKOZNZaNaT8Q+VKbjN6HpufO/wGFQRKOZ76gUtyLfdSm5zYjuxq0hFxNh31WZlyjBY9gwoF9NTmpb/ADGgY0UYqi/bTWmfMhB2YmgY40xc3PM0UhYHBfmBalKA+cX5A/4U+snyCw51WxbJczlbM1AkhWMWNlJyRR4jQTbu3ibwjMKcfnVscaRm/mkPGrNLN5zhyFBUVxsGLn7BUZSLa37lqwn6UHeeFDwr4mOPM1FV6HYeJtK8v30LO2CtZedvuprh8SbLy51GkXyqcfuoFs48KsPllQOtQAGxOWFMXVRx/fSqTfUVNz9lQSzIpJYHiTbOgquBckXOJwqEs7YLgvbxoOZCNIABbDOiguskvcchQOsvwsv309nAzAA7KRVa1y1r40EfXa2GJtR8fZSG2sXc4Y0sjKqk2JwvjUFc0rxxcNTmyjvqhTpIUqL5Z52qoSB31AX4LbGw7KvSCZypa0YONhiaANIl21gdgF7CkMysnOwwAvatUe3jUtcXN8zTqF02sOIorGzyOvhBNsRf91UOkrTeUeMA8sq6SgFQCByrJKqiUC2QNv2VBQsUpULpFnNjjjUk207eBhcjyEm/wrQET1BwVhzyNW6dQsWIK0yOWJJ4vy5EwB8pH7asEtyQCQM7NhWqaMuNQbxpmDxFZ1tJpMljGDYEjEXoIku3cAPcMPqvY0TqOK3a3BuI76SSAaS1wAD4Tzqti0VtajScQFNFXmdTj6RUDBja4qCVBZoASD5r5VSNypx1NfigxWgZVOAbwt2WFQaDIt/GjFuY4UrSsTpdSUzNs6zjdTKCAAQvEZUBNxvZmz040wLvWBP5MZZRmDzpSzSH1G8HJB+2q2njAxLWHwuaT1WmPgAXm+NQW+vCl9KAvx51naX1AQB5j5Re5q1YlZjqkBIyYeWgWRY1VW0kXJYfsoqmQzM2lRp+lVGFXJtGjHjcazix5U22WNiZ37kXH5/Gr7xi98eJ7+VKMzxIni1cMf2CsrQguqajji3fW+QhmCDh4mwrPI4JLDNmBXuqBI4nMjaWPiFXGPcANpsRlnRjYDcAWwsSKu1DRxxNKrI3qrGdS2HHiKmoFlscxbTf7q1M35RwNJKqMELJxHCoKVAJYAXuL54giow1WYLZjiDwNR4irApfA5HkaUlkuGUjiLZGitmxkDyR+GxBsRXXJAINuyvP7Ryu6QoTYkHHlyr0AYutxY1rizyQlThQDC1icRRDXGVAkAg88DWmUJAN799Q2OF6PhPKlAGRHdUVAeBzqHmKhUHv4UAFPDvoDmKXy91EqBiPjUtyJoKNyw0BfxfspdspUFziMgar3IPqYE2AyrTEpWNRfvp6AS6dBY5jLvrAL6rOc8zWrc6hYKbXxNZj4hY+bjVg0NCrKDHmOB40ILklWGA54EGlgLeRh4fx8qvYsqk21cqgjn0xfVhyNVpulZtLKVJyPCs7MzG5NzyNC4OBwNMDfcGsssRaU6V+IrRpwHA2xqeIVFYmhdcWGFB10nC+k+U3vW/VzFZZkCnUuTfYauR7k2zGVKAWP3CtEW1XVfUbDJTlTnaY+ay12ORlWLWTbw2zIrSimJfEMOJFWrEuFjgMh+2hIBkDiMTRVLsCpJx4/urIdV73vzBreiKwOrFjVb7N2Nw+B4cfnQZYZfSewybApx+Fa2BmQr5fvoR7JAQWOIyq4wkY6vjQc8KARcYjPmaYsSyuRYcLdlXPApc+K3Oi8Ia3isFFr2pgW1k3RCsDzGXdW30yRgbdtUzbdTbxY86mAE1OilsAQMKewGApkjARRqvYUSnBb3pgyrZgP3UticW+VWiA53x5mgYT+P7KYXKsm2WdKWC5m7Grf06jEsSaB24vnic8OFTBlQpBOrOn1Hl86t9A/isOwVP068ST30wZUFjzHcMaHjPP41p9ADI/ZU9EfipgyzenzNQIoyFaPSHAmlMJ/FamKZYpPOarLqOOPZWl9oGYksW+6l/Rfx2HICs4rWYytJbIfOl/NfjpFbhs0XJvjalO3X8RPwqYpmMYhS928R7ajOkYxNuQFaTs3f8A4mkd2NFdhGmOrH8RGNOtXMYtUr+QaF5tn8qXQinG8j9tbm2l/rIHdjSNsriyuVB7MTTrTMYZZLEIPHIclGQqLFLiWYAnO2db4unJGDZvEczamO0GSvj3VOtMxgK6B5rd1LoZ/MSF5ca3jYAG5ck8yKh2gH1491OtMxhKxoPuFARgnUw7hyraNioNzIS3dUOz4Bjfup1q5jEQMlAv91EAKMPia1jYEf8AEtzwpW6drwMptxwqdadoxeY6j5RkP20HkAwB8Ryrf/Tox9XzFKNgoJbXnkLcKdadoxA2FlU/HCkXWxLYDgONdBtjcWVzjxIof04WsZSByAqdavaOfJlZnzwoHRbBSRzOVbv6cmrUHywBIqt+n68BKcOJGAp1p2jmtJiSMCcFVazzRPMdDEgG2rHE9ldlemRxLZZCZDmxFBelqMRITbEm3GnWnaOfHt441OkW+kWqz011gY4Dma6P9NwUazzOFT+mnUT6n2VOvI7RzlRbtnnzoKi4i2RroDphu35x/wB0VB0oXa8rY9lOtXtHOCKCw7edZdwEE8faD9mNdr+lxhrmQ4jlVM/SUZ1IkIKj8POnWnaOZ+WVKjMnUO6mZluGRCx4jsrpDpgWzCW1vDlS/wBLC6gZzY4k2A+FOtO0cx9T+JVAC8Tx7KyzWQ67gK30jge2u0Ok38McpA+k6b/tqHoKWN5SScyRfHnTrTtHEVZGLaVvqF7nD5VYu2BW5cAcVArrJ0cEANOxtgMALVYvSUFrS2buzqdeXg7Rwzs4G/lqQ/bkarYPF4GVVvhYi47wa9C3SwwsZLdpH+NUv0dXuHlLgZErl9tOvLwvaOIXhFtQN081siKBZi1ksA/Zj3V0z0IKbCctyOkfbjVkfQ1QWExZsw2nL7adL4O0cwbZBjMgPYKLQRHO8Q4KK7K9JUeL1yT2rUPShm8h7Bap05Hbi8+8TgNoIZeAyJrITqZYnuqg4/DlXpn6Mhv+cbscSBkPnVSdBSx/ONjfNRlyzp15eDtHKQnSCHtwj5GrAzjG4YJmOZrpH2+B4o5yvALpw++gejgWRpTccSox+2p05eF7Ryyzqplt5vDe/OqJiUeMW8hx7q7p6LGWAEzBRiRa+PzqtuhhiWO4J4eUfvp05eDtPLkhtMt7GwFh3Gry6gKL2roN0QC15jmPp4fOr26QhIPqkYH6anTl4XtPLjsymLOg5BivfKxrsHoyFLesf90fvpT0OMxkeqcRnpFOnLwd55cl+B50M7XF/pNdZuhoUH5x+X+NA9CThO2I5cR8anTl4O/FyIdvbdIYzpIa4HCukGKtlY8q0xdFAljcTtdTfyitzdKVsDKbHsrfHjyxrGeXKOcjXuAe2xpze1iPlWk9J0kH1Th/DVq9PGXqkHtFa6VO0c9SpGOYzqMvEZiuiemLe5kOPZU/pa8JSPhU68jtHOxOINA6h4hjzro/0yx/mkg9lH+mrxkPyp05eDtHN1Hl8qBIGOXOukOmKMpTY9lH+mD/AJh+VOnLwvaODKQ0jY4f4VqAso08hWuToyF9XqkhsxYZ1cOlJYWkI+FOt8J2ji7g3k7hVSR+owXLtrsz9FRjq9chrYYCqR0ex/nG4/h/xp1p2jKWjiARrBeH+NEG2RuDlWpukajdpiSewfvpk6UEFhKdPEaf8adOS9o58sAk8SGzcaSKKx/MF7HCuyOlqf8AjG/O1R+khlIEpB52/wAanXkdo5EkwXCM3PbkKpE8l7k/DhXSPRAP+Me/T/jR/oy5GY/7v+NXpfB2jKpDqGGF6r3A/LN8cseNdaPoyooAmPypJulK1l9Ui2eH+NTpy8HaPWirVYuLcszVSgkgVazhBYDGuxzMPWuq/wBH2bbxtpNuYY1Z5mgCH00QaizCR0+y9DZ7h93AJn28u1Zj/Kn0ayM9X5byCx76o9yPr9tdYvw2c/8A7s10I47xRsPwLh8BQc3pnWD1Dcbrbrstxtjs39KWSYRhfUsraB6cjm+lwcq0b3rcGzmG3Xbz7uf0/Xli2yB2jiB0+o+plzINgLk2wFZOiqx33XsP/wDIf/00FPsBo9ydYWQ+JoNk8d/+WBMpI7A16Dbu+p7Xa7KPfyBmglaFUstm/PdY0JV9JGLi9NFv4J591t4CXO0YRyvbweoV1FFbiVBGrleuV7u/O6CW28oVpNxs/SmWzgFtzFpcDJudT26RHsDsHXRu9i7Rb5b3ZpifUM9zmJtWsHt7KCL1xp9zPBt+m7udYJ220m5QQiMOhAY+KVWsL/hqzq3Vj0dBNLtNxuYbqGl24jIUs4jVWEkiG7MwyrB0OLqR3nU5ItxEuyHU9zrgaFmkOK3tL6gA/wBytnudf/wOcj/m7Xu/+JhoOxtpX3ECzSQybZmveCbTrWxt4vTZ1+RoyAXFWObMe+qjZzjnfLhUGTc9X2mz6hsOlyh/X34f0WUXUemL+M3w1ZLzNN1bqcfR9p+skgl3Ca0iKQBSwMjCNT+YyC2ogZ15Pr+63LdR6p1CDZTbhOknZrHuYzH6cZ2zfrNyG1ur4pJY6VNeg90sr9FLIbq242bKRyO5hINUbdh1OLfmeJYpdvudswXcbWdQsiahqQ+FmUhhkQbVg2XuXbbuSNJNnutok0z7WKedY/TaeMsrRaopHsboQLixq3bkH3V1MqRpXZ7RZf8AOZJ2F/8ATXDU36HGo/mN14iHtYdQZjb/AEhqg9B1Hq42G52+0TaT7zcblJJI49uI7hYtAYt6rxj/AIgqDrOz/o8vXAHbbRRySyJptKPSuJIyjEWdWUi1865fX5OqJ1zp0vSY4Zd0mz3zpDPq0uAdudA0fUTlVEiRD+3+9lhmM/6nZ7ncySldF5Z9csngudOl2ItfC1B3tl1GbeSFJOnbnZqF1CTcelpOXhHpSyG/wqnddf2m13T7ZopnWBo03W5jQNFC01vTEpvqx1AmwNgcav6dF1KGM/1HcwzgqvpejC0OnDHVqkk1fZXJ6jN/TJeodd6ZOJEheMdZ6e48LFVRQ8bZpKI2XmrC3fQdjqG/j6fHG0qvLJNIIYNvCA0kkhBbSuoqMlJJJsBR2O9g6htxuIVdbM0ckUq6ZEdDpdHXmCKz9X2u33f6SB90203vrF+nTIAWEyIxI0t4WBj1alOYo9G30+928w3aIu62k8m03Biv6bvFb8xNWIDBhgcjhRXQxPZQsO+sO2O6/qfUPV3cc23tD+n2iW9SDwnWZLY/mHEXrdjxNqCtiATelJPd2mifMdI+JqW4nE1lS2J/xohQKhIGJpbs2WA50BLAd9LicThRsF7TU7TnwFApsMTlUBA8TZ8KF9bXzAypwOJxNAt2bhYVLHLAU1LcnAZc6KBucAbmoFA7+dNgKUtwGJqAnkM6GVSzd1AgDPE0EZhkMTQx4C3fRAt+2gW5YmgBFzYm/OjcDAfZUCnie+gTbBczQDEm+QoMQouMe00TYDHG3DhSW1kFsuAoAqF/NlxpnKoNIGJyFFpAMBiaQBr6rY8L1BApxLfGmtgB8TUsbgXyxNQAEk0VLjV3UATc2FEWxNAMOGNBBe5oWxNzRGok8KU2DY4mghIFiBeq5CdXKwqxidJOVZ73Y2zNAezjwphFcBmzHCiihbHNjmacXuRlxqCG1sMOVTVcYCoAAbfKgTY4Y3zoqplKvjk1MCMAM+BqSrdbk5Y0qm9hkOdEMcT4sxkBVZYiynPhyolsO3M00acX8xoqLHYd+fbSN4Dhhyq0krhnUKgjHG/GgrwNy3eDyokmwY48qTFGxyGRo3zIwN8BUCPYkAeYmm0kWQY86XAtc5jKmswa2fOgbVxItbKpYNniKhIy5cKjABbcTQVabAtmDVZHhtxNaGBAC51VIMbgWxoJ5gLH50ytcYjIWquM3B7DTKSGYDI0FhsRUAwqMQRjUAwwNRQUHTnQBNssqKki+HGgCNRFA8RGsDtrVnhWWLCRe+tZ51YlDPvrJvNyu1MSiJ5mnYqkcem91UufOyjIVrItjXN6mJ/1XTv05T1PVkt6l9P8pr3041qJWnbbldzGXjDDSxR0cWZWXNSKvUqwwOPGue22k2vT96ZHDyzLNLKyjSupl+kY4AACsXSo0/VQejtv0fp7YGYErebWFCsAhIwIOOdMJl3jUB+dczqMEUu76a0sayETMLkXNhG7W+YvVvU9QgXdxXMm0cTaR9SDCRfihNMLluIBoZYXrF00/qPX6gCSu6f8jl6MfgQj/Ni3xrlQ76Jus/qtZvPM+z02bT6agCNr204yKfnTBl3ZcCL8adD4Reub1vbbabbpLLEryLLCgdhchWlUFe43q6dI4en7mGJQkawy6VXADwtlQa5LEXByqq1Y+l7RI445TsI9sxiUCZGVma4F76QLVtI0kr9tBTNOsAjLgn1HWJbc3wF6kUyTGT07kRsYy3AsM9PO2VZOrrI+3hSJ/TkbcRBJLX0kk2Nqu6c8f6aONV9Mw/lSRnEq6+a543zvxp6Hq1A9tWhrjH51wej7ZTFDM2yjBGphu9SlydRxta9ad+0+0kTebXxPKBtnRj4bufyn/wBLHHspjXBl03XjURLeLjwpNtANrt44EYuEFizYknMse81Y1yPD8qiob2wwP2VSb8adWtgcuNCQi+FB3LFTjnRA1NYnE1a+m2OB4VSM+2vZ5BvINvudrLstwnqQTo0csdyLowswuLGrItOkKMLCwHYKrbVc6s6OPAUGXYdE6f0uebc7NZEl3GMxeaWQMTbxFZXYX8IF7ZYU3UOkdP6oUbe7cSMgKBwzoSjeaNjGylkPFThWoeoM8e+o3qWxGHZQZ59ls9xtk2TxL+mjMZWMeFVMTB49Oi1gpUVP08A3L7tUA3EiLFJIM2RSWUHuLG1Xfdx5UuFBzD7e6W25fdhJVmllM8mjcTorSEgljGkgTG2OFdLc7Pb7yBtruU9SJirMtyBdGEinw2yZQasULfwk351YOygViVF8yaRACSxouThdacW04Zc6gzLsdrHFuIEiX0t20j7lcSHaUWkJ/wAwqrd9I6fv9lH07cxk7WLQY4kkeO3peTxRsreG3Ot3h4fZStf/AGzoM+y6ds9hG8W0i0LI2uVizO7tbTqd3LMxsOJrJs/bvR+nzLPttuRIjM6NJLLKFeS+t0EruFZrm5GNdTxWwoHIcuNBU22gfcxb1kvuYUeOKS58KyFS4te2OgVU3TNi2z3HTzCBtN0ZDPCpIDGYlpMjcaieFbO6pRWDY9H2PTpDLtRNqZdB9WeaYWvfBZpHAyzobjovSt1uxvdxtw840FjqYK5jxjMkasEcr9OoG1b8e6gLUGfe7La9ShEG7i9RAwdTdkZXGTo6FWVhzBo7TZ7bYbddttYxFCpJCC5xY6mYk3JJOJJrRjwFDDjnQZodjtYN3ud9BEF3O8EY3Mtz4xECseBNhYHhWi3xo1DfgKCtszVZYnBBft4UX8x15URa2GVZUoTG7YmiTwGdQ3yGHbQvYeEXoAbLiczSG7HSMAczTcccW/2yqL2DHjQMAALDKgSBmahvxPypfDx+AoJctkMKNj3d1Gob8qilIA7TUAtU+00PEeygJIGdKLnG1uV6Hhv2U9AtueNC4vf5Co2ojkONQWt4aCWJzwHKlvmclGFFr2uc+VVuTYYXFBLa8Tgo4VAdROkYZXqZ2Lmw4AU4yGkX5UChQMPmaP8AFwGVDG1hhzNQ2wve1RU1AAnM1BqA5UTw5VGJtgKABQBjjQBAFhUN7Y/IVBawsKAC5JvgKBIBFvnR4nV8qVycCRhfAUCOxsbcOJpYYwLE4m+PfTN5e3jTL2C+NA3091BjkRjU56sqI8tQAgnE8OFQkWsMamJGOAoLl4RccKKFrghs6oxYY91XnPm1UG92w4m5ohkGs3/D9tWE6ss+fKlTyjT8abD6fiKKIw/bS2OYy5UT24cqhvbLvNQKwEg7sj21T5s8CMKuNuHxqmTzkgY2xFAVxLFsuFMtwzNnlSpllhfGit7m2V8qBhZs+80CDqsMQKOH+qgt7m4xoJe7AHC1Qi96PE3pBxtRVWmzsRnf7qDE31cjnTnjh4r4Uslr4f6hRFxNxjiDxqAYYGq4y2nLDhTjszqKgNib1DbUDzwqAnUbjhQe2Fs74WoGVfzFI51sxtzrIl9a99axllViUL4Y0jRRyPG7rd4SWjPIsCp+w0/GgbXwzrSA6LIjRSC6OCrDmCLEVV+nhRomCC8KlIm4qpAGn7KuN6l8MRQZ9xstvuZI5ZlYvCbxkOy2POykVcyq1wwzwPbeiMscuBqd4woESMQosUYAjQBUUZADAAVWNvtv06bQxAQRlSkeNgVOpT88avxHaKBtUCTQxbhdEo1AMr2yxUhlOHIiq5UDq0bi6OCrDmrCxq7/AGxpH0kcQeFBVttltduQ8IcEDSA0kjgD/K7EVe63GGfCq0Lg4C441bjwFKM0kSS6RIt9DB17GXI0VijWV5Qtne2sjjpyq1iPqGNISb4g3oMkfTtpA6vCrrpJIHqSFcf4S2n7K0mGObSso1BWV1GXiU3U4UwtfHAUMj4caupoZ3sbCgrae6kJwyNQE8u+op5CCcM+dBQWNqAxzwFWLa3hyoP/2Q==';
		}
		else {
      image = 'data:image/png;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAyAAD/4QNxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDM4MDExNzQwNzIwNjgxMTgyMkFGN0Q2REJBQjU4MEMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkREMjhDMDQ1QTA4MTFFMzk5QThCQ0Q5MEFGMDMyNzkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NkREMjhDMDM1QTA4MTFFMzk5QThCQ0Q5MEFGMDMyNzkiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjVGNDVBMDcxNUYyMTY4MTE4MjJBRjdENkRCQUI1ODBDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MjJBRjdENkRCQUI1ODBDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQACAYGBgYGCAYGCAwIBwgMDgoICAoOEA0NDg0NEBEMDg0NDgwRDxITFBMSDxgYGhoYGCMiIiIjJycnJycnJycnJwEJCAgJCgkLCQkLDgsNCw4RDg4ODhETDQ0ODQ0TGBEPDw8PERgWFxQUFBcWGhoYGBoaISEgISEnJycnJycnJycn/8AAEQgBrgMgAwEiAAIRAQMRAf/EALEAAAIDAQEBAAAAAAAAAAAAAAECAAMEBQYHAQEBAQEBAAAAAAAAAAAAAAAAAQIEAxAAAgECBAMGAwUGAwUEBgcJAQIDABEhMRIEQVEFYXEiMhMGgZFCobFSIxTB0WIzFQfhcoLwkkNTFvGycySiwqOzNBdjk9N0JTUm0oNUZMTURVU2EQEBAAEEAQEGBQQDAQAAAAAAAREhMQISUUFhcYGRQgOh0SIyE/DBgqLxciME/9oADAMBAAIRAxEAPwD7VPO7Exx4fiP7KpSJbgXsfxU0egkKWCg1fLCttUfDMVlpGhBXUmBAsVquOJ3GGC8SeyngZxe/kyvyq9nVBdjagpO3QC+JIpXgBXVH8qtSZHOkXB7aIwJXhmKgwN4gb1BioJq2RGMjaVJF+FVhSj2cEA4igCkRyK1sDga1izaWVjbKssq6b9mIPYKZHCuLG6k50FjIxVgbHG4PbSbdyrFSM/vrSv1DtrJMDHL2HEGitWsccO+gbMM6YG6g53oFQcxUAU3wOYzo0jJ9S3BFEXIuDQRh9QzFS4IoXbiL91KG0mxBAOVBHQEXGBGItUGq1wb99NqXnSjAkcDiKBdRViCLXogjVgcxRfK/Kg4BsbcaihHxpiAc6rClS1ibXprsOF+6gyzRkswU27DVayFTpkFiOIyNWu66yCbd9Ky6hhmMjWWkuDkb0siB1sfgaACt/CwztU8a/wAQ+2oFUuBhjbNTnR1K91+w0pYA6hgfqHZTlVYY/A0CLxRsx91I6so1JiBjp/dRfWhDjxAZ87UwYNiD8KigGWRcOIqtgHiscx+ymCi5XJlyPZShirMj4XxB4GgVWKzENxAINW1WBc8/Da3O1QHT2r9ooI68v9u0VFbVgcxnT51U6keJcxwqKLqT4l8wpb28YyyYU6sGFxSMNPiGIPmFAWGoW+Rqu9iGOY8LU6mx08M1PZSuLeLhkwqBiAQQcjVURIBQ5obfDhTocLHMUj+CRX4Hwt+yiiMHI541WMBInI3HcasfAq3w+dVy+Fi3AixoJCfCedzemycjmL0qg6Q4z49ookjwsMqgD4EOPj3UGzU86c1UfD4TwN17qKEqEjUnmGNudKrB108Dl2HlV5rO6WJtgcx21BGubH6hgac+NLcxSatS6sjkw/bRQ49jYigkRugvmMD8KWPDWvJqKeF3T/UPjQGErDmARUUrYX+NK1vUF8lovl/qtUQag7H6jYfCgZPLSTeTtBBpojdMcxgaj5qO2oDe4vVUuQ7706ZEcjalbFrch99FNVUmdvxC1WL5RVbYyoOVzQVjBSvFT99OuZ7hSzeE6uDCx7+FMua9q1AE8zn+KgMZWPIAUYsmPNjSocHfmTRQY3JPDKkdiAFHmei2BCngNTfGoRpHqNmTQRFAJOfAmmfymiosLUkx8NhmSBUEXElvgKj+U01rC1JIbL3m1RQONkHxpJcCtuOFWKLDHM51XNmnY16CZIRQl8oQfVhR4KOZoeaTuoL9vYTRjgDXSAuftrlxm00Y5tXWAsK3w2Z5blbK3OgoGdsOFTzGmJAF+ArSFYhRljwoKoUcyc6igsdZ/wBIonkKgB5ChYHDgMzRJtgKAFxjlyoBbVlgKTAA/ICrLk4LlzqtbZDE3oCVuAD8qz7iQhtIFltw41bM4UEk3tgAOZqlF9SUWxVRdjnc0FkMIjS7+ZsT+6mkdVGVMbg/eaySN6hOo4DEc6BHcufEbjgBwpS+ldIUDl8aNlAxoWAxwJOXZVC3HE3PG1PEsbOAw7u2oBaoLkgLnfMcKgaaMrigwOeGVVojOQguRW/G1rd5NJpVFJ+kZ2qZVkljWI4C4OFCCfQSpXw8CeFOSZX5fsFJImi5GIJtVR73wlRatG38S2IuBx/ZVEcd3/gzatmqOOy3A5CuxzFkVVQkLfsrBiWIYm/Durp51nn2+s6kwYcDQZQpBFib3wrayta4OIxpIIxYOwx4UZZwuC4t9goHGoi4P2UrhrXNjbOsyTyK9ibjO1bAQy3GRqDDKjIbWuDkb1UCxXLy/srXN/LF81Nqy8SvA4ig2RPqW4BsaSdwNLkHA/fR2v8ALt/tnTbj+We8UUI3XSBc24E0+pedVQWMZB51YLZEY1AbjnS+U3+k502leVKUU5igNBhcdvClCgHSfgaOntNBBZhiMeNI6DzAYioQVN74HOms3P7KAabjAmxpPHoIwNqK6wSuGGVTxBshjUUFY3NxmL4VNQXu+6gpYNa3ZTaj+E0GeQqznEGqzGvC691SXSHYlTbjhS3j4MR3XrLQMsnmUgkc8L1BKCMQRzqauTj40pZgdS2POxzqB/A/I1UFaNtKnwnyg/dTao2F7W7aBAIsH7r0B12wcW+6lCqSV5eUjlRV2tZluRgbUjhT4kOlhUVHLxkMcRkSOVFipKsMRkfjUuxXEalPEUllkQgGzjD4igUApIQuK44fuqy4bxLnxFV62Eil8iMxVhUN4lNjzFAuK4riOK8qYEEXGVLcg44N9hoW+pM+K1FKwMZ1r5fqH7ae4IuMjUDBh94qs/lH/wCjP2GgDDSbDLNTyPKmBDD7xTEBhY5GqrlGscj/ALXqBfI3Z+ynddaledR11Dt4UsbalscxgaKVSXiIPmGfwoSeJR2g/dR8kh5GlbBCPwk0BhN4lPZjSuNIJ+k/YaMOCheYBFOQCLHjUABuAaSVboSMxiKEZ0/lnh5TTnEEUUqtqUNzpZR4dQzGNBTptyb76dsjUFWm6h1ztiOYpFwuozXxL3VZFgoU8Bcd1VzXjZZR5QbMO+gLGzo4yOBqPhKh5gig1ihA4eJe6o5uqsM6ikmOlGPJhViDSgHG2NU7jFDyLCr8xcUFa+E9jffRbzD51LXTDMYigDqYHsoIcH7xQXEse21GTBdX4caCeUduNRQXC45Gk46uZosbFu0YUWFlA5EUCTrqibmBcfCkja6I38Jq2Q2Rj2VRGNKaTmCftqB0wjJ76UDwKvPE0T5LczVcjeE2wuLD9tFGP8xmf6b/AHU0uOle0E0yKEQKOApDiGb5UD1U3iOrtsKdzhYZnAUGFgo7agJqs+KQch99O5+kZmlAs1uAFFNVEpwJ7QBVzEAEnhVFiYy5zOIHZUDXsB2D7aiDP5UL49gF6JOiO/H9poH2/j3aHgp+2us5sLcTlXK2i6ZIwc73NdRfExfhkK3w2Z5CBYUh8Zt9IzpmPAVALCwzrSITwFKx0iwxJyFEkKLDOkva5zbiaAgWxbFjQJH1f7oqBWOeA+2jpC5C5qBSSf3UiJfM4Z2FWEcPiaA8vfhQUSnT4VwBF27qbbjTFq4sap3J/MI7hWhWVUXHICwFAk7aU08WzrN9tXTEsyqRYZkmqJQNVlwHG9BHK6VYDDj2mlxz50AvbbupvTxAxJPxqgAaiBe5OQFaoYxEv8RzNGKBYxc4sczTm3LDgKgSSUKLsQBwHOkjmSQEMceXCqZG1MWOWQvSjAhuIyoNI9ONSQthnWV5tbasuQvW0MrKGAxIyrE6BHYWB4g8qRXvBIBYg40uvxWON8jT8BQIBFq7HKsi3OmykXXLDOtWsHEAmueDwOYq+GUodJ8p+ygeZnCnQpsc6yajyNdE2Ydh41mkix1AXI8w/bQZjqOAU3BwFbo7hFAHClhi0eJvMcuyi+oKypmcVqDNuJCZAoGHHHjVLFgQdPZnTOCRfiKB8S4Zmg07csFsRnlR3Bf08hiRTgLlwsLVn3EoGlCb8b0D7fXoOWdWkOeXfSwm0YFjjj86a7fhooAvkSL1LNzFQhjyB4UurgWseQqAsrEZ91KLnNrEZ0c+Z+ylZD5ha/HjQE2tYtSKw8uokinCKRfOoyDMDKgRr4ML4Z91Bg1rjhjnT4gfiX7aikEFc7VFV6TrGOdEocwSew1Dfw81NqcNfA4HlQZGVGds7jMXNIY1XEDDiL1ZKt3JGDc6QNjpbBvvrLQenGRewIpTGoxUC9OQQbr8RUuCLioKSqjxAYfUOVMY0PbTMMbjP76QHSf4T9hoEeADxqSCM7HhQAvgWvfInjV9VMoBt9JyPI1FVhHRiqmwOK0p1q/jQMG5Z3q43YWPnXGgfGlxnn8RQZmcoy4kANk3b21bdb3HgP2GpKAyhjkfvpQgF8MswPvFA+rCzjDnwpSGTxJ4l4ip6ZteNsORypCZI8Sv+7iPlUU+DjUhsf8AbOoCGurDHiKr1Kx1KdL/AGGmuHsGwbgaAAmM6T5fpNF11C3yNQ4+B8+DUFYr4W4VAI2JujeZfupH/LcSfScG/fTSKcJE8y8OY5UbrKn8LCilkGF+X3Uhx1KcyLH4U0ZJBifzLh3jgaRsLNyOlqAA6VVuAq2kjAaLScsRUQkDS3DC9QArdiOeINFWvgcxnUbAqfgaDAjxDMZjnRQABUr20Abgg5jOipBLWyONB8PGOGB7qgXJVbln3UWAZSOBFFLFOylBsCpzX7qDOhKqFP8Ap7uIp/pI+I+NEpqXTkfMp7aqRrob4FTpYUB8zBe/7BTxmy6TwGHdSJjO/YMPjT/TqGa3qKKeWkXCR17iKdDdQRSHi/EH7KAyeUjnh86CeUD8OFFsSo+NDJyOeNRSPjInZnRfymgcQX7cPhTP5STyoK3xw4AXNVyeGRf4rfZVg/lFjmwvVe4w9JuTCoIxspPKkC6lLc8F7qEh1aYxm7Y/5RV9hqCjJRQAt4L8xQYWQjsoDEheWJoyGyEcTgKigviOrgMBQc2K3yvTKLKB2VW/idV4DE0BUXuxzP3VB5mNNVY8Q0jicTRSv4h2ZDtNGX+W3dRI8SqMhjSbg2hbtwqBVxW/4zh3Ci3jkC8FxPfwqCygclWonhQu2ZxNBZGSdxEgzJx7BXWOAsK5G0wmSRs2bDurqnO3H6jyrfDZnlug/wCygzhcB8ahJYeHAc+dRVHDLiedaZKFLYtgOA/fTEC4Ud5p6QHjxOVFEnlnQwUXPxo2pG8RCcMzUEuACeNKuo24WxpnsFI54UL2W/E4Cgw7gXlfG/CtiKLLbIAfOssylXvzyrXF/KXuxpRRubhg4+kC5+6sy2LAvfE41fuhqKv9Iw76oA4mqNbwowBU27eBoxxBBfjzqnbuxOki6fdWk3tf5CoEkkVBqb4DnVKbg6vzMj9lVuWLEufFy5dlJ2mg1yQ+pjexGXKqRAxNiQBxIqyBy6lW+nj2VYSLclHGooWCrpXIC16z7gAKg4m9XSTRqtgb9grJJIXck8BYCkH0AkKClsRgT8aT/Y1fNE7i4PiGYAzrOF7STyrtcoMBfw8Miaqn3Cw7eae2r0kd9N7X0qWt9lXKkdzrBItwrL1IL+h3bItvyJQRz8DY0Hjtj/czqO6i9XZ+29xuYr6dUTs41Z2usJxxrSf7hdaUh5PaW+RR5m8eXxhFH+0oU+3ZSTiN1JYf6I69xNuDiiHvNKPLdH/uB0nr046cqybDftgNtuRYufwowwv2Gxrp9X6ru+k9Nk3m32j7+WIqE20V9Z1MFNrK2WeVeN/ujt4YtlsOqRKE6hFuQsU64OV0l7E8dLKCOVe+9YSRxSHCR0VpLfiKgn7aDxMnvvrUhv8A9K70c8X/APsaz7X+4u9k3Em3g9ubncTwX9WBJGZ0xt41EJIx5176JZHfQosM7mvDezrr7/8AdNjYAuD/APXCmgvHv3rakf8A6R3uGGb/AP2FeyaRZW1HAWuf4QBc37qs3Mqxm1yWavC/3D6rutv0/bdG2IK7nq7+lhgxiBClf9bMB3XqDVvf7iRvvG6Z7Y6fJ1ncJgZEusQthmASR24Cqm9wf3KW8p9uQNGMfTR9T/ZKfur0vt3oGy9udNi6fAg9SwO5mtjLJbxMTy5DgK63lyxX7qaDyPQPfWz6tvf6T1PbydK6pfSNvNfS7fhVmCkNyBFetMS8Bjzryf8AcH20/XOlrvenQl+r7NlbbtHYSOlxqS+GXmHK1eg6LNvp+k7OXqkLQb8xKN1E9gwkXwsTa/mtelGvxDLHsNcf3B7m2ft4bIbmJ5pN9MII44yARe13Or6RcVT7h3vu3bbiOP270yHeQtHeSeaQKVe5GnSXS+GNfMPcO392N1npvUfdKaDNuI4tsqshRQrqxREjZtOfHOkha+2nwMeRz765Hufrw9t9Ifqpg/UhHSP0g2i+s2vqs2XdXYZSCQMRfI14v+5rf/pKdDgRPBa/LVUm616np26/W7HbbzR6Y3MSTKl76dahtN8L2vV7rYhxhzPZ21g6CL9A6XwttITf/wDdrXlOq9X3/vLfSe2/bUhi6dHh1bqo8unjFEeN/wD0u6mB1Nr7tHVOtydK6PtP1e229v1fUvU0Qo3EJ4W1nljj3Y16cgN+w1z+ldI2PRenR9O6fEI4os/xO3F3PFjW5bhQVxHKlWM7sVkIf4NXkP7ge9k9kbHZbttgd/8ArJWhCCX0iuldd76HvXsHKszD5ivkH9+gV6R0MX8P617f/VisyZ5Ldmkf3V9zgY+w+okcDeX/APtaV/7xbzY/nda9odR2G3v4pTqtbn+bFEPtr6qrakWxuCB91VuvgZGHqRMLPGw1Ag5gg4EUzPBi+XK9ue5+je69h/UOi7j1Y1OmaNhplifPTIhy7DkeFdVlvln99fH/AGttYfbv95Or9F6SPT6duts0p2y+RCUTcBbfwsSByBr65ud5tdltpN5vJk223hGqaaVgiIBxZmwqWYunqsoq1jpOXD91MwBBB414Td/3d9hxSNGm8m3BU2MkG3kZP95tF/hXe9u+8vbvugMvR98k80Y1PAwMcoX8XpyAG3aKll8GZ5dnE4fWvHmKCkBuQbMcjRmZY42nY2ESl2P8Ki5+yvOn3v7Vk6J/1GOoonTfUaETSK6F5EAJRI2UOzY8BUwrvuMGX/UtZYeqdO3G7k2O33cUm/2yhtztFcGVFNsXTMeYVxdn7+9s9Q6Ju/cUO5dem9PkWHdTyROLM+kDSgBYg6xwr5v0D3z7W2P9xfcPXdzvinTd9Ase1n9KU62HpYaAhYeQ5irONudNkvKae19sI+pOPDgaIYN2HiK8/ufevtrYdK2fXdzvCnSuoMU2m59KUgsL4MAhZfKfMK79lcB0OYBVhkQcR8KmFJoXUUYYHFTSNE64xnUOKN+w07NgdVlZfFc5WGdc7oXuPo/uWCbc9F3B3MMEnoySaHQB7arL6irqwPCorYJR5XBA5HMfGvK+7Pd3Vfb27g2/T/b+56zHLF6jbjb69KHUV9NtEUmOF869e6A8L8xVKqykiI4Z6TSY9Zkr5pN/dbru1hk3G49l76KCIFpJZGkVUUZlmbbWAqraf3Y6xu4hvNl7O3m42sl9MsTyOhIOk6XXbkZ16/3y5Hs/roFxfZyhkPdmK5/9pzf2F01W8pbcD/2z1r9PXPX1xuzrnGfQnt/3t1nrfVo9pvPbG76VCUdm3c+vQNI1BTqhjGPDGtsvu5P+tl9m/pLmWD1/1nqYA+mZdPp6ey3mr06MReJzfkTxFcY9U6FH7hXojGP+uGL14h6J9T0gCf52m1rA4aqzpc6en9VrXz6urtmJjIOYJB76cga7HJvvFUqfTmb8D2PdeuD17377V6BP+k3+/DbtD4ttt1aZ17H0YKewmpi3aZXMm70bXsQe8HuojG3bXlOmf3J9m9XnXaQ7/wBCdyAke6Robk5AO3gv8a9TGSPA2YNu+pZZvMEsuzzHtv3YvXur9b6eNodsOkT/AKdpDJr9Q65E1W0rp/l9tenOVfNP7bW/6p97Xy/XW/8Aa7ivo7yxwI7zuI4o1LtI5AVVGJLE5Wq85i4nsONzM0I8BoPbbuqS+EF+yxrldF9x9H9xLO3Rp23K7ZtMkvpSJHqP0h5FVWPdXS3E8EO2k3G4kWKBFLSySEKqgZlicBWbLnFU7YBW5Z/Gs849OTWMnGl/2V5Sf+6fsuB2g/WSz6fC0sEDsnfqOm/wrt9J9w9F9x7YS9J3ibkKQJUF1kS+WuNwGHyq3jymtlMy7WN0cio8kkhCqFu5OAAUXJNTYb7ZdR2/6np+4j3W3LFRLCwdSRmLjlXnfcfubo3QNtLt+rbkwbjcxTJt10O+uy6TiitbFhnXkv7Z+8/bnSfb+36Nv94Yt/JuX0w+lI1/UYBPEqlce+r0t424qdpLjMfVI/D4eBxFFcj2k1Cp0dovbvoRm6A/OsNFTzlT9P7akp0jVy/bUybXzNj3VHGohPiaipp8OnspCdSqvPOnU3UX7j8KSPHUe2w7qCf8L4VVuMVUcc6sP8tuy9UzG0btxK2Ud9Am3GqTV2eGtC5saqiARIyPpWx+NWHBLDM/tqVSx5ueZwo+ZieAwHfUPgOHKw76IFhb51AuqyA8TlQtYqOOZoR+LxcBgtFiAwJ5GgDtYYZ8KCLpUDjmahB0ljmfupjgO6ilHmY/CqZfH/lBHzNWLitz5c6rlOlAeLG9QQ+I6eZxqP4iF+kZ1Bh3/wC16UHkL8hQWxSBZ4yfMTZFrrKhOL/L99cnZJfcJK2JLYV2SeAzrfHZnluRsWCjvNNQUZ0SeAzrSFc8BmaIFqAGJPKjUAJsL0i882NRiXbSuXE04AAwoEYWGOJNL5m7BUc3e3BRc0QCBb6mzoKpQbq6tptgDSwvqUxg+U3Y9hq2UXjKL5jlWSE+m9jlk1BpkTXGbDLFRWPBjYmy8TXQJ4DLjWOaLQdVvCchypBfdIYweHAczVUUzs/iF78OVU5kAm3LsrUNECYYk8edBJY1cXPn4GsxikU+JfliKtTcDUdfHI1f2nHkKCmGIoCz5nhVjeJSDgtqOfb2cKqlYDwA3c/IVFYzwAwH20MhpGLGmfFgFOHMUBbE5DhVR9OwYAj4GqmjDXIFpOdUxyyxnSVsOR4VeLSC4b5V2OZldWQ+PAmsu90tsd4Cbf8Al5v+41dYKpwIx4g41j6lBH/Tt6QLfkS5f5GoPkXsv3D1rpPSGg6f0ObqUDTM53EZYKGKqCnhRsRau+/vL3TpJHtXcA8CTIR9kdaf7URo/tiUubf+bkFh/kjr3AhxCm9vpOVKj4/H1aHrvXdufesr7BNu19rsDCyQ6rjGV3OrE5kj4ivrcSFxc4Ds491cL390vZbz2t1CTcIpl2sZn28hHiV1IyOdmGBpvYu73O79p9MmkxdEaJpGzKxu0an/AHRSq9GRoUaRptiL5mvnHtJh/wBee6icbl7d/rCvojzBBdQHbne9fOvaAMnvv3MMrlySf/GFJ6j3YQykOcSD4j2V4j3QBP8A3K9tbd7emiRuoPPXI33oK+hhFUFFyOPxr51/cK/SvcPtz3GQfRhkEUx5aHElv91mpCvpJxpCg4Yd1FXSRQ8bBkYBlYcQcQaNZVWQ6Ygal4iiHU8fnXO9xdah9vdI3HVZVEnogCOK+nW7HSqXxqdE6hN1fpO16pNt/wBM+6T1Bt9WqyEnSb2XMY0HSr59/c8fm+3T/wDzv7Y698BxU27DXgP7nX9T27f/APjc/jHVm5dn0Im5PfXjP7ni/tKbn68H/er2LEaiV54ivHf3P/8A+RmI/wCfB/3qk3hdnCTf9a92dJg9ve2fytptdpGnUd/JdA8gjH/lYznicD+7Pqf256ttP0L+35Nuuy6lsGYbiC2kyY2Mpv8AUMm/dXovbW2g2vt7pi7aNY0O2id1UWuzqGZjzJJrznvf29uVlj93dA8HU9lZ9yiD+bGo81uJUYMOK91X2J7XuDg1+BwNLHhqXka5Htvr229ydLTfbYhZB4NzATcxyWy7jmDyrqg3YHysRb4istKpUDOTkeYr4/8A38Zl6N0QuLhd5IbjjaMV9fd7OQ+Hbwr5H/fz/wDKOh//AH1//dipx/dC7Lv/AJ5e3QAU6T1MGwB/Lisf/a1Yf7vz9RT0vbftXqO93b4RequmMHmxiD4fEd9fTQpVVKchdeGVQtrwJKsKZnj8VxfL577B9m9Z2HU9/wC8vdTqev8AU7j9OhBWGNyCwJW41HSFAGQFXe/PbHWfd/VeidPQafbsEnrdXAkCMxBwXTfUbKLC34q92GIOl8+B4GvnnvD3b1+X3Lt/Y3sxI16tJGJd3v5gCsCFdeAIIwTxEkHMAC9Jbbkskj3ey2Gx2O3TabDaxbaCMBUhiRVUAdwr5Z/cuDY9A92+1eu9KEe16rPuvS3SRWQyxao11SIvMOy3410j/bf3Dvseu+9eoyyNiV214o+4eP8A9WvIe9vY/SPaW69tT7Obc7nebvqKpuJ93IHYqjRkWChQMTTjjO+S5xs+275r7HeqcGWGYEf6Gr4p/aL2ttvcGzPUuvIN303pkrw9O2UmMXrSBZJppFybDSBfD5V9s6ql9nvGXBhDNY/6GwrwP9kwv/Q4AzO8nLfKMfsqS4434Fmse5/Q7LbIYY9rCm1kt6kCxoI7jIlANNfNfbmz2h/u17pgbbQvEm3jKRNGhQYQeVSLCvqYF7xt8DzFfL/bp9L+7vuq+Q28YJ+EFTj6+5b6e9633n7fh9w+1N/0aCJUlEfq7ONQFCzRfmIFAwGrFfjWD+13Xj132htBM19306+x3IPm/KA9NjfnGR8a9kVBsciMQRXyzowPs7+6HUeinwdO9xR/rNkPpEo1OVHx9RflSayzxqXSy/B6P+53XP6F7Q3skbW3W9tstrbPVMCHZf8ALGGrT7G6IfbPtnp/S5V0zNH6+5P/ANNL43U/5cF+FeV9xW94f3M6T7dX8zp3Qk/Xb8DymQ6ZNLf+zX4mvp0g1g886XTjJ51Wa23xolVyAqRIvDBhzFQEgXzX7RTmzDsNYV5z32Ff2Z102vbZy2+VcP8AtWZYvYvTXtriZtxqXiPznrt++L/9F9d5jZyg/KuZ/aax9hdNBy1bj/3z1r6Pin1fB64lZFDKb8j+yvnO4Jb+8+2J/wD9Zn/oevogj0kqpsRlyI5GvnM1/wD50bYHwn+mkWP+R6cfX3U5envjrf3H9x7j257ekk2R07/euNntmGaFgWeRe0KMO01q9mez9h7Y6ZEWhWbqkyh+obuQBpDI3iZVZsQq3t2515j+8Foo/bu8kH5G33xE3yRxf4I1fUCw161N0kxBGWORpdOMx65yT919jznur2l0v3V0qfazwxjeqrHZ7wKBJHIBdbsMSpOBBpPYm36/tPb+32HuSMx77asYkYushaFbemxZC2QOn4V6NgUfUuTYEVl2HVumdWUz9L3cW7jif05XhYOFbPSxHGs5uMejWJnL53/b7cQbT3B763O6lWGCHe+pLK50qqrLuLsSaq3G96l/dbqEvTemSPsfaOykA3m6yl3LDEKB28FOWZxsK8/0b2vvPdfu33Nsn3bbfosPUHm6lEhs0zCaX0UH/pYnLvru73b/APyv90Q9X2MbL7W6sVg323W7CCQZML3OHmXs1CvS4zcfuxo85nGu2dX0npXT9l0rYx9O6fCu32238KRL9rE5kniTXz/3R6vvH3vB7LEjp0fp6Lu+rLGSPUewYISP86qO0k19IRkkQTQsHRgGR1NwysLgg9or48nQv63/AHO9xbGTqW66bIV9eN9m+h3X8o6WPEWa9Y4b230jfLaTzX1va9N2XTYU22x2ke32yjQIo0Crbtwx+NfOv7gdKj9q7zZ++OhRDazwTLH1CGIaI5o5PxKMMbaT33zFdIf21c3B909ZuMx69Zd3/bDbbqBouoe5OpywXGtJ5lZCQbjCTCnHrLntnzoXNmOvu1eo6im03nT9xvTEk0cu2d42kRWIDxFgRqBtXl/7TbXav7Sgll28TuZ5rSPGrMCGFvERevV/o02ft+bp8bmRdrs2hSRramEcRVWNsMQK83/aTH2bADkdxuB9oqfRffF+qe6vcg/lk99L5O4j7aCk6Chz4doppBdNPO1YaQLdLHjSRnVdjmPD8qbV4DzGHxpVGggcG++ooE2JQfVlRA0sQMrUD/MD8B4ajnSQ3woK2xDJ23NVuNcmngov8hVoFg98ziaqjI1SschlQBMYiON7D4Y1b5mHIC9VweQE8bsaZDpRic7/APZUVGxkB4Lh8TRfHwjjn3VG8CXPDE1FHE5moAmC24Ck88gJyAwojxEqMgcT+yj9fwoI/AdtI/iFhkTbvonxOBwGdQ4uByxooPwUcfuqmYgyIOC+I9/CrSwBLnhgBWVyS5v5hiRyoHJLERr5mxY8hVhUAaRy8RqRqI0vmzfbUbwIb5nOoLdtZZYu/AV1cgSc65e2H58bHngOVdRzkozNb4bM8gGAsM6hwF6a1qRjiB8a0gjAY0pJI5CjYnFvgKU+I1BEFsaJNu85UTYDsqpmJBIwvlQBRqYngDj8Ke9vFxOVKAFULzxNE5/soAAb/wAR+wVn3CaDqQZ5nka0k6cM2NTSLHVjfOgpgkuNL+YZVYwDCzC/YayyI0LBgcPpNXxSCQY4N+HnQZ5IWW5XFPxVXcgaR8q3k8suJqhoA/8AL8PbzpkZSbdrUyNIuRw5U527pwv2ikIfkQOdqBmnktpB+AqvHzMcqYK4yUgHOj+nkIvkOZzoKTcnDjgDWpNuqgFsW7eFGKJI8R4iOJyqwY+I4nhypar30kYkAF7EZGs7J6RUliL5kcDWganGJtzAohVHD4muxysv6hr2A1DIPlVG/nD9P3itcMYJRpAvc6GrdIsZGntvYVQ+2JF9ekDK9B8l9k+8ova/R36duul7yeV5mmDxIALMqrbx2N/DXon/ALqbRhYdF3/Z4Vr2qxub2fT2m96sWBuEn30HzTqnVvdHveD+kdO6VJ07p8zD9Vu9zcXUG9rkLhxstya9103YJ0rp226TASYtrGI15kjFmPaSb10zt1NiSSRlc1Yum1wLc6DLFAwNnwU5DjXhPaEEqe/PdDmNlS76GZSFP5wyJFjX0VhcdvCoGLDHhwqCtlUgMOHKuV1/oW39xdJn6VuG0hrNDLa5jkXFHH3HsrrDAlTkcRQXBrfD5UV8y6Z7m617IVei+6tlLLtIfBtOoQjUNAyW5srryxDDKu0/90/aYjLJJuJZOESwkEnldiBXtWVXUo6hlOasAQfgazr0/YRtrj2kCNnqSJFPzC00R87Oz67/AHG3+33HUds/TPbe2bXHA9xJMfja5YYarWUZV9KREjRY41CIgCoowAUCwA7hSkac725ij4swb99FQjG4zrwP9ykklk9venGzld7dgqlrC8eJtXvrkZj5UNQORsflUBdQWOHHOvG/3Likf2pOsQaQmeE6FBY+bsr2Ia+Bzo3IyoOX0Eleh9MVwVI2sIII/gFdAHScPKaJwx4HOgUU9l+VFfOesbDeexuvL7i6LC0vSN62jf7KME6SxubAZDih4HDKvewbiDe7eLd7c64J1EkZIINjzBxBrQpdTpz7cjalLA3Q4HMXpajPIrajpNweBr5J/fSCabpPRVgikkK7xyyIpaw0DHwg19bDXLDkbUazLi5axmK0YFVGRsLg91FlDZ/OlaM/Se4H99KJCps4+NZVDcCzeJefEV8t9zbXqns/34vv/abKXqfSN3ANt1JIBqlh8KxlrcvArA5Zg2r6rcHEYilKkHUh0mrLgsy+ft/eL2dKmnbJvt1uOG0i2zepq/Di1r/GvA+/ZvdnUpele8ut9Ofp3Rdnuo49nsGu06R6hK084sNJfRbG3Lv+8qBG5ZFCOc7AC/xp7rICCLg+ZWF/mDScpNollu9cbY+4One5+hbvqnSjI+2eOdEMiGNmKobkK2Nsa8n/AGb280HspPURopRu57q4KkjwcDX0IpoA0CyjJRhbuphJrzNzUzpYuNckv6guMGGIr5BvOsbf2b/c/rXU+vRTR9N6ttkXb7mOMupbTFy5NGQbV9eddJ1L8aVtLYsoZfqUi4/zC9JcfFbMsPQ+r7brfTIOo7TX6E2r0jKhjchGKXKtiPLXjv7tdPnXpWx91bAf+f8Ab25TcBucLMoYHsDhT3Xr2u63MHT4dxut5II9rAhmllOSIguWw5CvmXvz3VtPeUWx9k+0twN9uepzod5PEG9OOGM6vExA4+JuQFOO+ZsnLbDof2m2O5m2fUvd++UDedf3LuvZEjHBb8C5NuwCvo4YHDI8jWfp/Ttv0zp216XtRp2+0iSCLnZBbV3nM1dgws3mHH9tTlc3KyYmE8r24N99Agqbr8VoNqAs2I4MKZWDKD86yrzvvcNJ7Q64sYLNJs5QEAuS1srCuZ/aqKWH2L01ZEZHDbjUjgqf5z8DXspFJGpcHGINRZNeLebjernTHtyY1z7CmzN2MPtFfOd1tpZf7x7d3RvT/ppX1QDpuEf6sq+jSAga1zBuRRN2UFT3Ulxn2zBZn55eY93+3l9z9E3HSJ2CTG0m3nOSyp5GPYb2PYa8p0T35u/bGzj6D762O5282zURQdSijMsUsaYISQcSBxW9+Nq+mS2YB7Yg2YVRgrGCTFD4oycu6kumLMws1zNHgOrf3Ek9wbR+kexthut9v92pj/WNEY4oVYaWe7cbcTYCvTezfbCe1vb+36arh9ySZ95KuTzPbUB2KAFHdXeVvSwsBG3IWx7hTrxXhw7jUt0xJiEmubcvnH9t4pU9ye9WljZBJvboWUgEeruPKTnXuOrdJ2fXelz9K36atvuE0NzUjyuv8SnEVusWGJ8S4A0sbYFTgwNLyzc7LJiYeB/t/vupdH3e69idcVjP08lumbrSdEu382kNlgDqXsuOFL739u9Y23W9l729rx+t1HaD093s/wDnRgEYD6vCSpGeVsq+gY3YA5GgSGIHfcU7a5xvuddMfJ4OL+7PtsIp6lBvOnbwYS7SSBmIPEKwtf4gVxd8eq/3Q6jtdtDsp+ne1dtKJtxudwNDzsBayDuuBa9r3PKvqhC3tIoYgeFmAJ+2lUHT6ZPC607SayYvvOtulujHuYNW3n20ACatu8MaDADwFFFfMP7fe7une2+mf9N9ai3MHUYtzJphELMT6hA4ZWNfVHP56fxYHvqblQkqTqBqyJtjbvqSzFlmclmssuyx1OgW8wbD50b6ivzIoKQyoRkTeofDIWGWRrLRZMHXkfNRl8htnw76IAYseGVKp1NpP0VFS147Dl9tIx12PBbE99MToJXn5fjQVbKyfH50AkOkMeyqIx+WH4C9+29HcMWAUZlbmrSAsFuAWgrj8luZ0im/4tuAH20kJshJ+jD4mmfwKp+q/wB9RRbx3HAZ99QtgAMzRwRbf7E0sY8NznxqCINNx20vmdgMsLmje7MB2XNQAam+FAB5jyApC+nUcycAKJYjVpzv8qQ2jS5xsNTGilc+mBfFhjbtquJPBdsWc6nPZRIOgyNm+C93On0+BU4mxbuoHGJ1HIZUp8bKTkDcCmPiNh5RnQJ8dhmB8qgtgP58YGd66gHi7q5m3AEyd9dIEnLjma3w2Z5biTbLOlUfUczRI4DjRJArSAxsLcTSA8seAqG79gOApj4RZc+FApFzjiBnSscr8cfgKe308BnVQOtyx8owHbQN/EczkOVTyi/E5Cpf6j8BRAt4mz+6oIBbE58aUm5sKly5wy50fCgoBoBFmxvnWSWMpjHcjszrSSz4DAU3hTvoMscxwE2I4W/bWj1UPlNzyGdVvCJcho7RVLQGPyeJuzOg02ZsTgKn+X51k9WdcGv3Gj60n1kd1MDTqAP4mpW1NYHC/CqRuHNlQC5ysKVjM12JOrgoH7qgvYxp4WOWYqmXcM3gTwg/OlXbyfWbX+d6vSCKPGwLczRX0BiL4ebsoAs2DeE8hTXVRh8hSONWBz4AftrtcollXyj40AhY3f4Coo0nx58DwqygUgcM+FLZTmMaelI4jOoBoHC4pWVlOpW7xTg37+NSgW7WwINKxZTqthxtTeU/wn7KJoK2bC9iCMaVnUMrA4GrFwOk/DupGUYqR2iirLihSacLgXHLjUAByJHZUD0hW2K/EcKNm4NQ8fYaCBgew8qhANA3Oam/Ail9S2D4dtATGpyw5WpQXB0k35E0+pedQgEY0C6jxFAMBxwo3K4NlwNQgHMVFBhcYZjKlPjGWIo6QuFyBwoMHU6hjzFBhcaJ2ZSQr/EXFMHa17ahzWhO2lybGxNwe2iND+JTYniMKzWhDKcjjyqEAixF6VgfrXUOYzoC+aNfsNQKYmU3iNv4TlUEovpkGhu3I91NrI84t25ioQsgsbMKCEAixxFVshGK42+dQpJH/LOpfwH9horKrYHwnkaigsgbA50HS+K4NzpnjVuw8xVeto8JPLwcftoCr3OlhZuXOlYaf8pyPI07Irj7iKQMQdEvHJuBoM282sG828uy3cYmglQpLE3leNhZkPZasfTPbPQOjapei9Og2Mkgs0kK2Yj8JY3a3ZXTZclJsfobl2UInxMbYMOFAQzWxxAz5igxB8a4kZjspmBB1rnxHOgVVvEMDwIqKNwRfgaqKlH1JgGzHC9S7RNZsVORHA05s6kA1AAwOBwPKk0i5U8MVNMLOvizGB76rcshDHFRmeNqKa58r8cjwNKt9NxmMCO6nwYcwarBMbFTipxB5UAk8pdfiKonC+B80P3GtRHFeOY51QVDR2+k/YagS5jPpsbq2KtTm6kOvcy1XcNGYZMHQ4N9xpkYteNsG+886KdWBYj42oaQSwPeDUIDaWyOV6AYrJZ+IwPCoIrEOytnhY0GH5gIzAprAswNJcrJZsRbA/voJJZkPP8AbQxZbZOtFxipHPEVG/GuYzqKyzH8z1Bwtbv41dP4oiRwxFIwDRk8Hx7jQRy0IQ5nw/KgEXhCN9Jubcr1aD4Wbneq4jZSOAFHy6UPlP2UEU+muk94qWKgPx+qmkGsheWN6UtqGn6jgailPiPqDJcv20XbSQ34sKK2UFDkMu6qwCQSc18vdQVSLpC8y1m7qs3DBYj22ApJiCwvkbGllvdUPDH4UDILML8fE3fT21Asfh3UrAlQvHzGmvrsB5RnUUE8YDn4CgLlmUc8TRBxZB/tepYK3ZaoAAAxHdSXLMwX4mji78lt86mQa1AoACBRxNUyN6jEZIuJ7bcKM0pACpiwFviaYRiGElsWtcmgRj6kgBFlQZdpp2vdUGZN2NJCDbWfMxw/fVhFmCjE5k0UcvCtKthqPbnTEhB/tc0iKSAW+AqCyElpo+ALWHbXXtYWFcmEj9VEDwN66jXIxw5Ct8NmeW4XuSRjwqEfEnjTAAC1ITx4nAd1aZEWxbgMBQJsNRzOQqYtgMFFAC5w+dFKSxFhhzPbQAAAUZCixudC8KgAH7KAgW8TfClxkzwT76PmN2y4CldwuBz4KKgYsAMKS1zdseQoC7YkX7BkKJ1nAWHbQEm2Bw7BnQuBiftoBAOJJ4miIxnb4nOgUtfM/AVB2DSPtp/CuAzqY8MO00C3tzpbg5JfvpiR3mgT8ewVFIxIGGfAAUFsBjew++nIYm2VSwOBOAoF1Le5BPKpqvgot2mnJGQoE8FF6D3StcD0xfm1OLr9J76WKyr6eWnhVtdrlITfAqa5/WH3idH6j+gDnd/pZ/0np+cTem3p6e3Va1dKpQfNBsPcsvVY+iv7l6qofoP6901bZZhvfUEOn1BtwQtzlnfjWjpW89y9O69F0/09z1TeT9D2W73m13e7CQw7r1Hh3DB2EgUuVACopBN8q7J6T7lX3h/XlOw/pw2v9P8AQ1T+v6Jm9f1fJo18NN7dtOOne4191brrsJ2DbSXZrsYYnecSj0nlnjd7RlfE8lmAyGVEY997m3W82HQuqdNjm2u23fUIdtvrmL1Y29c7V9tJG6sGQuCGdGuLC2dWdW96ybHddV23T+lnqB6LGr79RuEhlJeMTgbeFldpbIcThyFzXPX2z7yXo+16Xr6UW23Uv6n6hfc2a25O9EdvTw8TFb8qbrHtDrfWpupLvE6VLHvHP6PfTJK+82SGKNT+mkQLfTIGdBqWx50V6P3H1pug9F3PWV2h3kW0QzbiFZFjYRKNTspYHUQMl41k2nuTcfrtnsuq9Nfp69TWRumymZJi7Rp6zQzJGB6cnp3YC7DAi96ze/ZItr7G6rtZJGeabZts9uSpeWWZk0qLICS7ab1TB0nqfuWPYdQ6h1DbDb7WGf8Ap246akqu888LbQbt/XtoMasxVBfxccKgu6b7wbf73p223XTX2CdXWVumvLKpmPoqZGXc7bSrwkqpIxbkbUkfuzq253XVNptfb0kkvRZFXeH9XEFZXiXcIICV8chRsUNgPxVwh/b7rcY2LdOk6X03e9PWWJd5topvXnM8DbZt1LM3jEiavUVbsC2bU3SN31Pee4/dMHQN7s/S3c0JWTdxT61Ee1h20m6h0BUlHqArpJHiXOqO6fem23D7aHocC7+Tc7SLqN5txHs449vPcRBnlDEyOVayheGJFVye9DND0fc9N6VJvIesyvtICJ443i3cQl9SGZSGAVPRa7qx7AcL439hQ9P3G0k6Zt9hvkh2MHTpIOsQ+qNO21enPE6KxVzrbWtrNhiLVom9tdf26dBh6bLsJE6RM+7maZX24kklWaNo4odrGUjjVZvDxwxvnU0NRi959QlbbRr0UltzvZOlB23SrGN1EryMBJ6XiiZUOlwMW8OkWquH38+8i9bYdIknDdQ/pkaNOkbs8lm286goR6UiXcm91AyNdz3J0nc9Y6W222W4Xbb+KSLc7HcyAskc0Lh1LBcbEXU99cnZeyv0HWum72Hcr+g2G1hjO20kO+628Mm1jnv5QPSma/G9NA/UveUnS5N2+46XIuw6fLDBvt28yxnXMEN9rE6j10T1MWDDI2Bpt5HOPfGz2g3u6XadQ6bvGn2yzMIg8Um3iSSFf+G4EjYjjXE6x7B6h1Buqpr6fI3UZ23EfVt3FJLvYhqEibZPoVF06AykWX6b10Nt0P3jD1jY9Wn3Ww3b7WDdwv6j7gMf1kybjSpEZ8EPprGnNeVBn6J7gfpXt3a/q5Jep77ddR32w2KbiZRJJ6G4nC+ruJsFWOKLxMfgCaHWfd2/fonU26XtUh6x0yXapu4xuY5ESPcOuiaGVUdZFfFLFVIx5YrD7J6s/TNtB1E9Nm3fTt/ueobNHjkn2so3jSvNBuY5kUj+b4WW5BAPZW1vaG7l6R1Xaa9lsdx1H0Whh2G3Ee1gO3IkQHSqSS6nF3ZrdgFNF1el2p3ku3V9/t12m4JbXt45fXVQCQLSaI73H8NWWdON1rn9H2vU9sN3N1WaOTd73cHcNHCXMMS6EiWKEy+K3g1HAYk4V0vFyFQQ6iMgRS6iMCD2Gp41xAuvLlRuSMsO+orHKyrIysbK2XYarjKklGtcVbIxLsjLnlWaQ6SGKeJM+1ay0v0D6SRSsjZ4E8xgaGqM4gG3MUNa8GI+FQTW6+dSRzqAxPipsezA1PU/iB+BpGKN5lF+YwNA9nGXiHbnStofBxpPM1Xr0ZEkUROjYFgew4Gip4043XnwptanBsL88qXUnA6fuoENmtmHIZfKoIUaPGLFeKH9lEMkoK/NTmKVXF7A6T+Fv2VHCsbnwPwNADdPC+KHJuIquRWuHU+NcjzFWByPDIPjSstvLiOFRRWUEDUNJOXKofCdQxU59lVxsNRjOF8V7DVmkEkeVuzI0BYBhb7aQANng44ihd4zZvEpyI4UWYHxLmMx2VAhLRtqbFTgSKfBgbYg0TZl7DSBb9jDAkUUqqbeHBhgRwoFvEuoWORHfR1Mj+IYNxHOi4DoePEUAIK4riOK/upFx1FcccRTAsAD5l+0UtvGWQ5i/fUFE35bCQYqRY91GWI6RLEfEBe3MVZKBJGwtjbFaTbPgImzAup5iilSUOpGRI1AVabEqeBrPKoilW/8tmv3XzqzxKot4gDl2UBsUc2xFsuNEEMzcRaoGBc2ztlQ03ZmGBwqBTdXHFQPlUkwF144Woq3iYNgcuylYFXGnFRiRUUGACm3kIx7KogP5rqcmGHeM60MQFLDEHMVlYGPRbNMQeYoJESuByvpv9taMCWJyGFZ4yGY2xFy5+FW4qoQ5NieygiHTfVxxB7Kljf1ePLspnAfwcMzSliPB9fDu51FK/jsVyXE9vZRJAIfgcDUA9Pw/Scj20trkqfKcV76DO+LKeT2A7KkxvMz8EFjQ3Den4zlbHvBowjUNTY3xtzJoLsbADzNiTQXw3jXMfcaKXW6ZtnfsoMRGQ3PA9tRUNkYHgcDSkFmVmwHKiykjU2YxA5VJGwGnE3FBCQHBOVqrN2B4At8TT6fGC2JqqRrLnbEk1AkYDzNbyIcO+puWv8Al8Bi37BURvSQkDMXJPM0IkNi7Yu2NzwoLEFlA+q2J5VCbOFXE2orioCZcWoYK1hibZcaioRZSScedC5IsvzqMC1tXE+Wi7BFJy5UDbRR66tmS2ddYkXx4Vydtq9WPgAcb511VAAuczjet8NqzyBiWwGF8+6hbG4zyFEmwvxNAsEFznwFaZRsLIuZpXYRrYYschRBIBJwJxJPCqNRkJYYjIUFiiw+1mPOhq1Hw4j76IjvjIcBw4USbjw+FBxopDrY6FNuZHD40yxpHwu3PjRFlGGHM0L8sBzOdQRiBi3yoAM3iOHKlB1HwKSBxNMQ+bECgOAyxNBmtmbdgpSDbM9lFYlGLYk50ADAeUfE0MD5iT3ZVZpXkKBIGAGNRSFlGQoF7C5P2VYBbE50jHUQBkM6BARxvc59lNqUYAUxOnDMmltfE5UA1XysBzqA3ysBTaQcxhwFQqvEUHvBZjjkwv8AGpcrniOfGkJKt5e0d1WXblXa5RBBxFSkKuMVsDy4VAznA2BoM+z6ht9/Ju49uJNWzmO2m9SNkBcKr3jLAa1sw8S4VeR9SEX+w18wl3XQ4Ok+99hsOpwdP2r7/bxpLtHVkgSePaRM+iN1KxGRismki12tjXS9lx7SD3N1GHaw7BGfp+3aSbocnqbHwTSWDqVBjmbXlqOpReg96GB4gd55UxsL3OWfZ318492bPpXtjrp68+wimg6xstzsjCyBlbqJ/NgGnK+7Vmjf8VhekX2/0r2/u/bvQ+uiIdHO03Mm9fcEDbbnqw9GzblnOlj6fqemHwwwyFB9IN1NwbN32vQ1g3JOPG5r5d1DbbKT211vd7ZPV6P0vrO2n6HuBd1hgSXaneNtnxPoK/qZeG1+Arqbjf8AtHee/wB0bdbCbb7no0w3zGWIxSn9TDpWRtWhmEad4Wpge8a3AjUMfhWWfquzj3m16bPKRuN4kskCHK0AX1Lnh5xbnXhv7fdA6JvOje3+udP3Knd7H1ju5IGWSSYya41g3Mt2f00UgqnYK2e8Np7ci9zdE6n13a7RoP02/SWbdpHpZo0jlijLSi2oeMoO+1B6leobabe7jpi6/wBTtY45nYowjKylgoWS2lm8OIGVXTblottJNFC+5kjW428On1GP4V9RkW5/iYV4GDqO2l611vde3xDtusb72/Du4dgroZv1rHcSaXjBxmXw6sL5XpejD2ttt/7Z6j0vqSQznbzN1RY9LTTloA0kvVZncNGsUgP8wecgC1MD3HR+r7brfStr1fapIkG7j9VElAV1FytnClgDcc626jcCxuchzr4t7Xk6K6+2977t/QSdKm28mx6XIkyELuI5Gk09SvbUXVrxg+BOPiYGtG+Mu6Xr273u86bB1ePezjaTTfqH6pCiv/5H9BHEw1K0egoIxpe/ivjTBl9Wj6ltZ9/uemRm+62iRSTocAFn1+nY8f5ZvVfVepQdG2EvUd0G9KLSPTjGt3Z2EaJGuF2ZmAFeNnboPRfeHVOob3YbeTqkkPS32S6I03Es88ku3ll2+rxFtTAvp5Y10f7kR9IfocI6wNqdO92Yg/VaL2O6hWX0/U4aPNb6c8KK73Serjq0Ur/pptlNBM22m2+6UK4kQKx0lGdHWzCzIxFb/FjcjDOvnDp7UM/uMe4X26PtX0dFQsFMXTv0yHat0wKRa7l8YcS1VSbbpu66t7LX3O23PV9xsJR1SDcyIrzSCCH0o9zGSNfjJKqw82VTBl9LYYeJhVZfRm5I7BWHoPRouidNi6ZHO86xNIytIApUSO0npoq4KiatKrwFdMKBkKKrD3yvjkThQKMMRlxF6sK8viOFKLjy8M1NQZJIg7N4jn8qrMCNgfMMrnOrpMZGKYNxU8aW4cciPsNZaZ0jVSQBgOAzFWjVa4sw+RoOCPGo8S+Yc6NgwDxm1+HA1Ada5HA9tQgEftqBg2BFjxBpdAHlJX7qCXxs2fA0GRG8yg1DrGDDUOzOlEgvpY9xOFRSmILkoYdudIFjJ0m6nhwrRSsqsLEXoKmh1YajbkcaqKbmPIiRfwnOrgHU2Bv2GiHGR8J5GgzCQMCCCpGYOIFESAcQL8eBq2RMda4MM7cRSMkbD8xRj9QqKrlTWM9EgxRqaKUSrpYaZFwYcjStDIg/KbWn4Gx+RrOzlCXIIYZ91Btz8LZ0thfQ+P4TzpFkLqDg6nyuKYuD4Xup4GoF0tG1lPhOQPOozFGDMLcGPCmBDgqcxn++oDqBVsxgaKDWZbqb2xFDSGGpcCeVBVGIyYcRQXWhK+YZjnQBGK3VxkcxQYeIMhsTh2U2sepjhcZHnSyJky4G/wAKgh8WB8LcDWZ420q6HTIuI5GtJPCQYc6RR4APMn2iiqnf14wCLG+Y4EUIpNSlb93eOFKxMUgYYrx7RUkRPVDqbJIMxzoLyoZ79mBpVLLqJ8QvnxoRs/hJ8WFrjsoq40sRnc4VBE0up4gm9Kt1LN5lOF+ItTMoC3GBtnQBKKA+Q4iopSLuCmIGJHA0koVoyfpGJ5inVcNa5tj2GlfxnSPCwF2HOgwbRtDtG/FsD2VvuMXOWQrnjL1F83+NbV8VjHigzXtpSGH5YLNiDieypp1DUfMcjyogiQ6vpHDtoG4Nh5ePZUUt/UBU8M6BxBBwZcjTMBbUuBGVITqHqHDTwP7aDFvW9QaBgGtfstWjbm63+rJR2c6z/wA12k4OcB3VbtyTCNGLqSGNKLn8NiuLce6oVGkkm5PGiCoGHiJzpVUg2fEjFeVqigC0i8hkTxqAD0yBwwqagrlRiTjYUApLEMcDiAKAM/lK4/dlWcJrdQ5uTiw4VoNlUcgbVjWTEoDiMGtn3UFjt6kotgi4Dvq23htkgHxNIF0lLjLJO/nVjDC7nuHCoApJUBcBbOgNKM3w76KliotgOZqKoBY5m+ZqKUlmYWwFszQKi4GZzJNHWNRticsKA1sxOQGFBbEf/MRLfNrmuoW1HSBfnXJ2qr66OcfFheuqxIFvqb7K3w2Z5blxYlmPhGAAoXGrAXIqF8NKcMj+2hcIAi4u2ZrTJGDStoJ8P1AU4CjDJVwAFAWRT99AMTgg1HnwoHOOLYDgKrLtIbILgfKiyHDWdTHIcKsGlBYfIUUojbNjjQbTfSM+NMxYC+XIVETSO05moJjawwFA2XE0xIGJqu+o3z5CgIBJ1NnwHKmoeI9lKQO+oqFxkMT2UAG7qYC1Qm2dArWAucaS9sszUJ1Hs4DnTWtni1ALWxPH5miBxPyo2tic+dLqLeXAfiNAWYL38qQ3Pmw5CiByz4saOC4mg943iXUOFFTdQamTW4N99LHgWTll3V2uU9AgHOiSBxry/vPfdb2cfSD0QwhpupbaGf1pHj1B2sI/y45Lo/1/toO2OmdPjBtsoNDEFisKC5Vg63suNmFx21fBDt9umjbRRwxk6ikSqikniQoGNeN6y/WNr7p9ubjaQK/U99t99FvNt+qmXY3ijidZGDKfCmo2Ii1G9a5fd42HRuq9T6rtl2266PuRsdzt45Q0Ukz+l6JimkEYCSesvicDTjfKgug9qT/rEfqXVJeo7Db71+p7PZzoC6bhixT1Jizakh1H01VVAwve1eimgi3EZhnjWWN80kUOptzVgQa8v0L3gvVOr/0TcybF91Jt33cEnS94u8i0xsqSRynSjI661Iws3DKsHvCPb7Xr/RGmbqUu26m25j3+z2Eu6YyiDbhobQ7ZwQFbElLdtB7UBYwIiAqAaVUCwAtlblVB6f00osf6LblF1aU9FCBq81hpsL8a+ebWLfdU/rXSejz9Q220j33S2h6fuZpoeobfb+op3s6tuW9VIZVB0eI5N3U0XQOq77edfh2fV+pzy9K6tso+nxS7+UIsAXbbmdZAWtJdZH83ZQfRIdtDt1YbTbpCpN2ESKik5XIQDGjJCm4XRPEkightMihwGGRswONeR6DtP+q+nye49/vN0m53U+5/pybfcSwJtIopXghWOKJlRm/L1OZFa5PLCsnuDadR2/8AbNp91vNxD1zpfTTI+82u5dWM6oBIWeJgsit237Kg9oNltY5zuVgiWYkkzrGgkucCdYGrHvoPsdqDKTt4z+o/nEov5nZJh4vjXB97rKPZnUd9DPNtt307Zybva7jbzSROsscRsSY2Gocw1xXneiHp/uHqsHT4d31HZwdP2se5DS7ndR7jqccxbVN+Y4/8uJL3IGom3lW1w99/TulaAh2W20AkhPRjIBNrm2njarGg27SJMYI/VjGmOYqupRyVrXA7q53UOpbT250bc76ZWGz6bAXIuXcqg0qoaQ3LE2F2PfXA2nvPebvewdMhTpe66jvo5JdlFtOoDcorQgSSQbpoo7o+i+llBUkWor1zRK8iSlEkkjv6bsoLJfPQ5FxfspZtptd1YbyGObTfSJUVwL521g15eD3d1rebToU+z6dtGm640qLt33EoaAwqzSGUiA4RaCslvqsBes26/uRsoP1e7B2LdN2E77eeN96idQk9J/Smmg2hXFVa+lWYMwFxwqYHtf0u1vEfQjJg/kEopMf/AIeHh+FJLsdlNKJ5dtE8wtaV40ZxpxHiYE4cK82Ove5JOpdW6bs+l7SRulCKZpX3cipNHOjSxpH+QSJNK46vCOdZ+p9e61vG9p77oKQpterOZHi3cskZbXtZZlil9GOTwra9x9QHCg9gQeOPaM6AY94+2mj1mNDKFEmkeoEJKhreLSSAbXywqFQc/nUVAQcqBUHv50pVhiMfvqCTg2HbQZZbayJPgwpGDDHO2TDP41fJYseIqqzJ5cV/D+6stFDB8sGFVqfTfScEbLsNOyLJ4kOlhxH7aRjfwSizcDwNQWMobvGRpdRU2f4NQQA4XII5USHyuGHI0DUrKGFiLikJdPpJXkMbUwkRhcH9lRSaSmAYgcONHU48wuOYp8CKTy/5fuoBqVsL2PDnUwbBhjRKq2YvSNGc0axGV6CaSvkPwNV6wjWYaVb5Xpw5+pbWztUJRwRmDwqKGkjFD8OFVOATqAs4zU5EU6gqdINiMr5EVGIOEgt20GMxCJ9URKK3mXhVut0FnGpMwR+ymK2vG3iTNSMxVYcw4ONUR+ocKCwqrgMPN9LDjQIbzocRmpqu2gn0jgfEEOXworOpN8nGYPGopmZhaTTccbcqjOtw17WzvyNMCAcMUfLvoAAgxtjb7qCSAEAnGxpXQ6ToNuQOVQICpGIIwNqC69Od+BBqAhjYah8RSIAQShtj8KZHIUBgRbC9Kuhi2ON+GFFVyA61NtLY48CKzSFogQRYeYDMfCthDBx9QscDnVcoUqbeE8jlQLtnDKtjcWJHdTkAoLjM1gQGJ7AYAkr+0Vsib1EQxtccQcxUoZwwsqm4PA9lR3vZDgTzqajrJYZYXGNRSrsTmMhUVGUKNSm33VU50xsZBZrXvTlLtZTYDEjheqt3IVi0sPMQMONBls6qLY2yPfnVkbiM+leynE9lNEukBlN7kjTStGunTbxjMdh4ig0stzZMDzoBgvhbA/fVEUxi/Klx/C3Pvq/SGxOPLsqKGkg6v/RrPuXyVMz5hzHKrJZvRFj4jwP76oRPFrbxM3iI591AhS4LjBMAe+n258bxpgpxv99OQAgdvKxuUFUPqjkEgwW+POxoNV1iNh9X30HDHxHC3AcqYiMLbn86VWdsLWIzJqKjABQwwtjSs+KlRfhfhRVACVbHiL1RJKdJijxYfVwFBXuJCCUvcg3IGWNLtEsCThc3vxNIbBS18cyTxNaYgEWPSNRIxPfQO19PhGkZ3OdEhVzNz9tRlZlOo27BUGgC+GVRSqWKiwsOZoKtwSxvicOFFWuuAJ7aUa9BOAzqArYKTwxNIWCpn4m/bRKDSFJJvUYqpJ4LgO+gaF7TxgDwqR866IDysSTZRmRXJgLSbmJR5dWPaa7Tusa6eA4Vvhszy3I2mNRhcnIUoIXE4s2YGZ7KVdcrluWF+Aq3SqDw4sc2rTJFjaQ3fBRkoq26rgo+VRVNvEfhQdhgi5nO3KgCgudZwGQAp8AOVDHICwpCuttOYHmqKgIY6zj+EUTq7hTYClJubcKBdOo44gU+AoXUUpYnIUBJvUAtQs2ZNAi/HDnUUSwGHHlVZa5suJ4nlRNgMBnlzNQYYDPjQQC2AxPE1LgZYmp2cKgHLDtoARfzYn8NG3FvlwoagMBUsTn9tBCeWA5mgOwX7TRwHaaBPM/AUHvSpdfN3WpCikhje+R76s8p/hP30rCx7G++u1yp5cxcc65vXelydX2cMe13C7bdbXcwb3ayuhkjEu3fWqyIGQlWxBswNdRTcX+dK2kY3seyg8jt/anXNt1TpnUU6ptZF2T7uXcJJtpS0r9QcSbnQw3FkVdNoxY243qD2h1Ldw9b23V+obeSPq24j30L7TbtG+33MHo+i49aWVXVfQUlSMceFdzrfV5Oi9Nm6n+il3sO1VpdykDIrrCis8kiiVlDaQvlvesEXu7ZCDpc3U0/pbdYY/o4tzLDrEfpNOs02l7IGVbWubGwoL+m9H6jBvf1/VN7DKyRNBFttjt/0sHiZWaaVWklZ5PCAMQFF7DGsXW+j+4N51vp3VendQ2cEXTPWMEE+1lmZjuIxFJ6jpPHhhddIHxru7zd7bY7KXqEza4YkMg0EEyYXVY7kBmfJRfE1I+obU7CHqMzjabeaNJAdyViKeoAyq+o6Q2NrXoOX0vou8g6lu+vdS3Ue66puoI9mphiMO3hgiZpFRI2eR2u7lmLN2C1UdA6H17pfU+p77qPUNpuYuqTDdSxbfbSQskqxRwKEd55Bo0xi4IvfjXeM+3MZnjmQxqCWkDApZfMS17YWxqr+pbE2B3cCXIUfmpiWGpQPFmQLioOFD7c6t0htzB7f6pFtem7qWTcDb7nbGeTbSTEvL+lcSxrpLksFdWseyl6l7Ym3HtZfaXRt0m16e23bZ7mfcRNPMY2zdCskShybklgRjlXfO+2C+if1MJO4/8AhyZE/M/8PHxf6a50vWJU9xbfoU20eNN1t5t1BvhIhjYbf0xKjR+dSDKMTQYus9F9wdY9t7nob9Q2abneRvttxuU20vp+g6aPBGZ7iTtLEdlZOpe1/cXUf6Nuo+p7HbdU6QxK71NpMSykaGh0ncfy5EA1qb44i1ei6Z1bb9Th3M0AaOLbTSQNJKFCOI7H1omViGjYG6tWPY+4V33W910hNt4Yduu7g3sc0c0U8TyNCCvpE6SGQ4Ggt6l0n+tdEn6T1V1L7mH0tzJtwUGo4+pErlyNLAEAk1m6b0nqkO9g3vVN/DKNsjpFFstr+lWUuAvq7r8yTUwGSrZQTeus+92Yk9NtxEkwZU9NnUNqbFU0k3u3AUo6hsBgN1AbqHUGVBcE6V+rIsLA1FcrpXtpOmdc3vV13PqwzGRtjs9NhtjuWWbdlWv4vVkQNkLViHtDdQjc7HZdQi2/St1PJuTbbA76H1pPWli2+61hQrMWsWQst8OFdHo3X9v1UTpuoR0ze7fdy7FtnNNGztJEFZjEUI1jxjKupPPDtVEm5njhQkKGmdUBY5KCxGNB56HofuPb9S6z1GPqGxP9VSNI422s35XoKYoix/UeLwMdWWOVsqq23trq226N0TZf1Db/ANR6DIrbPdfp3MDosLbbRNF62q5Rybq4xthXpX3e2if0ppo45NOvQzqDpx8Vib2wONPDPBuI1m28qTRPiskbB1Pcykg0HI6N0fe9KG3246j+o2EG1EXoNEA7bkyNJJuTLqJs2qwThzrr3YZi45imKqaWzDI376KIIORoEA540Db6hbtFC54HUPtqDLJG6uxiP+k5UgmF9Mg0N25fOrndS5GR5GlZQwswuKy0Vlv4lNjzpCQ3gkFjQ9N48Yjh+A5fCp6it4ZBpPbUFbIykFTZhlfI9lWJIH7DxU1DcDHxJz4iq2GnxeZDmaC6q2QE3GB+yh6mkXxZftplkRsmBqKQBSbHwtyo2YZNfvpmCsLH50lyuD5cG/fQLd0+m69lH1FPG3fT0hFsQLjiKCHHxLnS2Vhe1HShFx9mFKVZTqU94NRSsj5o2IyvUElxdltz4imu1srjmKRnCHVYgHzA/fQBkBGqM2YYilNmF2Gm+ZGXxqyyNiPmKrs8bWBurcDzqCgs0BBOMd8LY2q14op1DC1+DCiwUghgVvnyrOmuG4UFgM7YgigFvSbS91VsyMu+rbNgwa5XPtFRninjKkHu4iqdRhIJNwcicu40Vfd1e+BDfDGgGZWYFe0WpVlRlKg2IxWmLqSrg54GoIrgFgcMb49tAaGZsqa4155j7qFlLtcDIUUmjxjSSMKEmvTYgNc2ptCl8MLCgym6gMc7/KoMU6hZ8AVDC5HaKVPOXQ6WA4ZH4Vo3WsaWwOYqsLdSNNi2KkfbVDRbldNn8LnLkauKqqYj/trM8aSGxFtPmBHGqwJY2AjfUgyB51BrVCowbE4m+NZZWZ5lBF1TIjt4053QPhY6T9V+VIjXOtl8JPDHAcKimGggMMGvZRlTOChuRrDYG2dRSp8TeY5A8KayqLq2J+NBnKqwJvqK58DakE5QWc6QMn51ZONNpJQDwCrnWKTU7Wkxt5QeAoNCku2qTwg5XytTqGOowjwnMnl2VmRmVbsNaHBb8K2LpCgh9aj6alAOm14/EwzFVSKXDcFuMBwJq/UG/lizD4VTIxVmLEISMV50UYCFXSeGFzwppZUQa74jgOIrC02ltUakq2BLfuq5ExvIwJGIYnC1MASSSzC6+BB/vHsqCPSVxtqwPYKcFVOZK/So51NJUFdPibImorO4RQwUXvkewVrOsoLWFgO2s+41Fb4CwFagpKjxZilAKXGJJvQXSFGVRVFhe96iqouLZGopQ66c+dLq8AABxtTjBD8aBt4RyqBHZgb2yGHfVBEjvoDC+bEcKM8yrbHzG4HPlSayq6VsC2JJzqi2F1TdIFN9JAHK9dQIXN+GZJrkbGIybmNh5A3mPGu9Zb2v2WrfHZjkiqqqBn2Cgbs1gLAU5awJAypUDWvkTjVQSABdje1KlsWOBP3VGGpgl7jM0xAUXtRSs9sFxJyqKCBYDvNRRc6j8KjMPhUCm5wv8qOlVGNS6jEnE0C3YSeAoD/sBUwXPM0tzkoueJqXI76CHm3wFAkAanwHKh4s7jv/AHUtsbtieAoINTeI+EfbR7FGFS9zjieQo6SfMbDkKAXAwHiPIVNLN5jbsFEsiCw+VLqZshhUUfCuVAkmoEPE0bAUC93yFGx7qJYcKQtfjfsFB78spFs78qQa2BQixHE048J0nLgajDiMxXa5VajxEMT3dtWBQMhSPkJB8abWOGPdQcH3tvdpsvanWDu5khE+z3O3g1mxeWSCRUiQfUzHICvMbvc+39z032X1fcttpun7SVdvvt3IiyRwn9DInpzFgdH5thZsmtxr6J4z2ffUCgdpOd6DzW66psd77Mk6h1nbRbLZbjaO3oboD00DBlgBDqvm8JUW414w7/ou76f7MZ+qbOA7XpjNG3UIhuens6pt9vLFMmqP05kJ8LFsMRxr6uzC9szyqtoxfWbKbWwAyzteg837EWL/AKYTbrs0gg/UbxUiQEwSxvuJH9WFZFVhDJqJRWHl5515rc9D2v8AVtx7EGxi/R9R6hH1lHWFdKbHGbdR69OBG4j9MfwyWyr6R6jHDTZefOnBsLaSBUHyH3gm3G590wRw7Labn9Imz2W23EEm43c4ig1xP0uFNCRJqlK6kvZl1G1q6/Tvd/Rer+7OgMN3GrQbPf7JmkcfmyMNkUdeyUhtN89Jr6P6g7RalOnNSAaZMPlSbrou72nU4enPDP0eD3Km66rttoNS/wBOMcY9Voox4ofWClrCxAPC9dnoXUfbJ9/7wdGl2yR7zpkEcZ24CQzzxzSu4h0gI7LHbVp+Ne9Di9sjRIuLH4UyYeE9/baPp+42XuPbbRJd40e66Z6ixB39XdxD9HJcC90niVQ18NVczoHt7Zbf3FH0GTaI+29rHdbiOaSMG6btYjtBrcYhPUnIGNiOdfTLlMDlwNQ3zHypkw+SvuvaO86J786hJPtJt1Ju928G4JVpR+Sg2rQP5lvIp0FM2rVvt3Hveubfqu86j05emT9L26dN3XU9qd7tXlDON6kTCaJEmLadQN2YZZGvphsh1DynAjlT6hwNMrh8pkXoO22vsfZ9dkjaSPeyyD9dEsEv6Mpuv04eJ3kdYNRQIrt+G+OFek9sb3of/U/uXpnS59uq+ptpo9ptyqqHWER7lkRbAWcAPbjnjXsjjnQKg8KmTBbMMjfvoaiMx8qYpbmp5ULMMj86ioGByNAqDw+VAg8Vv3UL2yJHfQZpUbU2IYcjVV2TmOw4irndtZwuOYpQ6nj8DWWiCVeOH3UWVXGONFkU5j4iqzG64xt8DUAKMmKnCluRiB3gZfKm9Vl/mqR/EMRR8EniQ48xQVK4XFcV+peI7qcoj+IfAildB9QseDrVWqSA6ra4jmRw7aKsIC+Zf9Qo6ARgT86ZXVxdTcGgUtiht2cKgrKOnka4/Cf2UQWIwIP30wfGzeE0GW+IwbnQKdYNwt+YvUEgPA0Q1zpYWaoQb3Gf31FIXVTfIcRR1K2RBogg/tFIVAOXhP2UChVDaSMD5T+yo0ZIsGI5caLRKRYYciKCgnC5DDA1AFZyMQDbAiq30qwexXgbU7B0bVqwOBwosHIINiKKoePUdSG57MDVazFQYp1uv4q0AkizJcjA1W6gMCLjVgQcRQUE6LAEOoN0P7KvGllIFj9S1nl25IwYDtGFvhWeKeeIhXsSuFx9QoOiVQlGsMcKGhdeXCqUmV1sHsynykWNXFW1jxZg1FDQus55UCg1jE4DnR0nWfEchQCXc4nlnUFG8X8tcT5hQCnCzeXx/OjulGkYnAg50jKAo7To+FA/j0+oreJuB40rB0U3CsDmOZqwLFe2FkwHfSM8YBkK+FcsM6Kx7jACMgeLFmGYFVoull0EqBiOK1aNRLSkAXxYnhfgKjbhFJEdtAGI50Qybl1BWwY3xJwqt99EuCpqkP1UmppF0hhozKgZU6xIB4VuTwkOXbRVQkRyTIzA/UbXPcKYTIF0qPHzYY2o2QnQpUWzHb30PSibxSvp5Y3qBWljY6tLaRnpwsar9Vo21RLj28au9KJTeNvCeJP7KgcKdOphyci9BX+skktr8B4FabQWcFrPcZ3xHfROmQkIQ7fUSLX7qqaJxjEuWZvjQOYWAxIZfKWqQjV+SbWXyNz7KqDyeJLBb42PGg8i4eAoc7DnzFRXQAZgVNgRnakcOQGv4kOIqmPdRSDMrIufbTPNEV1I5vlaopJQShOrBvEMOeFXRAlB4jhhWZ5EAK68ssOBpo9wAFsS1xawHEUGgL4mFzzoBBqbE886pM02oWUC+GP+FIf1DvYtmMhhUFjtEkZ1NzwvVEkpe/prYWsCcz3Cho0qxAub4tQNzZbG7Z91AkMAMnqOL2GBPCncBn0Cy6Rw7eJqxtMf04gWUcr0Y00Kbr4mONFX7QIkkQGNjXWUjMCuXE1poxpzauqCQMq1w2rPLcrksQtu00WJUXwoKSSWtnl3UCSzWtgM60yKqwFycTiaW2s5mwosxyAqYjADHjRUIGXAZ0AAMbdwqXJyGHCoWIwAuxqAnDvoWJ7BQxGLYmgSzebAchmaAlvpT58KB0oLuaV5CgsAAeA41XmdUhx4XyFA2pnxAsOF6IUDM/Ol9Rfpux7BUz81+6xoG1qMEFz2VNLt5jpHIUDIq4Y9wFD1L5Ie9qBgqLln86hccBekvfzN8AKN1HH7KijdzkLUNBOZo6gciT3Ch8/jQHQvHHvqEqOIFKdHGpdBkPsoPoDabWakDMTptjwJ40ygDC2NRhcdvA12uVWUsfEbq2Y4UUBXwjhwo6xbS2fEUh18cLcBmRQeN3Purr/T5vcO36iuwifou0j323KpORuY5SxVgPUuuMbRWx8djlhWnb9Z9073dxdFWHY7fqibOPqHUnkEzwQLuHZINsiq6u8n5ban1AYZY1yngm95dX6Jvpum7rpX6ISjr0G5RlRo45Un2+z9R0VZiZ4kkvHdQt8ca9N1LoU+46kOtdM37dM6g0I2k7+km4SaEOZEDxOV8SMx0sDxN70Rytp7s6nPvOjxHa7eKLd77ddJ6lFqdpId1tI55GeGTBXjf0hbUoNj8rW611xj7mgRNmdx0P0pdszLKI5YpIW3RWQa7htA03GF8bcKtb2fHHsNlt9nvJYN7sN1J1GHqDKkrvuZhIJ5J420o4kEzXAtbha1Z4/aXVUfrMq+4GabrkaJu2k2cZ0sieheFVkUKvp3UKb87mir+g9Z9yb+fpzdR6fAmw6lsRvvV2wkttWIRkgmkkJSRnDnygabcRjVe56p7nPuif290+LYLCNmm/j3k/rNoRpWg9KSNGXW7FbgqygDnXb6J0/c9J6Vtem7nd/rm2sawpuPSEJMaKEQMiswuAMTxry++2/Wt179bc9OO86dCOnrsP17bNZ9rJIs0m4YNrZcArLocEC9xjUBm967z9Ds3Mez6fuH3u66b1Hdb2b/AMrtptnqDFBqiklErKNAFrXxqqX3n1eb2kfdPToNlJFtJ5tt1EN6zR6Ipxtzu9uRob01X8wqwvp44Y9L/oqLbvsd103fPB1DZjch93uIY916zb2RdxuJZI20BZGkW4ZLWGGVcUdFT2p1CWDeDqHW+mdY2+8imdYPUEb7iT15Ynj2yHW+4d20sQqoMKaDsN1D3Juut7/pGx/pzQbDbwzybySOdrSzqxSDQko/Brvq8pUWvjXK2vuf3ZL0zoPUn2ewA9wSR7aCEPNq28kiPIJpWxEiWiY6FAOQ1ca6ntToPU+k+112W43Xo9c3Y9Xd7yRFnKuVWJEZdSq/pwoseeYvVUfs3qUXT+hdPTrvg6DMs8DnZITIY0aKMP8Am8Eka9s/hQLuPd+46T03rs3WIon3nRJoICduzJBOd2sbbdvzdbRC8tnvqta+NU9I9277qPUH6Pt9z07ebmTbPudtu9ms7beJomRXh3KuQ2Ie6MrC9j4Ratj+zZtzuOtSb/qjTQdZaKUxRQJC8Eu20DbSxSh38UXprmuJx7K3RdJ6x6r7rd9ck3G/WFtvtHMCJt4g7KzSNtkcLLIdAGpmw4AY00HE6Z7o9xbjpnRuvdQ2myTYdWmh2sm2gaUzxmdzCkwd/Ay6gPBpuAfMau6DN19vcnuCPe7naybCHdxIIgkokAbaxtGsLPKUXMahpxa9qCey+qQ9E6b0SHr1o+lzx7iGU7KMs3oOJYVYerbBr3534WrbN7W9fqrdRO/f0JZ9tvd3sRElpN1tEEcUgk8yL4VJQcRnQXdZ6rvNtv8Ap/RulRRnqHURNIsu5LejDDtlUySOsZDOdUiqqgjPOvN+5Os9e3Ht/rnTlfb7TqXTZdvt97LH6xSWDdmMxy7ZldHjLByGDE2sbHI16vqnSD1ObZ77bbptj1Pp5k/S7pUWUaZlCSxSxPYOjgA2uDcAg1yt97O3G86bvNmnV3j3nU549z1PqDbeORpTDo9KOOPUixInpgAC+F+JvUV0+g9Fi6BtZNtFpBmk9WWKIv8Ap430hGG2jlZ2jQ6dWnUcbmuprFCFJVhjXcyCecKBLMEEYduLBAW035XptK8qCXByNSlMa0PTtkxFRVEiKXNxVZjvx+eNGRZg7WcEciKTVMM0B7jWWg0SL5TQ9SRfMlxzFN6tvMpFT1VPA1AomjOF7HkcKDRI3iAseDLhTFo282PeKT04/oYqew0AtMmR1jkc6TWt+KHiDkae0oyYOPtoF74SJRVBjQHUqjtW/wB1WKARdRqHEZEVCEOWXI1UyMG1RPpfk3H41BdpjfAjHkaHplcrkcr40izox0TLofkcvgae9vKwI5E0A0o4tc93Gl0lPMSV5/vol4283hbnUMoXzeIcwKioY74hjfnegFvhqN+INDUB4o8R+Gpr14hTcUA0lTbUbcKDow8QY3HDmKbUzeErjyoAyDwkDsN6gGksvmwNKofFS2I7KPjRuAU/YaDhx49QwzsOFFAh1fzYN2cajq+k+IYY5UWQsvmPMWoBdSg3J50EsxANwb9lZZtuSWZQNQx08DWhVQXUnI86U+mHFhe4tUGBgjLc2vl2gjhTjWpX0mOWV6eYem5ZU8LcDzpwCVS9gL2DcaCtZtzqPlbsxvRWeXG4tc8P8ackBm1ktbitIL6bhNS53aiqJ5rrirG7DjypBKCTaM5eHvozAn00DY3vp4VEjCASSXtcgac6Bv1LaQioB+Ik41mkn3G4cImCjIKPtp3fzRxkANmSMSeVEKqIbrYZeHM0FYhY2DXY3vp4DvNWLDGGLS3w/wBsBVioztYn0UUYKM6dVjQly3HC+JqCsjDVGhUDLVx+FKY4rapbmU8Dwq8NPIdSgBRkWw+NqURlj6kvi5Y8Kiqi0aqI9IF8NVqa8Mfl8Z5EXq3A5JpX8RFIssCm0Uikn6AQSaBC0L+YEHkBalMjMNLrgOyxNWtJHh6xAPBSQPvogsReOzL33+6oqkaZvDpCsOORo+lIuKSaSOBFxTsgfPBhwypDGoAZHIPI40FL3LDWMeOmqxZrx8RiFfD5GtDNIQDoxU5jCkb05SPW7sMKDLJEt9SWRswh/YaKlXKgAq2bBsj2irCqpdAdS5qx4VXJDKAJAdQzt/jQCRCpuDqBOGnKjEToK4eE3CjOgkl1CnwBji3I08JCS2cXDXBPaKgsYG1yhAz8PGibalzReVOqggqrWthSsXsgOIvj8Kiq2tpJDeEHwjnSRBj49WZzI+dCd10thZjfSP20QgVI0T5jiaBkDSSs+BVMB2nnTguSLgcTURGSMgHic6K6rnDLCopoWJ3US6eONdZ2wsAbnCuTtGB3KsQR4rCusCGYtfAYCt8NmeSMwVbD4UAQq2viaOZuchQOJtxOdaZQW4HHh++gcfCMuJoMyDD5mqx48EXw0FhYDBfiaUyKuV/2moIgMMz91KSqmy4mioBI3iI0jtpDqY2Vj2mmIZsXa/8ACMhU1AYAgUAESjIam4sabQg8xv2UNSZX+VHUALhTUBuckXDtoEMc277YUpaVslwojX+G1AwCr5RUue6lNxi17UuonJTbmaBiwyvc8hS2GZAogqOFu+jqHC1RQty+ygVY5E/E01zzFS55igTS45VPFxP2U1z+IVL9tB71ivHPhzpQXbA+H76ZQtrjHtqNa2PwrtcoFAMV8w40jNrHgHiH2Uw1Ng2HZzpWNsY+GdBwfdHuDde2ulydWTYf1CDbi+5UTCKSMMyojKrIwYXbxY3HbXeCte7HE/ZXkP7j7iKP2vvtiiyy73fxaNrtoYpJXkKujNb0lYCw52r1Ow6js+q7Zd7sJDJA5IVmR4zdTYjTKqN9lBwn677hfrG56HtOkQPutvEu79WbelIm28sskMR8G3kYSH0iStrDnWKP3rut2NjBsujPuN/ud1vdjuNmdxHH6MvTx+cVmYaJFPAi1cnqm99t7r3zvZeqT76KCHp0G0Wbbf1CBDPHuNw0sTPswmvSHXmOVdX2lBt+mdMn33UYJIul9L3c7e35t1E36qPZSqiuzIF9U63L2LLrK2LUFHTPfu83z9M3G46O3Tuk9RfdxfrtxuomMbbJJpJdUUak6f8Ay743rRuvePVtt0Z/cv8ARCejekJoXbcBd0Y3IEcz7f0yqI2oH+YWA+mub7X2209y/wBv9x0RFMe/EXUIkO5gkieB95JuVjcGVF8ySY6eBrcPcRk9uJ0cdLlk9xLtU2p6LudtI0XrqqxXlk0+idvqGrXrtp7aD2MjrEjyyyLHHGC0kjEBVAxJZjgAK5ae6PbjxNPH1rYtCpCvINxFpBbVpDHVhfQ1u6sPvpNs3tjcQ75Jmh3Eu1hmO0UvIpeeMeokel/U0t4tBXxDCvJ7jab/AK1sfdke2jHWhu+kJBD1JNm+xaWeJpWj2voy2SR1D6g6gWwXlUH0Zt9s/wBYOnNuof1rJ6i7QuvqlMfGI76rYZ1SOqdO/XnpSdQ2zdQA1HY+qnrAWv8Ay9WrLsryk3uLpu/92e2Nxtk3Ahij3kE25k2m4RY5p44kigcvGLNdTfgOdYembfajpvQug77ZbxvcHTeppu95DDG0QkmWZ2k30+6aNkeEq3qYPdsF7KYMvdHq/TV346U+/wBsnUWF12TSoJiLXwj1asqk/VemQbyPpu43+2i301jFtHlRZWvlpQtqx4V4SLb7b+lp7e6lst5L1+PrB30sW3jaM7lhvGmj3TbsxtH6IiZSTquNOnA1z9703fCL3D0je7rdfruo73czR7GDpyTPukkk1bWWDfOhVdCaQGZh6Wnspgy+k7zqvTumSxQ9Q3+220k5tDHPKkbvjbwq7AmqurdW23TY1D7raRb2bDZwbvcLtlla9tOohjbtCmvD9R2u52vW/cI6nvZ4B1NYl2qL01eofq9uu2SL9Okuh7MJA90Nhc6uNaOjpD7c3W6T3Jt9xJ+s2HT4NjLNA28d44dv6U2xdtuki+oJbkrgGvemFy9J033Jtd37e2XuPqDw9Li3UYdlnlUIpJK6RI+gN5cMK6m03+038A3Ox3MW5gYlRLCwkQlcGGpCRhXy/p282I6J7O28s256Xvelxbq25/RybhdtKgSN4dxtnia4ZJcHHlORxr1HtfqP9M6Uo3m0kUb7qk8W1k222lRJv1DmUbtoH1Nt43bV5sBnkaWGXr7nmKl27DUup4X+FC3JKyo+PkKHj5CppPK3xoaX/FagokL6zgKS8nIUJQ4dry2+FVM7D69XwrLS0l/wj50hB/CPnVf5xyj+Jap6ch8+HYtQMbjI2+NVlj/A330fTVMQpYcjTrJFlbSeRFqCouf+ST/lNIZbYMhW/M1p1rwNQ6WFjYg0VnKyHFRb/UKUruciiMO3OrigXIXXlxFQC48DEVBjkSX60w5A4UgMsYu0QkTgynEVvOvsNVMpU6ghHMDjQUrPqFwmpeRFEToDbydhxFRkVjrjbS3bh86BlkAtLFrX8Qop9anEHSeYyNEm+fhbgwyNVCON/KAAcjxoNtZB5ZLjipyqC4m+D+FuBoFl8rHGs5V48WQsOIvcfCnWWO38PaMRQWawbqQTSqz+QjuJ4imuGGfcwpWYHjZxlaooKHBKE2Ga25UAlmKkkg4ioWZ11IuI54VGDOoe+WNhQAhEcE2xoO4wKgmxpmVdOoC5GIJoO6lSBibYAVBXPG8sTLgDmOd6zRMugM+IuNQNbRrYA+UfbWEKIZGH+oE8eYoL1Y/mKowGRPbRZQEAc3vYVWHOpwvka1mPChJuI0bw+NlFyeAoqrdMGcL5VAtc9tUO5sViJ0jy3zJpFEm4d5ZDZbXueAq+yhwtj4h4UXl20CpAka+pMbu2Atib9lXJE+sECwUYBuFNEhB1MANGGPCioeUFibIccMzUCASSlrmy3z4URFCDpHDM8SacIoUIMSce6rAoUWFRVRDNgpIHG9ME4tieA4U9Yozu5tbrOEUOyqvphrBTbMtWeXOcZmq0yfy3/wAp+6uN1COOLpUUkSKjgxeJVAPzFdFot2wKndCxwNolvb/epdzso9ztRtCzKi6dLYFvDlevDn9zjeXCy7XVcXUqRxy7m8qK5EK21KG+o86zdMY/qt/GMEWZtKjIdwrbBt2hYvJK0rlQgJUKAoN8l41RHsptvPNPt5V/PYuySKczyZW/ZTj9zj/Jy5W6WaGLiNjKGzpPTCm6YXzpBO6ELuU9MnJwdSH44fbV1e0sszLkVkPe+F6qkj9QE6bMMbg8a0GlI4jP76ozGSwX1EseBtgRSuoiuYyQjZr29lXC5BjZbjlfhVLGREZCupRhniKCnSrkDBicxkTWeVJYSJIzcXw45cK2SL6wVkW0i8aqJDBtSWx8ajnzFEOm5jfS7jTqGLDK9F2J8KkMoIJPG1ZkAGqM3DX8B4HvoSoUsLFWbE2phTuwkfRkSbm/IVeqAuCMABhasKNKXe/iCi5uOdaRK6fTwx5VKLvGEHG/zqGTShZgQTVZ3KWUMCtR5ozoQE244cBUVo2xCNHjck/aa6pAChePGuJDPC27iGPhN8BxrqNMb4X7b1vhNGeSxrKMM+HfSXbyodTHM8KrDyOdRWy5KKbWwB8QXnW2TaAuDEE5k8BUadVFkBtzqpVZ8sb5k04jHFr8hUCGR38K5cQKdYpOQXvq0KiiwNAlTgDQJ6ZOBe/YMBTCONe3vphoA4UCyjLPgKioWRf8KGeLfAURbNjc0bjnQC/YaVnIwtc8BUL6sEy4tRAUZHHiaKXSxN2t2DhR8XMU1xQLDLM1ALHnSEXyx7aa18z8KOFBX6S8b3qenbI/OrKBIGZoK9JGag91Tw8rU9ycsO00NI440HuySD+XjzHCiunM58b0xso5CkZdXibw2y/xrtcqN4xyXnx+FRRbBs+FKrMx8Xh/CKYqxwLUEN1uR5T5gKW2NiT2G9GxyYnsNKyAYXNuB5GoH1SLk+A5m1IJ0uCGBJGoFSDgcjhwohVYYjsIr5l0nZ7ToHv7e73ZxJtdhuOono08UYCxoJ9jt97tyFGCgTRuMPx0H0wzxnOQEggWJ4nIfGj6mFsbcq+be1dns+q+826/vYVln6js5upbUyLcxxfqxtdo6A5H0IlIPaa0bH3vv5eq9Phl3W0lTf8AUJOnybDaxSSDboPV9Kb9ertC7ERqWSwzNsqYHvlmRywjYOVNmCkEg8jbKmLO2Y+Zr5l1TrA9ue6+ubfon6bb9S6n/Sv06PDfbyPL66PLuXRoxGpZ1vITcmwAJNdnq3uXqvSt/tOgbneRR72Tbvvd51KPp+53CInqelHDFtdu0jXJvd3e1hzNMGXtCZGz7szSWkAsHw5HKuV7X6tuutdM/U72EwTRzy7dm9KWFJVjaybiKPcBZFSRSDZssRXl9p7t67u+pbPaS7nbbHcbveybWXpO42kiSRQgS+nNDupZEj3LHQjEJmDhRXvdTgWYkDsyqDEWDG3IGvFTe5Ovxezeo9aM22/qPS95utvJaBjFNHttw22Fk9W6FhZr6jWzqu89yL7nXo/Tdzto9puNnNvUkfbNJNEYHji9MD1o1k1s/HTag9SVFrY49tKAU8uI486+fn3l199wem7sRdI3+y28Em/B2O66gH3E4aRYh+k1LEgQKWuxa5sMr17DoXUZ+r9H2XUdzt22W43EYebaSKysj3KsLSBWtcXFxe1QdHUG449udITpvc3Bz50xjU+bHtpL6chqHZnQDXpxF2WmuxFwPnSk3xBCc7/uqq7/AEgkduXwoq4tbNvgKS5PlBPaThUW3E/On8XAioMckJZ2Lt8FwoBWTIAj7atk16ziKT8zmKy0AdThkeRo0pVznY0miRfK/wACKgspWVWzFJqkHnNu22FHxnEMD8KAEMvDUPtoARtkBems/wCL7KRkY43APMCoolF7R3GkaLG6sQ3OofVXzNccwKI1HEPcd1AoL3szWPdgaP5nMGoUYixa/wAKQiVPquvdiKCMsnmAF/vpQb/RYjPGrBqIuHw7qVkY46rEZG1RVLxKcRqU/Ok9SaM2Y3HAsM/jWga72ZrH76DC4szi3bago/WKDZ1IPZiKQmNzgQgPHiDV2A8LMCOGFVtCjAgY9hFBW0MiY/zV5DA0Y5UBsMBwJzB5GlEbIdNmU/SRkarl28ji+oo34r3PyqDWWA8YOH1Chrs1lF1bInKucDOjWdrsMNVrX+dWa5wNPqYZqCMfsphWxUOKOcsgMrUUsqlcguFYjLIQHD3IwPCiYnLXkJIb5fKgu/URqNIYEg2vWKeW5DopYhrXOWPZWgRqhJYFVzHOq5QHjkAuq5jDE1BRd5Hs7EhjiqjAGjIvpoUIJkbBeVudWQ6AQS2a/I1nJaeVUViTjj2c6CxBpQ2UWUXN+Jq5A8f50ltbjL9lBowGMKEmwBbsFNKoDIpYk5t3UUFR2UlmwJwHbVpW40KSeBPCiVLMFOCrjpFWWtgKilChcqlGhUArjbnpO7kmZ4N28SMSwQEgAnurs0Klku8z7xg6es8cLw7iQyyROV1nMiwYffWbrrTrt4VglaJnlCllNjYg1ug/mbn/AMT/ANRaw9b/AJW2/wDHX7jXJJP5sY07NfSzjpHUhZhv5L/5j++tG1m3+3nXa760qSYRTjO/4WrqDyjurNI67mVI4TqWJw80gxVdOSX/ABMeFe/3OPHrcyTRIvZVdSji6tgRWHp8z6p9pIbtt3KBua5r99a554ttE00zaUXE9vYK5fRi88m43riwma4ry/8Anznl4x+K8vR1qFGhXQFZQc8+dVtqU3PiU4GraDC4IqDIV0yrpawP2UsisrMznA21ED7avZVkIVs7Y8waRC13jk8VgAe0VRjnjMTB73U4/HsoB0eElrlycuQ5Vaw1L6N7gcDyrMGKXlzU3jt++iGhj8UpscPq/fWg3F9YC4W1ChtcEkOYNj9lWkWvpxFx4alWKmALAE6hzWk0DW0i2AXDHOrb+J3jw058qokYBACPGxxbtPKijtiyzoz38TYdtdoISMbHnauRtFI3MR84U4Xrsgeoxtw41vjsxy3Qm2Cgg5ADhRWIMbscuHbTgFcsScqJKgYi1VEa4FhaoARiRjQVb+K/dRJYYDEmigWOQGNQFRRFxmMeJpS4J0r8TyqCFxkLE1AoGOZOZohVA+80pIOCjHnQFiq5/Kl0l8WwHKoIwMTnzprEcaCWHKlOkZ1LsfLj2mpYjE4nnUULX4WFTSo4UbnlQLjtvQTSOVA6RwqXvmbURYZUC6b9lQIo4fGmoEhRcmwoBpFIcTZfnUL6sMbchmalmt+EcqD3oJGMmfA8Kli5x8oom7G3H7hU0lPJlyrtcokAixpblc8RzpgwOGR5UaBTYih2NkeNA+DEHDlU1qwwuagU3U9+F68lL7Aj3UPVYd71vfblOsPHuNwdO2jZNxCI1i3ELQwqUZViA5dldjf+5uhdN3LbDqO/i225CeoIZCwcrhiihfHmMFuati610x+mt1Zd/AemoCW3msCMAHSQxORDYWON8KDnbf2iNr1mLq0PVd0scO2Xp8fT9G39AbNDqXb39L1LX+rVq7axy+wtrJsdr00dZ36bLprBulbdTt9O2srRi35N5bRuyj1NVgedbN/7lhm6D1Hq3tubb7+fp0bzSwzmRAFjQysjqoEisyr4brY1o6L7n6J1oQQbXeQPv5IVmfaoxJxVWcRlgA4Qmx03txoOUnsPapH1Hbjqm6k2vUtnF06TbSR7Z0SDbqY4AhaHUWjViASTfM41qj9qTRJtJU61vv6lso220PVG/TmZts2k/p50MJilUFNQLLqvjeultfcnQN9vT07Z9Rgm3YLKIkbzMnnWNraXK2xCk2rke7fee06Bs94djudnL1TZIZpdluXcXVUL+lriBCythpVjjTUdrpnSoel7U7aGSSQu7zTTzNrlkllOqSR2wFyeAAAyAri7D2Uu0h6ftN11fedQ2PTJI9xtdlP6KxiaIlo3LRxiUqhN1UvYV1G9y9Dj3q9Ln30cfUmCk7LxGTxIJLhdOK2Pmy+NP0zr3SOter/SN2m89CwmMd7IWvZWJAs2GIzHGiuJ1H2Ntt9t9/sB1LfbXpvUZX3U+xgMOgTyMJWkR5ImkCmQaimrTeti+3tx/Wtr1ybrm7ln20J2phaParHJExR5A4SFWu7Rg3Ui3C1TrPvDo/R5v0ckp3XUNaxjp+10tLqcawrM7JGh0i9mYE8MxVsnuf25Cdsu830e1m3UUc6Qbm6SIkwunrqR+Ve9vHbGmom96ENx1CTqfT+obrpm63MaQ7xtp6RWdI7+nrWeOQK6hiA62Nq37HYQdO2UHT9qpTb7ZBHEGYu1hxZ2xJJxJrkQ+7ekze4t37bWQrutsIRGbSESSS69Ufk0ro0ZlrG9X7j3R0Hbb7+mbjqUA3pkWD9OCb+s5CrDqAK6ySPDe9QdUqFyJJ5HGhrbK2ntFQaj5hoH4acE2wAt2UCCNG8R8R50xuMGFxUOnkVPOpqI4hvsNRSWIxTxDiKI0sLrgeNHwt2GkZSDqyP4h+2gpk1h2sQe+k128wIotKdZBXH7DQu/4R86y0IYHI3qUhRjnYd1L6cgyk+FqgsNIUGY8J5ilsw89z2ioBGe/toIWZc7MOYzoCWM/V8KfSvIUCinMVFLrHC5+FIcTdVIPOns6+U37DQ1j6vCe2gTXIPMot+Kj4ziCPvpi68xVZ5x3v8AZQAxte6tY9gwoDE2YkHkaOt8mAXtzFRk1DxNh2VFBo4yPF870mpFOlseR502kJ5hqXnnRLJa2BB4CgU3IsE+eFAepkxA5UNTrhbw8GPCiU1DxG/dUCuqEWLEnhSqS2S2IzvTBlU6QLt2caDK58WRHAcRRVcqrnJ4ua8xWeTblAWTCI4kcRWwMlvALk/7Y0irY2kNxmo4UGWJhfSgBvgdX3in8Awx9VTx5UJ4j/NiHhHmHMdlRGFgF8QOBBz7DQWFAHVmJdjkDUYW13xLLSxsApBN2Q/GjK4jOo+YghRUVzmaw9EYvIApPKxqyMaSZBgUwJ4WGYFUwgl3dsSLsB28K0AD10VsVABZRlqoiyM2jdxhqxLH7qKAJHrbztYnn3UPE7lLWTEkVaoFgq8vEaimW+LHAn7qNGhUUKFGhQChRoVFZYf5m5/8T/1FqvfzbGGEN1DSYi1gGGrxdgFWQfzNz/4n/qLWDrqq0O2VhcGdQR8DXJZn7uNs8l+lUN77dGIVPijH7xXR2292W5ATayoQMo18Nu5bCgOm7HSPyVy5Vn3HRdrINUAMMy4o6YEGvXl9iX6rn2mb4i3qHToeoKgkYqY7lLZY/iFNtmSMfpSojdB5RkRzFWbSR5dujSfzBdHI4spsay9WJhii3a4PDIovzVjZhXl9rnePLpds4+K3y3UKitqUNzF6ldSBQo1KgrdQTqt2VS6sG9RTcgXHdWmqiNL9hBuKDJJaSI4WkJuDVIAk8Oa2xtmGrQt45XNtSrj8DVAVQzlTYsQaor20jReopxXiK2h1ADg3Qn7hWNcZHFtRcEgjgb0CzRXN/FiCppRaxJQEYGQ5cwOdIpEkhZ8QmCrwvVfq+o1lOkAWYcfhVqp6gES4LxIqC3aK77iMr5dVv+yu9ZLaBhbOuTtbJPH+BTZTXW06j+2tcdk5IARjn2dlKCHOOA5GixJOnhxIpjpAAz5CtMgwAyz4WoAMuePM1NJXG+PbQvqz8IooatRsMBxNN4VFqBItYZc6AjvjlUA06uwUQpGR+dEkr291Le+eHZQQsRha/dQz83yp8AKFych8aCXFLq5Y1NAPmxqWtkbVFSxOZ+FS1qGo8Me6hduItQMbcaU6eAv3UCyDM3NDxNw0rQAk5LnQ0C93JY8qawGF/gKl7ZCggB4CwqWHH7amJ/wqaTy+eNB7xSAMLknjRu3AfOppI8pt2cKmq2DC33V2uUCpbM27qXSV812HOrKlAoC5i1ArjcYGoV4qbGoG4Ngag8l1jdD/AK29vs2y3cqbKLepJuY9rNJDFJulhSI+siFBfS1zfw8bVwNG63x3u423TN6Ytp7jj6021m2ssB3G1EaRs0InVFd1k/M0ZnT3V9LYrkTj2Z0up8iMPxGqPEb+Le9c3HuDrGy2W4hgk6DN0yJdxE8E263DGSRdMMgEmmMHSCwFyxtWaWDedf2XtzpOy6fuul7npKa91udzA0Cba2yl2noxOcJC7yD+XcWFzXvmRsyb066bXAqZHz/bQ7ze9H9s+24ek7rZ73o+42Em9mliMe3242VvWeKfyS+tpKr6ZN9XitjWPqsHUNn7T9ze2JOlb3edQ30u/n2zbfbtNDuV3UjTxTGYeBWjUhSrENdfCDhX02lLAYceQpkw8P7l6kmz2fSve2wjkml6K/obzaMjR7g7feKIGgkik0uret6ThWHbXovbfTZej9H2uy3Xj3bap9/IDi+63DGbcN/vuQOyqv8Apboz9Ul6sIn9eaZNzPD6rmBtzGAqTvBfQZFAFiRhnnjXZsF8ROPM0HyfZSbnc9P33VOpJ1CT23Pv5eq9Ql2kW0/SO23mDieFmcbz0R6CMw0XwNsK2bzp+8Tce44ZZerTL1yaTc7OLp0O3k2u8hngSNI5J5tvKYSttB9RgAMVr18ntD25LI8jbG0cjGSTb+rMu2d2OtmbarIIDdsTdMa7Nr4KuGV/3UyYeLj/AKz0Tqsu32Wynmm3uy6TtdrLpaeAHbM8W6/UTroCmNH1Xa2rhyrjwwb6UmPpe26iksvVzvJOkbralunEpvS8m7j3jKjxghfVH5hN8NNjX03SoFgpHdUJ7W+ONMrg5tc2ypCqnh8qGo8x8amv/a9QSzDI/Ogb/Ut+0Uda1NSnI1FJaNsMqmhh5G+BxpyAc8aXQOBIoMMwbW2pf9S0gldPMpZPxDG1aJPUDtkaqOm/iUqeYrLQrLG4urA01xwqho7nVGRf7+8Uuo5OoQ8xeoNFKwX6rfGk9MkXEhIqaCOAbvzoFOkeQn4Y1NUv4Ljne1PqIzX5UNa8cO+opNRPmOn4UdCnM6qa6kZ3FVsIxlgf4aCekq4p4TULMvmGHMUPzvptb+L/AApcf+Jf4ZUBMkZGd+wY0lpB/LFhyan0xny2B5jOgdSYkhh24GopRibOTflwqaAmKWHMcDQMgfAL/vYUPTYZnWPwmgglDeEC7cRSlGGJN1/COFOSjDHwkfAik1vl9P4/8KgYlAot8LZ0o1Pg3h7BxqBNPiTEnPtqFg+CeYceVFKxWE3HlOYHOgyFxduGKinUDHVi3GkuQfT4cG/ZQHVqFhnx7KyvEIiSMuP7602EZv8ASc++s88y38I1EZjhbmagqklVSJJM2Fltz7aqZmd1MuVsSOVVspRyrm7N4l44VcoZILqLAk6r+YVRVGSBcYAnUeZFXQ2Uyqo1O5wNVbRVkka5uoFaIjaP1zzIt2VKDCCA1/OcB++rYhZB9pquMaFErfX5vjlVq4KAaijQo0KihQrF1Ldvsxt3XFXlCSDsINbFZXUOhurC4I5UEqiXebWF/TlmVHtcqTjY1fXGk6vBsdzuEmWTW73GlcNKqFHKsc7ZMydr4Vu2x1etKAQkkl4yRa6hVXVY8LisfWkdoYCilgkys2kE2FjjhVZ9x7I5pL/u/wCNbdxvoNptRvJdQjOm1hdvFlhXLe85zleNltzIumMZQb/agDxNl/y5P/2aJnlmGnbRsL/8aVSiL22azN3AUmx6nB1DX6GsaLE6xbP41kn9wbSGaWAxyu8TFW0qLXHK5r0/l+5beM46mm+XTjjWGNYkuVUWucycyx7Sca5HWtwJXi6dF4pGYNIBwA4VRJ1fqe9/K6ftjCDgZXxb4cBWvpnSRtCZ529TcNiWONPt/Z5S9uemNcepbnSOii6UVeQAo0aBIUEsbAYkmvcChWLY739XPuVU3jQqY+diMR863VAtI5AIY5VZVU1tBvkMxRWdvBMrHJgdXfwoMgWdFPldT8xRmuiKTjYhr1JziGGKgi55XojCfy52UYHgeV6afAAkh2+qpMCWfTwUXb41WxSRksLBravhVQqoxUYETSHAjlWmN2gFiNQOGoffUjiNy4JDeWLuq3Agk2JXDSedSrGjbOjzRBDqAOPfXWN0FlxJ4VwdtFp3EcoNvFmD99dpXdfE4uTw41rjsnLdYLKvM/toWt4vqPCl9Rb6jg3bw76iuHyOPE1pkb3Pjw5CiccD8v31LXyy4k0PLgmNFTTpxv8AuoaieFu2jccTc8qJuewVAPCuP20Dc8MO2hZR5c6l24i3bQTQBjfGhqI4au6jdOJvQMijIE0E1E/w99TSMzjSlpGyUAdtKYyfM/woGaRFwvjyFLqZuwdlEKi5ECjdPxUUoFsgB2nOpa+ZvR1JwH2VNY4A/KoJYcAamPAWqaj+E1LvyoJY86FuZqeKhZqD6BQpRKpwGJo+M9ldrlArbFTb7qX1QMGHxGVNoHHHvo2FAt2OQt218+92e5+q9P3nWhst6dHSNqk8G02mzbea5gjSyp1CQppgW2m2mRTpOqvoJW2K4V5/qHtHpXUJd624k3UW36nj1LZbedo9vuW0LFrlVRquUUA6WF7Y3oMsvVt/1Xq+06J0feLsFOw/qW93UaR7iVSzpEm2jE2pFILEuWUm1ud6x7HrPubqb9J6Funj6V1TcQ7zc9R3KJFM6x7SdNvGiR6pYVlk9RWcHUFxFuXSh9k9H2sEQ2u43m33cLSuvU4p7btvW0CRZH0lHVhGnhKWGkEY41a3tLpP6babfZifZy7FpX22/wBvKV3IfcHVuGaV9fqeq2L6wbnGg85uPc3ubaw9O6TvI5Ieobvc76Obc7TbrvNwu12ZXRJ+mhMkayyiRL3uqjHTiBSN7k90DaGFYt14N8IH3/6ILvm2Rg9Yzx9PfzSJJZGIUi3i016Q+0ekrtIdspnhm280m7h6jFMw3Y3E1xNM0x1amkvZgwKkcMBVUnsrprGCaDdb2DdwzSbp99FPbczTSxDbl5pSpvZAAFtptha1NBwo/cfX5Oizvtd4d/uV6r+jaaPaqu8h2QjWR2/QMIy064nTp8p1gEYV2/aHVdxv16pHut8d7+k3SxwNLt/0m4SNoY307mDRGVfWWxtZhiKkntDpkkcSJud6m4i3Z6hJvknI3Um4MJ2weSbTkENgoGmwta1dTpfRNr0r9RJHJNudxu3WTd7vcyepLKyL6aa2sosqiwCgAVB5b29LL0nd+4d5vusSTwjqm5jXaSRwIJpRt4pRpZI1fXpUgKDbDKpD1rr0HR+ke695vY9xB1OXZ+v0lYUWOKHfuscYgmH5pkj9VSdZIaxwFeg/6b6YOrN1dTNrab9U22MhO1/UmL9Odx6Nv5hj8OduNr1RtPZ/SdrLt2Rty+02cnrbHpssxfawSC+l4oiPp1HQGJC/SBQZveu5610/pZ6x0zffpoNm8Z6hH6Mc19s0irNOnqA2eJCWH0nG4rndS691vpsvuPqe33o3fS+iiGDbbd4YlWXebhUuryxgN6UPqxsbY3JF8K7fXpesif8AR7XpCdU6Vu9pNDOFkRJBO/gRZPVdFEJQnUwDN2UOh+1dn032tD7a34G+RoWTqDyXPryS4zMSfFiTgc7AUGWZ/cHTOqdO6Tuurfqx1kbiFdyNtDG+23EEX6gPEijS8RCsulwSMPFWCHrvW5faftrrbbtV3e73e02+/VYo9Ey7jdDbvYEXQhctNeg6f7b2XT92m+O43e93MKNDtZd9OZzBG9tSQ3C21aRdjdjxNc2b2D0eWGParu+oQ7Tbzru9ltYd0Vi206v6okgUq1rMTYNqAvgKKzb2T3ZL17rPTem9THpbXZw77Yxfp9uZDNO86rtmeWyel+ViT4v4q9Xtf1J2m3O+VF3ZjQ7lYrmMS6R6gQnHTqvauZtfbUW16zJ1xeo76XczRrDLHNMjQtGmr00KCNcELsRjnXYs34vsqUQqDwpTGnKj4uYoXbsqKHpLwJHcaGgjJz8cabUez50NfZ9tBkkWYObMD3ikvKMxerJJQHa4+6k9ZKy0Qm+ai/xFA3IsDccib05mj5/ZSmSE8RUFRWRDdFK9gNx8qH6oLhMhX+K2FWaoeDW7qBdLW137xegn6iI5NfuqepqysP8ANVZWPNRY81uDQLyr9Osdoxop/TRsWOo/KiI1HlNqq/URZOhU9oo+pAchc8hUFlnHEHvpWcr5h8qQ3ONig78aT1IlPhLM3fegYsH+nT/ERUCC9w+o/wAWNJ+oP4Wv3UrSS5CO/LK9RVxJt4luOYqvWL2iOPJsqr9Pct5hYcgbVNO6UWUAr+E40FhUHGUXPAjKjcqL31L9tUapuAN+QP7KXRITqdSTwANBaLvcxmy8QczTAA4p4WGYP7azNOY/5lrc8iKVt2r+Uhv4lONQab+pgMCMzSySRIulzYjgM6ytNNIB4lRRky51ETU1rHXzPHvoomSXc+A/lpxvm1RAiqQQSBhYYaqt0FvKACvmPPsoWLWmOSZLQUNC8iFbAOviB425VQzaopGJN2sF4d4roMdKiXic/jXM3g9KdUHlPit2moLY10ksvh4sOy2Aq5D+WwbDC6g9vGqpBdGF/KFJbnVs3iVCRa1rCinTGEA8BYCrRgBekI8ajgRc/CrKgFCjQqKz7yJZobEamRhJGuV2TxACuL07qMm3iUzBmSbcSRpHbxL9QtftOVdDq7TIm2kg8yzrf4gjGqJ9mm/aLf7ZvHCxZ9vfwl1wI7G4XrNmum/9h00ljlF0a9sCOIINiCO+laGFzqZATzIrzsc7beHdTyD0N286tEj+fS0h1AcxY411v1ksLbaJvzn3EetS1ls11FrqMvFyqd/S6X8pmjRJt4NDflrkeFc3qg//AAaK+X5VdA7tC8m3dG9VbK0aeLBl1AjLhXIbo+1Mhdtw40vpMZ1EggarEYnKs8pOV43tP06/BXS20aJOQihQYVJt/mrH06ON991DWoJEzZin3+z228VGM7Jo/Luuq5IF7FbXpun7WDYKypI0rO1iLEte2rK18qs4yc+XPtNYeI6AVVwUAd1Sql3Akf0o1JlDFWRjpK2GrHPgaySb55NruZ4joO3RiVte7qxUi54YcqXlx8+PxG53VBdj3DMm5sLCuNveqkCGaEkw+qFkQDE6QHPxqvcbwetspo/zJgJJJ4kPiIDhlB+WFDYdNcCPd7ksq6mddqSMXc2F/nTW+yf8wy1dEgEe1MpXS8zFmU5gE3H2GulXL6RuH3M26lIspZbLwBUaTb5V1atWbBSsNQIPGjQJsCaKzjxRyA/SNNZ/E22YL5ePOr5D6asfxC5+NUNqjiKLiGWiKTpZDY2TR5u3lWfaeOVkbDl2catZgsY04qFIA5nnSQKVAkGeY7eyqjoAXWw8y4DvoFRIRpGK51NYsJBxw+NKx0nSMf8AmkVlpdtgsm4jcDwK1gvM11GA8wGHKuTt2ZtxGY1sgNj/AIV1ibYi1b4bM8i2FrsMOA4/GgUU5juAptTMbxi54m1AI7k2OjnatskOoYarjsP31PUfIN8BVohTJr350SqLmoPdnUFBkfnbtoepIe3ttWgLfEZfho4cVt2igz65TxPytUwPmJ+Jq84ZN8KBDHMYdmdFU+AZWv2GmFuFPoi/DY0DEhyuO6oBhyNTw/hNQoy5SfA0NUw+m9Abp+H7KmpeX2Uvq/iuD2im13yIqCa17flQ1r2/KmueVC/ZRQ1rU1rzo3FC60E1ihqFHw1LCg96QCLEUNJHlPwNNUrtcpNRHmHxFEMDkaBcZLiaUoW82HdQMWA7+QpfEewfbUClfKfnR1W8wt91QcbqfuLofRt3Ds+obh4d1uFLbdBDNJ6pH/DjMaMrSfwDxdlVS+8fb+36e/VNxuWj2cMv6fcyNBMG28lg2ndRmPXDgRi4AxHOud7u3Um09w+0dzDtJN+0e43zHbwafV0/pGVnjEjICVByvXI65ses7vpnujqCdH3bye4TtNvtOmqI2nWPaoAZtyPU0JrxFtRNrXqjtdV9+dK2uyml2Deru4nhBh3UO5gT0ppAgnZvRJEbDVoe2lm8N66Le4ulDfbrpytuDvNmrS7japtdwzCNdQDjTGdQfSQhW+o+W9cf3dtP1Xt/ebzpnRN1L1bq0cMLpHGn6lUikEirPrk0qqgHI5mj7lh63PLs+u+39rKN/udtN0zcwyaY5Iod0NcU8o1EX20y6iATgTag6EfvL25Ns4d7FuJWi3Ehh2YTbbgyTuq63G3iEeuUKAdRUEDjWfrHvDZdO6CnXtoG3u3lmjgiWKOVvE0whlEgRC0bJ4sGA8Q051i610fc7KToG16bDum6T0uCSIS9MSB9+koVIo9LbnyRvHr1lMb54Vwth0r3WvQOqdGl6XMI49w3UY23Esck88r79N4IkmEhV29BTqJt48L1B7ufr3S4NjF1LdSSwbed/S20UkEybiSS5AjTbFPWZjpJsEyxyrT07qWx6vA246fMZER2ilVlaOSORfNHLFIFdGF8mFeN6tsuv9X3Ow6/Ptt7Fttlu92Iths5E2/UI9pPDHGkuDlWcOjXXVfS3wrte1OnT7M9T3su33MJ6hNHIh6hP6+7kSKJYleexZYzhZVByAvjQZdn7sTYJ1Me6dxHH+h6i3Tot1ttvOIn/KhlTWqmfQzGbSLt4uFdbde4uj7SPavPJMJN4hl2+1XbzvuWjW2pztkjaVVW+JZRavCNsOvr1XqnufbdL6k2723VF33TelTxxfp54ZIY9rMdIkJWaykq5PgsLZtXX6lsOobrrye5TtesRbXebGPaNt9hIkG8gkhlke08XqWZJBJcFWwIxzoO8Pdnt5tzstpHunll6iA2w9KGZ1mU+YxuiFTo+vHwfVatHTeu9J6xuNztdg8kk2zOndK8M0Qjf/ls0qINdjfTnbHKvHbrbr0X2vHvYwqdW9sbw9a3Gw/UDcbhY91LIZYZpL+fcQSt/CXyr0XQYeo9J2fTtvPs5J9z1WWfe9X3aMgTbzzD1z6oY6mGIiXT+GivQ6F5VNC8qlm/FQt/HUB0ryoaV5ULj8RoYdpqKbSvKhpHKhbkD86Gk93xoKJFXWcBS+HsqSRXdrn5UnorxJNZaMSg5UhaLiV+yp6UQ+kfGhaEHBRfsFQAyQ9h+FKZI+C/IU9/wpYduFVvKqDxuF7BiaAFlP0/fSF0A8wHzND1g+Qa34mp1CnGxY/IUVQ0mrBNbnsGH20npS2xBHMjE1t8eQUAULPzHwFQYxt1OTOTyamCTR+VVI7RV7Ip87XpNIHkJbsONBWX3BFiqgcccaQPInkj7xer7sM0C9udQkcXv2Coqn9U97NHpPaaBnma+hB351dp1YBBbm1IUC4KxY/g4UFBMxNxg/MC5pS0t7SyEdwrTq4P+WOX+NEC4sosvE8TQZfQ1ebG+QzJ76V9ml7gDV+BcK06SpIh/wBV/wBlFSPKvm43zqDJ+mfOOS7ZaWGVVvrj8LJY8WGXzra4t5f5hyP76GoKNJHjPA8TRWZXRwFW4t9QP2GmZgGupNl86GjJtUALp4JOJGRNVK8kItKCObZj40F2pS1r+C2ofurnTK0t3+pSTfkuQq6VrJYC4kN8OFBQhR21EAgKRxNQBSHXRkukae8VdjOlstC/M1m29zNok5C3761xE6ZB9SsR86VRQl01fV9Pwq0G4vzqmP8AlaMmB091Wp5bcsKgNCjQNRWfdNHoETtpaU6Yzn4wNQP2V5+CTc9OeOAlV3Uu4YSIcQUYYG3K+Vdjq23eeKEobGKZHJ5Dyk/bWeI7bqqhn8O727MsctsdSnzLz5kVm4t8UWetsOokxbiMF4XKXIOkOGK+Fu21Vt0qVdwNzDuC2hT6EUniVTcNYdmFYJ4N3sNrLDMrSybmZX9aPygh9R1DhnWoTqs+z2+0kC+rHaf0yCQwZBiMbNieFZvKzTlPOvw1AiTqUO4n3+524dzoAjhOYClML99Jt9y8cu56hNt5Y0MtylrsAY9GXfW5d1N+rk2SkM0ZQiRxmGUsbhbY4U67xnlk26oGljfQ2Nl8vqXvY8Kn/nfZnj/qv5/i5u23yepuuoFJfR9ZWtpOq3plPL30sG9Lz7rqEcErxhkIjIs5/LKYA9prpruy7vCkYM0b+m6arAXXXfVp5dlRd0XmbbIoEyMFcEkqAUMl7gdlMfb8/Rj/ABPzcyBuou+532323pzSONEUx4FQhOHdUTpG6l24SfcGEuzNuEi8raiWt9tdCLcPPuZNpfQ0TFWdRmNIcW1X51z5d2r7LeNuHUSRq8cJYi5ZXYAgfiwp24TaZ04/L0GpI9htZ19JB+olsiuRYEghDj2XxtXJ3O9l3Z27xEDcrMVVOYQcr5FhVhbc7ubaxbdGWbaq7GZx4NTtrWtO32u26eYfXIbfOdIkAwBc/wCNXW68tJ4+Yv6VGIduYGIM6G89vxN4/wBtbq5PRBM53G4lx1tpB56CVvXWqrNkpH8pvT1VN5PjRWecn0WvmLAnnc1XPf0wOJIUnu4VZufHptgLgHu51VKRqT8IP3VUZd3YFdOAxA7qsS3pLYeNcxzvWfdHU9swp0jvzrRLKUQxm1za1s6IHraPDwOC34HnViAlbSGyjNRmT21nWO3mGuQi6nMgVfCLuBMbnhytUqr9prfcRquCg4MeXZXbESpicR91c6EATR2wscK6Y8YucuVa47Jy3S18sB99SwIwwtUvpwPwoEE4/ZWmQ1X8P28KgUriMfvpjYjHKk1HLh+KiidJxypbv8OfGm0DPjzqarebDtqBQq5jPnR8Q7aBx8o+NCzfViOyghdcjnyqaSezuoixGGVTTbI2oF0kZG/fUueI+VQtbPHuqar5D51FS6nA/bSFFGINqcqTmaGhRlhQV5fTcc1ogg+Vrdhp/EO2kYA5ix5igPioXPEULkdo5iiGJyxoJccqnh5WqXXiLVLDgaD3fqE5KR2nKiAGza9PQKg5iu1ypUpdNsiRSlnyWzdtA5IGeVJct5cBzNTtcEn7KOoHI1BzN57c6H1DeRdR32wi3G9h0+juXBLpoOpdBvhjXRJYYk3HbTFrYDE8qGm+LYnlQJq1Z3UdvGjcDwriaJN/CM+J5VWVA8KYMczQHQCc8eLUblc8RzFDQVFj4h30Ro5WPbRSlkbFWseyl16Tkf2GrCqnsPMUPEMxqFQAOWyFTx8gKXTxjOPKiJAcG8LcjQcvce2+jbrqP9W3G0R96fTLyXcK7Q/ymliDenI0f0llJHCuoVJzYmiWXv7qGrkDQTQKmleVAseQHeaXUx/woHoUtnPGh6d8yTUUxZRmbUhlThj3UwjQcKhKjsoMckjl20oe80n5xzstWyMxc6V+Jqth+NvgKy0Qi3mYX5Z0DqAz0jmcKjyBPCgAJyHGkCEnU7Fj+EC4qAaTJgl2/ibKnTbxqdRGpudN+dwCgdv+FD8z6rnuoGJAzwpCUOQv2ip4BmpvzIvR1pzAqKTTJ9J09+NC0v1HUOzCnMgPl8XdQs5zOkdlAmqNfNh2tU9TV5Bft4U2hRjmeZxpG0XsBdv4aA6WPmPwFIVjU+EeL+HOjokObWH4f8aGr0xiuHZUUumXibjkM/nU9RR4VFm7f30devPwDkcDROhVta4OQ50ClRa8hv8AcKTSx8hsnI8ab0iTqJtyXhULODa1+ZHCoBrA8FtJ+ygwUC5z4HjejdLG57750oRr6x8FNFAakxkxv9Q4VLCTxnL6f31C+o6D4fxVGGgXTDkvCgXUdYV8l+rmaWfTpxy435U66baWwPEGsTsZJDbGFP2fsqCloydUqjPAR8bdlRH0Aq/iubaTwrUF1FAuZxL0kkAaPUg8d/EDxoM1tExPldb3HZV6F/UFrMbaj21mDn9QUbMjQQe2rdRjYSqbEH02U0F6MGnZhyyPGrUNwbczWdyYnUkdtx251epGLLiCcbVFPQo3viKFRVUzRWEUxsJroAeNxlXnZ9rJsJtrs11lGnMnr3sCGGnSxGRr0MxQFWktoActfKwXG9Zf1O2k07c/nw7hNcJHiumOrvtXneU7dbpiZlMM+36m9pzOAYYJfSL38WLlFJ4Grf03Tdw7TquiWRcXxRrH6sbfOsk3SZE2xh6a6tBI6tKr4nwtq8LfvquWcPvdtDNE8abWMrMZB4CAyYg8RhVzyntmvyk/uNMfSp9v6ssG6Zp30lJJQGtYEWPPA0kW06nB624LRS7p31gWIQjT6fwowTpP1CaGCW+2BRl9JrC+hibFe0VbFNNJuptqJGVY5bK+BbT6eu3iB41ntwu/H6c3/r4PzZ4IOqRjcbpo4v1UkiusdzoICGOhDtuq3n3T+lFupGUoB4kwUp9xrVHPNLuJtrrK+lKE9Sy6ipjL2ytn2UI5pZN5Js2dtMTr4xYMQYy9jpA4inbh4+jP+PhfzZo+l7to5m3G6K7iZgzSw+HC2kj5Uw6d03bwos/53pXk1uNRsTixtwvTJuY13k8e6dRtoHOkyG4xRWxLZ41zf1ttjuIlR5v1RkjgMYuoGtiPhY0nK/Tx9JfhTR1Jd5pl28KgKm5YqjKcbI6qewXGVcQNP1BotqdesSu/rcAqjQMeeFbv6duZ2iTeMqbaJGWILfWS/iNX/l7Pbxw7UD0/VG2cHFwz8SeGPClxP33X0nzGjYJBBD+khbV6NgxPEkar/GtVYdifV3G+3HB5yi/5YxoH3VtrUuZLfWZWBVM7G2lc+NXMbC9ZmZUjLNi3GgrmJ1JEuZxJrPMESb0ib6Vv8at8RUSMbHPurKx1AuP5jmxY8BxqjPOxAi1YBvzCONa4I3eTVbxEYX+kVicevPZMUvcE8hXZQKoQJxGJpUir0hGAUwINnahKgAsRY5ratDWBKWvqGVVaSi6ibsuYPLlWWj7CQtuI0mwkU4ciOddomx8PxrgwpfcxG2kXwHOu0smkaWFxwP763x2Z5LdI4586UMTgM+fCoBwY35cqhN/LmMq0ymnG5xNTUP8ACoLtmbcwKBsuIwooeIZYCiADjn31NV8hegVbMG3ZUEIK5ZcqGsHLE8qIAPf21CB3dtApBOWHbQsR5saOq2fzFHVfIXoILcKBAOdAgnECxoeIZnDsqKNiMj86GsDzYUbA9tSw5UA1A5Y0LnlRKjhhQ8Q7aAEE52pTGOBINPcd1Ggqu64HHvqXXiCpqwgGkIK5YjlQfQKUsMhieVLqZhiCo7KIZBgMK7XKmkt5j8BTZUNS86BdRhe55DOgJqs+PIYc6HqIT4mH+Wm9SP8AEPnUACBfKSKBLnBSCOJo4vl5efOicPCv/ZQIW0i1iOZqJbO+JoqL48Bl++iQONAaBAOdKdIyNu6heTgL9+FFHTbym3ZQuwzF+0VLvxFu7Gh4Tmb99QAtG3HH7aVrn6dQ55GrNCngKGkjI/A0CBm42tz401r43vQP8S/EUpBGKtegfSBwo1WJQcLY/ZUJJ4/AUDEgZml1E+UX7agB4L8TQJ4E3PIVFEg/U1h2UuqNcBiezGpoJzwH21Lqo8I7zQZJWlLtgEXmc6pCu5shsOL8fhVro0kjEmy8L8aOlxgGFu6stFSGOPIXJzY4k09KxZc2FJqlPlUW5nCoLKQuowvc8hSlZT5rHsBsKI1LkgoBd2yGkdtD01Pm8R7abU34aQy42CEnsqKPppwFu6kNgbKSTyGNS7N5wQOQohlUWAt8KBdEp8z4ch++oNS/SPhRaZBzvytS3DYubD8P76AGVjgqnv4VAUBuTc8zT6k4EVWXD4KRbi37qiozqTpWzGgIlGP1cxTBI7WFqUqCbIbHiRwoAdd7K1+d6AJUYr8RTBCPK3zxpD6jHSLFRmcvhUC3SU3PlGV8L0W1ILqb8gaJvbxJ8qrGknXiv4aKIIUWcZ5nhS2u10PhXhwvRkl0KTrB5XwrI8xYaYwUX6pOfdQNuZ9f5K4E4E/uoInpBUbxA5KM7DnVSt4glvD2Z2q9GC6mjsoyAc8qBgDd5VytYL3UxsyqVwItVRNk85VjmBkajyEFQylWvgwyNQZN7HqnMiizgC4qKyyBQ2II8fMMONPIzNOxsAykH5VTPG8bHT9R1W5Gg0xMxazeNbWXnamh0h3jBsQbqeyq1cOiSx2EiHLn2U5KSguDpcYrUVahYYNxvY09UqxMQbzWxuOdWhgwBBqKqmVWsr+Qq4bhgVxrjvA+33ezXYqPR20DsrOdSsrMNQBHGuvuE9QenlrWRb962rlGH9BH0/Zq2pl9ZcBbwlbnDle1eHLH8mL6zb4XVfRc+7lfq/6PbxlfSDjcSsBpIsrKcMyK0fq9uzpEZY5fVLINJBOoC5VgCaqiF+p75Sba2Ed+RaJbfdVPT+mbPafp9cIXdKpX1ODMPNY8edPt/TO2P0y4853PzXSbDp4WSH0vTEgDOUBFrXsbrlVK9K2sMTQwzvG0rB1kD+PK3hPHCtsyj1QwJ1GORSBfEFTa478q5buq9FDu7JaKANIouy2fH4it3nrylmcY/wBkXRdKRIpoU3btK7BjKGGtSBp4dlVp0vaJHNDJuXkeQhnct4xpwzFX7eKU9RmkYhYY0RNsinBkYatZ53NJu91Hs5NpPLumgjVpS0CqW9XxHDDlUvPSWcZby/rC4ROm9P223ki9MyxC0spbxWA+on4VbJLttnEp1LBBrWNCq3JZgCLcBnWLZfqpV2UjkLFPLNJMjMFPpMCiJpOedGKXb/poId4nqBN0sGPB1XSrHnlWeX3LczxfTf2kXxbt13272bIXeFDIkxAtpCBghtxN+FVdKRzBLut4FZpyN6FXIFOXdhVmzhkjbePO+vcO0wkIFh4Vsth/ltR2f/5bF/8AdX/7orN4zrzu9xx1VOkIV6fEzeaS8jd7nVWyqNh/8Dtv/DX7qseQLgMTXvdybFkIN7nwgfbWZyZX9JR4AdTgfcask/LjLucTkO+qFbRGDYgHFjzNAJT6jJF9Ixa32Csu4c+oQmYGkAcCauMnpxlrEu7XqnbJrcysNXbwueNEFYvRMcYxONz3itsbflqFzU2J5Vj3BRGXQTa+fberYzg6MSq31BeNKsaS6qw04tkTUuA+eotwFVu4CW1AAY2tjVbzKR4ALjG6msq1wC86K5tZrqK6WBGAsOJrjbdwdxE2Nw3K9dgut7kXBzrfDZnluBumKnVb6atSQOPCLHkaq1qML25YY0hsTdbjnf8AZW2Whgc748qgKcKqEjLmARzvTCQZiwvmKgck5gVPEeypieIoEHMHGoqFb43x7KAAvYjGja/E0CoOBoDYUpwyPwqWAzxHOjYcqAB1PHHlU1CoVB4UMVzxHOooG3C96GphmL9opyQBcnDnVLbhBl4u6gs1E5Ch4uQrO07HEALyNL683HLmBTA1WJ5UuluDWrP+ok5j5U6TuxtpuezCgt8XH7KlgeJpP1Ed7E6TyNEnVkPjQe+LhPEcBxH7qqfcfhGBFwTVFzO174/dViQAfzMuVdrlVMZJbkDVY2J4UybZwDgRfPG1bAAososOQqMQBc0GQ7ZhfxeEZY/bVf6adhiRYZKeNbMzdj3CiTyxNBgPrRHAEGiN1ICEK6vxEZ1sY6R2mqv0yEahgx41AyyBxcNYcrU3g5376zOskZF8LYAiro5A/hcDV99BYLcKNLpXlU0gZEiijQIBzpCTwa9T808hUBKgcbUuo/T4qljxW/xo6gOBFAt3OYsOzGhpjPfzNPqXnSllOA8VACpI/EO2k1lMAL9n+NP6d8zbsFSxUZi3bQKDrzNuyjdVwGfIUhBf6dI5jOl1FL6DccS2dFO38XwUVAt8XwHAcKQMScQV7+NPYZ2J76gzyP42Cgn7qSznzGw5CnkZtZsv20t3/D9tZaAIoyGPOjQ/M5AUhd72WxNQOaQuL2XxHkKUpK3nYW5CjoYYBrdwoBpZvOcOQpgABgLUNLfjNI1r2BLNyqKcm2JqvUzYLgPxH9lD0QcXJPZfCm0L2/OggCrxx4mgzIBckWoMEXhcnIUulAdT2vwHKgHhfzWC8udS8Y5fKjqTgL9wpSzE2Re81FKWjJsvxNsqlowLBTTElRmFFZ5dzbwxks5ysKB20k6VVr8TypS0aC1j3XpFglbPwXxJJuTRaCOIama54asr0FTza20C5HEKf20HaVRcBkHAXpxKUXTEoP8AFlfupfNdnuTxbgKgyFd1MdTWCjALmxNWpt2J0s2k/UTn3VrXSqhltYYKKYrgEzLZ0yrMNuQrPrvfKlbaOqgI4BPC160sg1BVwAxNqBLhwCNQAvhUGV03KWU2YX4ZfKlZ5PCGQlb443FbC4LgHDC9jST2VdYwIviKDlB0ZmYHDUTYmryVkQB18b+EG/yo+kPT/MUEAhmYZ91H9LrDNG3cjcqCmMaCVKkMvmHdxq1XRHZHXwvirdtUT+pEVkYEdtW60kjfHwsBcH6Tz7qKtDegRYXU8KdTHjpaxGRFUh5FVVYhgp8wq1tQKyxi18wMjUCbiM7mMxrM0MgOEkZscf31m2/SEhlaeaeTcSlSitIfKDnprZ6iM1mBUkWINMri+knHgedTE3wrJt+nekk6zTvO87BmkbBgV8tiOVqrj6buBuk3G53kk4jJMcZAUAnC5tma6VCpieJptoMm8224nsdvun2xybSAdVss+V6xt0iZofRbeO2ty8xIHj8WuxHfXWoVMTfE+SsUWymi3QmG4ZoVUokBAsqngDnS/wBPJ3o3UkxkjTV6cDAFVL4m1bqFNPEHLPR0k3X6meZpCg07cHD0rG4025VWOiyF7y7ySRTJ6zoQAGe1tWHGuvQNNPE+Rhzf6buCs2rfSmSYi8uAYC2nTh2VZJsNWzTaRzPFoXR6iYEraxHxraSBmaraQE6VxPEjhUxPE+QrRRBCkCXYIAgPdSMwjTI3Y/E00kughVUlrYL++kRGUmWVhdRnwHdVUjLJK5MgsFxC3yFUSyGQoBYRIcTzq1m1m7MdLDLiay61SMvYWVvy1PHtNEV7liPDfxsfkKuRhGiopIUDE8SazIkm4mLsbLe9+2uikSL5F1N+I0pGOVJHUgA2wszcDTIpJDsxJODWwFapF1K5bHEd1VWUB4xjY3W3I0yq9dvHYFhq78qKqirbSBbCk27tJGBkVwPOnCAMb48cayp9qR66KBezV1gCbqcB2Vy4CF3MRvYE2rqM3EC/Ot8NmeW4BFsVIF+dDQrYEC4pm1ZjC1ArfG9zwrTKtoo8xgeIqsxE+XHmDWgAEXAsahxxyIoMlpE7B3Xp1mPFQe0VfmccDVbRoc/C3MUA9RM7EUQ8TZGkOpM/EOYzoXV+34WNFW2Q/wDbQ02yxHKk0LyqaF5kfGoLLA4iqpJUQ2uSapme11ic9pBpYtsx8TlrcAeNAPTllNwf3Vau3QedjfllVoFhYEd1K2lRdwPnQTTEuRAo6uVzVR3CL5UJ7cqT9UfwD50FxVj2UDEWsWbEZVX+p5phViTRvwscrGoqptt4fAw1Zm/GkUyxkgtYD6Tl8K1gocRaodJpke6WFVGHmHGo0iLhKdJ4UJZTCLeY8OYrI8ms4+I8q7XKd906HSBYHLnSLKwbW7ajwBOFKxObeK+ZzoXUDHEcDQMzDvJoAlcQceyrodvG6lm83ZwrPJHobEf5O3toLFme41eLi1/urUkqSZYHkayRQq1y3xPM0jIytp06SONQdEgEWOIrHMgjOpTcdmYqyIiQWcnUPtq301tbgc6CqKVpBY4MOPOrdF/Mb1leL0ZAQTbMY/ZWhbOoYE2NFPlQoEWzal8R8p+JqBjhS6r+UX7eFDQ2Za/wo2b8X2UA038xv2cKmleQqEsONzypdLt5iLcqCGxwW9+fChot4mbHtoksuGHYKBD5tbsFADrIvcBftNKAzYlfCMhR8bnECwp/FyqKQgcVI7qW1vK1uw1ZqYcKUv2X7qDJJI6u11DDmppfXU4AG/I4U0r3dvyiarNzlER8ay0fSWxY4chlRsALDCqTG3Ale7GhpkH/ABGPYRaoLzSNIi4swqq7DAoCeeNRWVcWQ352+6gOsvkCF7sTRFwLKlhU9ZONx3ilO5gH1iopvGeIFIdROkMSePIUpm9TBMF/EcL0bgC2sAchQQRouJNzxJNTUg8ov3CqzNtl+rUfmaR90tvwrztjQWsztgAFHEmqmkx0pdz2YCqvUkl/lxsI/wATHE91OvrAWjUL250UGjkOMgsOCrmfjUV4YAbizHO2Jqeizm8jFzyBsopliQeVQTz4VBW27Lfyxp/ibP5VWsYkbXK+tj5V4AdtarC9kxbi3KppHlAx4tQIsajKxPFv3UbajYeRftNRkXyKLczQZBhGhI545CoqaVc6yMB5f30qpclwSL5d1FlIARWz+6o+tVsCMcBQKuvFsDf7qisbsSp5YY5U12VchgOFKrEKLqagGpGc3tgMjWfdKPAqmxY5VeGUlrjjyrI5WSa6mwFwP30DEMEYEXVbEkcTVvgZscCR3GhpNmsw0jIGozsdLaLrlfvopJEYxunnAxHOsKx2DNE1iBfSeI41taQBgVBW+BJyrFdQ5B8bK2BHKgiTqsgLDC1jw+dX3EdxE10fEIc/hSSosgZ9QAOGnjhVSXQ6LaGzs2V6g0lnmjDqLFfq44UTKLaZh3NVNn1kq+hmHkOR+NMJlKWeMh1455UVcro+Ci7DlhRBkB0m3ZVelZhdAFccQcahWci2sEjgRiPjUFoYHA4HkahZRmaoEqnCRTcZ1PWRcE8X8NsRUVbrXnU1Lzqn1Ij4mJ1d2XdQMwI8WC/itiaC0uBhmeQpG9Q4A2J4dlVmWNRdU+3GgNTAuX034ZnuoGdhEMQCTlaqzMbaUBBOJYimASLxMwZzlfE1W8slnIQE5XNQLpaNGkLAE4EnE0moNb1b6R4rcPjSFA5XVdjmQMFFKzl2CRkMxzJwUCqEnmec2jFkBxth8KRlCFi4uxA0/hFXaYwrJ5mGJZcqomlZxhjYAWoLNumKFzixLVtDEkaRgTfHsrHCpWWwIQolvFjiav8A87kEDhljUqwWHgcseN7cKrbSknhF1IsQKa4ETXW/8VM+osLAC9RVKM0chbJXz/fWhlOoEm98MKp0lgQSAMiKkbXXQ7XZcjzFBrgVfWSwxvXV1AjPOuVt/T9eM3411AVFxWuGzPLdA3DiKFyDa2ByolgDf4GoSCPurTJTqB1YW41CCcQaIYEZd9C+k2tgcqKGkNUsMmHxoknMDGhfUPLUExXLEUjRq+IwPMU12GYw4GoQ2YsDQVHUmDjD8QquaUqulCDfPuq9nIBLWFvtrDokkfwkAtwqh4Y0c63GkDIczWrSw8rfOlRgBoItbgaqmfT4IiVP1VAZdwUOkC54kcKzalYkvmeedQE3AbLmK0mKN1BTMD50FAK6SvPEEcDUDuo0kBhxBGPdRTbs19PhGRNXLtwvHV30GT/TQ8PaK1PECPCLH7KzFsccxQFXZTdW7LVpjfXgbBuINZbKeVFSUYMOFB72R9bal8xzHKk9Nb4GxObVFQLYDAcaL+BiGGkcCa7HMUtp83+8ONDTfxD5cKIxx+VFYmc2jNj9lA231GQjEKB4r1oZFmax8i5WoEKiCPI2z/xpGl/TgDzXz7KCyR1hUBRicqojjaUlicOfM1fqimS97j7RQkmSJQAMeAFQZyrRtY4EcRWqNw6g8eNYjKL4gm+fOnhkf1NKrpVufOg0TAMhxsRlVG3kYkoMAcQTWgAjG1zzJrK2uOa4AABv86K1BRmcT20aHj5ilJY4A3NQMSBnS3ZssBzoemc2Yk0SoGJJoIFA/fQLY2XPiaGnVzC/fU0ouAHwoJdVxJuaRmBOkHxceyi1l4AsaKIFHac6Bb2wvh2CphzJqyhUUmHBTUueC09KXUZmgyyF9ZsB86X8zsFSSYa2CqW7hSapmyUKOZNZaNaT8Q+VKbjN6HpufO/wGFQRKOZ76gUtyLfdSm5zYjuxq0hFxNh31WZlyjBY9gwoF9NTmpb/ADGgY0UYqi/bTWmfMhB2YmgY40xc3PM0UhYHBfmBalKA+cX5A/4U+snyCw51WxbJczlbM1AkhWMWNlJyRR4jQTbu3ibwjMKcfnVscaRm/mkPGrNLN5zhyFBUVxsGLn7BUZSLa37lqwn6UHeeFDwr4mOPM1FV6HYeJtK8v30LO2CtZedvuprh8SbLy51GkXyqcfuoFs48KsPllQOtQAGxOWFMXVRx/fSqTfUVNz9lQSzIpJYHiTbOgquBckXOJwqEs7YLgvbxoOZCNIABbDOiguskvcchQOsvwsv309nAzAA7KRVa1y1r40EfXa2GJtR8fZSG2sXc4Y0sjKqk2JwvjUFc0rxxcNTmyjvqhTpIUqL5Z52qoSB31AX4LbGw7KvSCZypa0YONhiaANIl21gdgF7CkMysnOwwAvatUe3jUtcXN8zTqF02sOIorGzyOvhBNsRf91UOkrTeUeMA8sq6SgFQCByrJKqiUC2QNv2VBQsUpULpFnNjjjUk207eBhcjyEm/wrQET1BwVhzyNW6dQsWIK0yOWJJ4vy5EwB8pH7asEtyQCQM7NhWqaMuNQbxpmDxFZ1tJpMljGDYEjEXoIku3cAPcMPqvY0TqOK3a3BuI76SSAaS1wAD4Tzqti0VtajScQFNFXmdTj6RUDBja4qCVBZoASD5r5VSNypx1NfigxWgZVOAbwt2WFQaDIt/GjFuY4UrSsTpdSUzNs6zjdTKCAAQvEZUBNxvZmz040wLvWBP5MZZRmDzpSzSH1G8HJB+2q2njAxLWHwuaT1WmPgAXm+NQW+vCl9KAvx51naX1AQB5j5Re5q1YlZjqkBIyYeWgWRY1VW0kXJYfsoqmQzM2lRp+lVGFXJtGjHjcazix5U22WNiZ37kXH5/Gr7xi98eJ7+VKMzxIni1cMf2CsrQguqajji3fW+QhmCDh4mwrPI4JLDNmBXuqBI4nMjaWPiFXGPcANpsRlnRjYDcAWwsSKu1DRxxNKrI3qrGdS2HHiKmoFlscxbTf7q1M35RwNJKqMELJxHCoKVAJYAXuL54giow1WYLZjiDwNR4irApfA5HkaUlkuGUjiLZGitmxkDyR+GxBsRXXJAINuyvP7Ryu6QoTYkHHlyr0AYutxY1rizyQlThQDC1icRRDXGVAkAg88DWmUJAN799Q2OF6PhPKlAGRHdUVAeBzqHmKhUHv4UAFPDvoDmKXy91EqBiPjUtyJoKNyw0BfxfspdspUFziMgar3IPqYE2AyrTEpWNRfvp6AS6dBY5jLvrAL6rOc8zWrc6hYKbXxNZj4hY+bjVg0NCrKDHmOB40ILklWGA54EGlgLeRh4fx8qvYsqk21cqgjn0xfVhyNVpulZtLKVJyPCs7MzG5NzyNC4OBwNMDfcGsssRaU6V+IrRpwHA2xqeIVFYmhdcWGFB10nC+k+U3vW/VzFZZkCnUuTfYauR7k2zGVKAWP3CtEW1XVfUbDJTlTnaY+ay12ORlWLWTbw2zIrSimJfEMOJFWrEuFjgMh+2hIBkDiMTRVLsCpJx4/urIdV73vzBreiKwOrFjVb7N2Nw+B4cfnQZYZfSewybApx+Fa2BmQr5fvoR7JAQWOIyq4wkY6vjQc8KARcYjPmaYsSyuRYcLdlXPApc+K3Oi8Ia3isFFr2pgW1k3RCsDzGXdW30yRgbdtUzbdTbxY86mAE1OilsAQMKewGApkjARRqvYUSnBb3pgyrZgP3UticW+VWiA53x5mgYT+P7KYXKsm2WdKWC5m7Grf06jEsSaB24vnic8OFTBlQpBOrOn1Hl86t9A/isOwVP068ST30wZUFjzHcMaHjPP41p9ADI/ZU9EfipgyzenzNQIoyFaPSHAmlMJ/FamKZYpPOarLqOOPZWl9oGYksW+6l/Rfx2HICs4rWYytJbIfOl/NfjpFbhs0XJvjalO3X8RPwqYpmMYhS928R7ajOkYxNuQFaTs3f8A4mkd2NFdhGmOrH8RGNOtXMYtUr+QaF5tn8qXQinG8j9tbm2l/rIHdjSNsriyuVB7MTTrTMYZZLEIPHIclGQqLFLiWYAnO2db4unJGDZvEczamO0GSvj3VOtMxgK6B5rd1LoZ/MSF5ca3jYAG5ck8yKh2gH1491OtMxhKxoPuFARgnUw7hyraNioNzIS3dUOz4Bjfup1q5jEQMlAv91EAKMPia1jYEf8AEtzwpW6drwMptxwqdadoxeY6j5RkP20HkAwB8Ryrf/Tox9XzFKNgoJbXnkLcKdadoxA2FlU/HCkXWxLYDgONdBtjcWVzjxIof04WsZSByAqdavaOfJlZnzwoHRbBSRzOVbv6cmrUHywBIqt+n68BKcOJGAp1p2jmtJiSMCcFVazzRPMdDEgG2rHE9ldlemRxLZZCZDmxFBelqMRITbEm3GnWnaOfHt441OkW+kWqz011gY4Dma6P9NwUazzOFT+mnUT6n2VOvI7RzlRbtnnzoKi4i2RroDphu35x/wB0VB0oXa8rY9lOtXtHOCKCw7edZdwEE8faD9mNdr+lxhrmQ4jlVM/SUZ1IkIKj8POnWnaOZ+WVKjMnUO6mZluGRCx4jsrpDpgWzCW1vDlS/wBLC6gZzY4k2A+FOtO0cx9T+JVAC8Tx7KyzWQ67gK30jge2u0Ok38McpA+k6b/tqHoKWN5SScyRfHnTrTtHEVZGLaVvqF7nD5VYu2BW5cAcVArrJ0cEANOxtgMALVYvSUFrS2buzqdeXg7Rwzs4G/lqQ/bkarYPF4GVVvhYi47wa9C3SwwsZLdpH+NUv0dXuHlLgZErl9tOvLwvaOIXhFtQN081siKBZi1ksA/Zj3V0z0IKbCctyOkfbjVkfQ1QWExZsw2nL7adL4O0cwbZBjMgPYKLQRHO8Q4KK7K9JUeL1yT2rUPShm8h7Bap05Hbi8+8TgNoIZeAyJrITqZYnuqg4/DlXpn6Mhv+cbscSBkPnVSdBSx/ONjfNRlyzp15eDtHKQnSCHtwj5GrAzjG4YJmOZrpH2+B4o5yvALpw++gejgWRpTccSox+2p05eF7Ryyzqplt5vDe/OqJiUeMW8hx7q7p6LGWAEzBRiRa+PzqtuhhiWO4J4eUfvp05eDtPLkhtMt7GwFh3Gry6gKL2roN0QC15jmPp4fOr26QhIPqkYH6anTl4XtPLjsymLOg5BivfKxrsHoyFLesf90fvpT0OMxkeqcRnpFOnLwd55cl+B50M7XF/pNdZuhoUH5x+X+NA9CThO2I5cR8anTl4O/FyIdvbdIYzpIa4HCukGKtlY8q0xdFAljcTtdTfyitzdKVsDKbHsrfHjyxrGeXKOcjXuAe2xpze1iPlWk9J0kH1Th/DVq9PGXqkHtFa6VO0c9SpGOYzqMvEZiuiemLe5kOPZU/pa8JSPhU68jtHOxOINA6h4hjzro/0yx/mkg9lH+mrxkPyp05eDtHN1Hl8qBIGOXOukOmKMpTY9lH+mD/AJh+VOnLwvaODKQ0jY4f4VqAso08hWuToyF9XqkhsxYZ1cOlJYWkI+FOt8J2ji7g3k7hVSR+owXLtrsz9FRjq9chrYYCqR0ex/nG4/h/xp1p2jKWjiARrBeH+NEG2RuDlWpukajdpiSewfvpk6UEFhKdPEaf8adOS9o58sAk8SGzcaSKKx/MF7HCuyOlqf8AjG/O1R+khlIEpB52/wAanXkdo5EkwXCM3PbkKpE8l7k/DhXSPRAP+Me/T/jR/oy5GY/7v+NXpfB2jKpDqGGF6r3A/LN8cseNdaPoyooAmPypJulK1l9Ui2eH+NTpy8HaPWirVYuLcszVSgkgVazhBYDGuxzMPWuq/wBH2bbxtpNuYY1Z5mgCH00QaizCR0+y9DZ7h93AJn28u1Zj/Kn0ayM9X5byCx76o9yPr9tdYvw2c/8A7s10I47xRsPwLh8BQc3pnWD1Dcbrbrstxtjs39KWSYRhfUsraB6cjm+lwcq0b3rcGzmG3Xbz7uf0/Xli2yB2jiB0+o+plzINgLk2wFZOiqx33XsP/wDIf/00FPsBo9ydYWQ+JoNk8d/+WBMpI7A16Dbu+p7Xa7KPfyBmglaFUstm/PdY0JV9JGLi9NFv4J591t4CXO0YRyvbweoV1FFbiVBGrleuV7u/O6CW28oVpNxs/SmWzgFtzFpcDJudT26RHsDsHXRu9i7Rb5b3ZpifUM9zmJtWsHt7KCL1xp9zPBt+m7udYJ220m5QQiMOhAY+KVWsL/hqzq3Vj0dBNLtNxuYbqGl24jIUs4jVWEkiG7MwyrB0OLqR3nU5ItxEuyHU9zrgaFmkOK3tL6gA/wBytnudf/wOcj/m7Xu/+JhoOxtpX3ECzSQybZmveCbTrWxt4vTZ1+RoyAXFWObMe+qjZzjnfLhUGTc9X2mz6hsOlyh/X34f0WUXUemL+M3w1ZLzNN1bqcfR9p+skgl3Ca0iKQBSwMjCNT+YyC2ogZ15Pr+63LdR6p1CDZTbhOknZrHuYzH6cZ2zfrNyG1ur4pJY6VNeg90sr9FLIbq242bKRyO5hINUbdh1OLfmeJYpdvudswXcbWdQsiahqQ+FmUhhkQbVg2XuXbbuSNJNnutok0z7WKedY/TaeMsrRaopHsboQLixq3bkH3V1MqRpXZ7RZf8AOZJ2F/8ATXDU36HGo/mN14iHtYdQZjb/AEhqg9B1Hq42G52+0TaT7zcblJJI49uI7hYtAYt6rxj/AIgqDrOz/o8vXAHbbRRySyJptKPSuJIyjEWdWUi1865fX5OqJ1zp0vSY4Zd0mz3zpDPq0uAdudA0fUTlVEiRD+3+9lhmM/6nZ7ncySldF5Z9csngudOl2ItfC1B3tl1GbeSFJOnbnZqF1CTcelpOXhHpSyG/wqnddf2m13T7ZopnWBo03W5jQNFC01vTEpvqx1AmwNgcav6dF1KGM/1HcwzgqvpejC0OnDHVqkk1fZXJ6jN/TJeodd6ZOJEheMdZ6e48LFVRQ8bZpKI2XmrC3fQdjqG/j6fHG0qvLJNIIYNvCA0kkhBbSuoqMlJJJsBR2O9g6htxuIVdbM0ckUq6ZEdDpdHXmCKz9X2u33f6SB90203vrF+nTIAWEyIxI0t4WBj1alOYo9G30+928w3aIu62k8m03Biv6bvFb8xNWIDBhgcjhRXQxPZQsO+sO2O6/qfUPV3cc23tD+n2iW9SDwnWZLY/mHEXrdjxNqCtiATelJPd2mifMdI+JqW4nE1lS2J/xohQKhIGJpbs2WA50BLAd9LicThRsF7TU7TnwFApsMTlUBA8TZ8KF9bXzAypwOJxNAt2bhYVLHLAU1LcnAZc6KBucAbmoFA7+dNgKUtwGJqAnkM6GVSzd1AgDPE0EZhkMTQx4C3fRAt+2gW5YmgBFzYm/OjcDAfZUCnie+gTbBczQDEm+QoMQouMe00TYDHG3DhSW1kFsuAoAqF/NlxpnKoNIGJyFFpAMBiaQBr6rY8L1BApxLfGmtgB8TUsbgXyxNQAEk0VLjV3UATc2FEWxNAMOGNBBe5oWxNzRGok8KU2DY4mghIFiBeq5CdXKwqxidJOVZ73Y2zNAezjwphFcBmzHCiihbHNjmacXuRlxqCG1sMOVTVcYCoAAbfKgTY4Y3zoqplKvjk1MCMAM+BqSrdbk5Y0qm9hkOdEMcT4sxkBVZYiynPhyolsO3M00acX8xoqLHYd+fbSN4Dhhyq0krhnUKgjHG/GgrwNy3eDyokmwY48qTFGxyGRo3zIwN8BUCPYkAeYmm0kWQY86XAtc5jKmswa2fOgbVxItbKpYNniKhIy5cKjABbcTQVabAtmDVZHhtxNaGBAC51VIMbgWxoJ5gLH50ytcYjIWquM3B7DTKSGYDI0FhsRUAwqMQRjUAwwNRQUHTnQBNssqKki+HGgCNRFA8RGsDtrVnhWWLCRe+tZ51YlDPvrJvNyu1MSiJ5mnYqkcem91UufOyjIVrItjXN6mJ/1XTv05T1PVkt6l9P8pr3041qJWnbbldzGXjDDSxR0cWZWXNSKvUqwwOPGue22k2vT96ZHDyzLNLKyjSupl+kY4AACsXSo0/VQejtv0fp7YGYErebWFCsAhIwIOOdMJl3jUB+dczqMEUu76a0sayETMLkXNhG7W+YvVvU9QgXdxXMm0cTaR9SDCRfihNMLluIBoZYXrF00/qPX6gCSu6f8jl6MfgQj/Ni3xrlQ76Jus/qtZvPM+z02bT6agCNr204yKfnTBl3ZcCL8adD4Reub1vbbabbpLLEryLLCgdhchWlUFe43q6dI4en7mGJQkawy6VXADwtlQa5LEXByqq1Y+l7RI445TsI9sxiUCZGVma4F76QLVtI0kr9tBTNOsAjLgn1HWJbc3wF6kUyTGT07kRsYy3AsM9PO2VZOrrI+3hSJ/TkbcRBJLX0kk2Nqu6c8f6aONV9Mw/lSRnEq6+a543zvxp6Hq1A9tWhrjH51wej7ZTFDM2yjBGphu9SlydRxta9ad+0+0kTebXxPKBtnRj4bufyn/wBLHHspjXBl03XjURLeLjwpNtANrt44EYuEFizYknMse81Y1yPD8qiob2wwP2VSb8adWtgcuNCQi+FB3LFTjnRA1NYnE1a+m2OB4VSM+2vZ5BvINvudrLstwnqQTo0csdyLowswuLGrItOkKMLCwHYKrbVc6s6OPAUGXYdE6f0uebc7NZEl3GMxeaWQMTbxFZXYX8IF7ZYU3UOkdP6oUbe7cSMgKBwzoSjeaNjGylkPFThWoeoM8e+o3qWxGHZQZ59ls9xtk2TxL+mjMZWMeFVMTB49Oi1gpUVP08A3L7tUA3EiLFJIM2RSWUHuLG1Xfdx5UuFBzD7e6W25fdhJVmllM8mjcTorSEgljGkgTG2OFdLc7Pb7yBtruU9SJirMtyBdGEinw2yZQasULfwk351YOygViVF8yaRACSxouThdacW04Zc6gzLsdrHFuIEiX0t20j7lcSHaUWkJ/wAwqrd9I6fv9lH07cxk7WLQY4kkeO3peTxRsreG3Ot3h4fZStf/AGzoM+y6ds9hG8W0i0LI2uVizO7tbTqd3LMxsOJrJs/bvR+nzLPttuRIjM6NJLLKFeS+t0EruFZrm5GNdTxWwoHIcuNBU22gfcxb1kvuYUeOKS58KyFS4te2OgVU3TNi2z3HTzCBtN0ZDPCpIDGYlpMjcaieFbO6pRWDY9H2PTpDLtRNqZdB9WeaYWvfBZpHAyzobjovSt1uxvdxtw840FjqYK5jxjMkasEcr9OoG1b8e6gLUGfe7La9ShEG7i9RAwdTdkZXGTo6FWVhzBo7TZ7bYbddttYxFCpJCC5xY6mYk3JJOJJrRjwFDDjnQZodjtYN3ud9BEF3O8EY3Mtz4xECseBNhYHhWi3xo1DfgKCtszVZYnBBft4UX8x15URa2GVZUoTG7YmiTwGdQ3yGHbQvYeEXoAbLiczSG7HSMAczTcccW/2yqL2DHjQMAALDKgSBmahvxPypfDx+AoJctkMKNj3d1Gob8qilIA7TUAtU+00PEeygJIGdKLnG1uV6Hhv2U9AtueNC4vf5Co2ojkONQWt4aCWJzwHKlvmclGFFr2uc+VVuTYYXFBLa8Tgo4VAdROkYZXqZ2Lmw4AU4yGkX5UChQMPmaP8AFwGVDG1hhzNQ2wve1RU1AAnM1BqA5UTw5VGJtgKABQBjjQBAFhUN7Y/IVBawsKAC5JvgKBIBFvnR4nV8qVycCRhfAUCOxsbcOJpYYwLE4m+PfTN5e3jTL2C+NA3091BjkRjU56sqI8tQAgnE8OFQkWsMamJGOAoLl4RccKKFrghs6oxYY91XnPm1UG92w4m5ohkGs3/D9tWE6ss+fKlTyjT8abD6fiKKIw/bS2OYy5UT24cqhvbLvNQKwEg7sj21T5s8CMKuNuHxqmTzkgY2xFAVxLFsuFMtwzNnlSpllhfGit7m2V8qBhZs+80CDqsMQKOH+qgt7m4xoJe7AHC1Qi96PE3pBxtRVWmzsRnf7qDE31cjnTnjh4r4Uslr4f6hRFxNxjiDxqAYYGq4y2nLDhTjszqKgNib1DbUDzwqAnUbjhQe2Fs74WoGVfzFI51sxtzrIl9a99axllViUL4Y0jRRyPG7rd4SWjPIsCp+w0/GgbXwzrSA6LIjRSC6OCrDmCLEVV+nhRomCC8KlIm4qpAGn7KuN6l8MRQZ9xstvuZI5ZlYvCbxkOy2POykVcyq1wwzwPbeiMscuBqd4woESMQosUYAjQBUUZADAAVWNvtv06bQxAQRlSkeNgVOpT88avxHaKBtUCTQxbhdEo1AMr2yxUhlOHIiq5UDq0bi6OCrDmrCxq7/AGxpH0kcQeFBVttltduQ8IcEDSA0kjgD/K7EVe63GGfCq0Lg4C441bjwFKM0kSS6RIt9DB17GXI0VijWV5Qtne2sjjpyq1iPqGNISb4g3oMkfTtpA6vCrrpJIHqSFcf4S2n7K0mGObSso1BWV1GXiU3U4UwtfHAUMj4caupoZ3sbCgrae6kJwyNQE8u+op5CCcM+dBQWNqAxzwFWLa3hyoP/2Q==';
	}

		var walletHtml =
							"<div class='artwallet' id='artwallet" + i + "'>" +
		//"<iframe src='bitcoin-wallet-01.svg' id='papersvg" + i + "' class='papersvg' ></iframe>" +
								"<img id='papersvg" + i + "' class='papersvg' src='" + image + "' />" +
								"<div id='qrcode_public" + i + "' class='qrcode_public'></div>" +
								"<div id='qrcode_private" + i + "' class='qrcode_private'></div>" +
								"<div class='btcaddress' id='btcaddress" + i + "'></div>" +
								"<div class='" + keyelement + "' id='" + keyelement + i + "'></div>" +
							"</div>";
		return walletHtml;
	},

	showArtisticWallet: function (idPostFix, bitcoinAddress, privateKey) {
		var keyValuePair = {};
		keyValuePair["qrcode_public" + idPostFix] = bitcoinAddress;
		keyValuePair["qrcode_private" + idPostFix] = privateKey;
		ninja.qrCode.showQrCode(keyValuePair, 2.5);
		document.getElementById("btcaddress" + idPostFix).innerHTML = bitcoinAddress;

		if (ninja.wallets.paperwallet.encrypt) {
			var half = privateKey.length / 2;
			document.getElementById("btcencryptedkey" + idPostFix).innerHTML = privateKey.slice(0, half) + '<br />' + privateKey.slice(half);
		}
		else {
			document.getElementById("btcprivwif" + idPostFix).innerHTML = privateKey;
		}

		// CODE to modify SVG DOM elements
		//var paperSvg = document.getElementById("papersvg" + idPostFix);
		//if (paperSvg) {
		//	svgDoc = paperSvg.contentDocument;
		//	if (svgDoc) {
		//		var bitcoinAddressElement = svgDoc.getElementById("bitcoinaddress");
		//		var privateKeyElement = svgDoc.getElementById("privatekey");
		//		if (bitcoinAddressElement && privateKeyElement) {
		//			bitcoinAddressElement.textContent = bitcoinAddress;
		//			privateKeyElement.textContent = privateKeyWif;
		//		}
		//	}
		//}
	},

	toggleArt: function (element) {
		ninja.wallets.paperwallet.resetLimits();
	},

	toggleEncrypt: function (element) {
		// enable/disable passphrase textbox
		document.getElementById("paperpassphrase").disabled = !element.checked;
		ninja.wallets.paperwallet.encrypt = element.checked;
		ninja.wallets.paperwallet.resetLimits();
	},

	resetLimits: function () {
		var hideArt = document.getElementById("paperart");
		var paperEncrypt = document.getElementById("paperencrypt");
		var limit;
		var limitperpage;

		document.getElementById("paperkeyarea").style.fontSize = "100%";
		if (!hideArt.checked) {
			limit = ninja.wallets.paperwallet.pageBreakAtArtisticDefault;
			limitperpage = ninja.wallets.paperwallet.pageBreakAtArtisticDefault;
		}
		else if (hideArt.checked && paperEncrypt.checked) {
			limit = ninja.wallets.paperwallet.pageBreakAtDefault;
			limitperpage = ninja.wallets.paperwallet.pageBreakAtDefault;
			// reduce font size
			document.getElementById("paperkeyarea").style.fontSize = "95%";
		}
		else if (hideArt.checked && !paperEncrypt.checked) {
			limit = ninja.wallets.paperwallet.pageBreakAtDefault;
			limitperpage = ninja.wallets.paperwallet.pageBreakAtDefault;
		}
		document.getElementById("paperlimitperpage").value = limitperpage;
		document.getElementById("paperlimit").value = limit;
	}
};

(function (ninja) {
	var ut = ninja.unitTests = {
		runSynchronousTests: function () {
			document.getElementById("busyblock").className = "busy";
			var div = document.createElement("div");
			div.setAttribute("class", "unittests");
			div.setAttribute("id", "unittests");
			var testResults = "";
			var passCount = 0;
			var testCount = 0;
			for (var test in ut.synchronousTests) {
				var exceptionMsg = "";
				var resultBool = false;
				try {
					resultBool = ut.synchronousTests[test]();
				} catch (ex) {
					exceptionMsg = ex.toString();
					resultBool = false;
				}
				if (resultBool == true) {
					var passFailStr = "pass";
					passCount++;
				}
				else {
					var passFailStr = "<b>FAIL " + exceptionMsg + "</b>";
				}
				testCount++;
				testResults += test + ": " + passFailStr + "<br/>";
			}
			testResults += passCount + " of " + testCount + " synchronous tests passed";
			if (passCount < testCount) {
				testResults += "<b>" + (testCount - passCount) + " unit test(s) failed</b>";
			}
			div.innerHTML = "<h3>Unit Tests</h3><div id=\"unittestresults\">" + testResults + "<br/><br/></div>";
			document.body.appendChild(div);
			document.getElementById("busyblock").className = "";

		},

		runAsynchronousTests: function () {
			var div = document.createElement("div");
			div.setAttribute("class", "unittests");
			div.setAttribute("id", "asyncunittests");
			div.innerHTML = "<h3>Async Unit Tests</h3><div id=\"asyncunittestresults\"></div><br/><br/><br/><br/>";
			document.body.appendChild(div);

			// run the asynchronous tests one after another so we don't crash the browser
			ninja.foreachSerialized(ninja.unitTests.asynchronousTests, function (name, cb) {
				document.getElementById("busyblock").className = "busy";
				ninja.unitTests.asynchronousTests[name](cb);
			}, function () {
				document.getElementById("asyncunittestresults").innerHTML += "running of asynchronous unit tests complete!<br/>";
				document.getElementById("busyblock").className = "";
			});
		},

		synchronousTests: {
			//ninja.publicKey tests
			testIsPublicKeyHexFormat: function () {
				var key = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var bool = ninja.publicKey.isPublicKeyHexFormat(key);
				if (bool != true) {
					return false;
				}
				return true;
			},
			testGetHexFromByteArray: function () {
				var bytes = [4, 120, 152, 47, 64, 250, 12, 11, 122, 85, 113, 117, 131, 175, 201, 154, 78, 223, 211, 1, 162, 114, 157, 197, 155, 11, 142, 185, 225, 134, 146, 188, 181, 33, 240, 84, 250, 217, 130, 175, 76, 193, 147, 58, 253, 31, 27, 86, 62, 167, 121, 166, 170, 108, 206, 54, 163, 11, 148, 125, 214, 83, 230, 62, 68];
				var key = ninja.publicKey.getHexFromByteArray(bytes);
				if (key != "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44") {
					return false;
				}
				return true;
			},
			testHexToBytes: function () {
				var key = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var bytes = Crypto.util.hexToBytes(key);
				if (bytes.toString() != "4,120,152,47,64,250,12,11,122,85,113,117,131,175,201,154,78,223,211,1,162,114,157,197,155,11,142,185,225,134,146,188,181,33,240,84,250,217,130,175,76,193,147,58,253,31,27,86,62,167,121,166,170,108,206,54,163,11,148,125,214,83,230,62,68") {
					return false;
				}
				return true;
			},
			testGetBitcoinAddressFromByteArray: function () {
				var bytes = [4, 120, 152, 47, 64, 250, 12, 11, 122, 85, 113, 117, 131, 175, 201, 154, 78, 223, 211, 1, 162, 114, 157, 197, 155, 11, 142, 185, 225, 134, 146, 188, 181, 33, 240, 84, 250, 217, 130, 175, 76, 193, 147, 58, 253, 31, 27, 86, 62, 167, 121, 166, 170, 108, 206, 54, 163, 11, 148, 125, 214, 83, 230, 62, 68];
				var address = ninja.publicKey.getBitcoinAddressFromByteArray(bytes);
				if (address != "1Cnz9ULjzBPYhDw1J8bpczDWCEXnC9HuU1") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromAdding: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "0419153E53FECAD7FF07FEC26F7DDEB1EDD66957711AA4554B8475F10AFBBCD81C0159DC0099AD54F733812892EB9A11A8C816A201B3BAF0D97117EBA2033C9AB2";
				var bytes = ninja.publicKey.getByteArrayFromAdding(key1, key2);
				if (bytes.toString() != "4,151,19,227,152,54,37,184,255,4,83,115,216,102,189,76,82,170,57,4,196,253,2,41,74,6,226,33,167,199,250,74,235,223,128,233,99,150,147,92,57,39,208,84,196,71,68,248,166,106,138,95,172,253,224,70,187,65,62,92,81,38,253,79,0") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromAddingCompressed: function () {
				var key1 = "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5";
				var key2 = "0219153E53FECAD7FF07FEC26F7DDEB1EDD66957711AA4554B8475F10AFBBCD81C";
				var bytes = ninja.publicKey.getByteArrayFromAdding(key1, key2);
				var hex = ninja.publicKey.getHexFromByteArray(bytes);
				if (hex != "029713E3983625B8FF045373D866BD4C52AA3904C4FD02294A06E221A7C7FA4AEB") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromAddingUncompressedAndCompressed: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "0219153E53FECAD7FF07FEC26F7DDEB1EDD66957711AA4554B8475F10AFBBCD81C";
				var bytes = ninja.publicKey.getByteArrayFromAdding(key1, key2);
				if (bytes.toString() != "4,151,19,227,152,54,37,184,255,4,83,115,216,102,189,76,82,170,57,4,196,253,2,41,74,6,226,33,167,199,250,74,235,223,128,233,99,150,147,92,57,39,208,84,196,71,68,248,166,106,138,95,172,253,224,70,187,65,62,92,81,38,253,79,0") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromAddingShouldReturnNullWhenSameKey1: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var bytes = ninja.publicKey.getByteArrayFromAdding(key1, key2);
				if (bytes != null) {
					return false;
				}
				return true;
			},
			testGetByteArrayFromAddingShouldReturnNullWhenSameKey2: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5";
				var bytes = ninja.publicKey.getByteArrayFromAdding(key1, key2);
				if (bytes != null) {
					return false;
				}
				return true;
			},
			testGetByteArrayFromMultiplying: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "SQE6yipP5oW8RBaStWoB47xsRQ8pat";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(key1, new Bitcoin.ECKey(key2));
				if (bytes.toString() != "4,102,230,163,180,107,9,21,17,48,35,245,227,110,199,119,144,57,41,112,64,245,182,40,224,41,230,41,5,26,206,138,57,115,35,54,105,7,180,5,106,217,57,229,127,174,145,215,79,121,163,191,211,143,215,50,48,156,211,178,72,226,68,150,52") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromMultiplyingCompressedOutputsUncompressed: function () {
				var key1 = "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5";
				var key2 = "SQE6yipP5oW8RBaStWoB47xsRQ8pat";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(key1, new Bitcoin.ECKey(key2));
				if (bytes.toString() != "4,102,230,163,180,107,9,21,17,48,35,245,227,110,199,119,144,57,41,112,64,245,182,40,224,41,230,41,5,26,206,138,57,115,35,54,105,7,180,5,106,217,57,229,127,174,145,215,79,121,163,191,211,143,215,50,48,156,211,178,72,226,68,150,52") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromMultiplyingCompressedOutputsCompressed: function () {
				var key1 = "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5";
				var key2 = "L1n4cgNZAo2KwdUc15zzstvo1dcxpBw26NkrLqfDZtU9AEbPkLWu";
				var ecKey = new Bitcoin.ECKey(key2);
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(key1, ecKey);
				if (bytes.toString() != "2,102,230,163,180,107,9,21,17,48,35,245,227,110,199,119,144,57,41,112,64,245,182,40,224,41,230,41,5,26,206,138,57") {
					return false;
				}
				return true;
			},
			testGetByteArrayFromMultiplyingShouldReturnNullWhenSameKey1: function () {
				var key1 = "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44";
				var key2 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(key1, new Bitcoin.ECKey(key2));
				if (bytes != null) {
					return false;
				}
				return true;
			},
			testGetByteArrayFromMultiplyingShouldReturnNullWhenSameKey2: function () {
				var key1 = "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5";
				var key2 = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(key1, new Bitcoin.ECKey(key2));
				if (bytes != null) {
					return false;
				}
				return true;
			},
			// confirms multiplication is working and BigInteger was created correctly (Pub Key B vs Priv Key A)
			testGetPubHexFromMultiplyingPrivAPubB: function () {
				var keyPub = "04F04BF260DCCC46061B5868F60FE962C77B5379698658C98A93C3129F5F98938020F36EBBDE6F1BEAF98E5BD0E425747E68B0F2FB7A2A59EDE93F43C0D78156FF";
				var keyPriv = "B1202A137E917536B3B4C5010C3FF5DDD4784917B3EEF21D3A3BF21B2E03310C";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(keyPub, new Bitcoin.ECKey(keyPriv));
				var pubHex = ninja.publicKey.getHexFromByteArray(bytes);
				if (pubHex != "04C6732006AF4AE571C7758DF7A7FB9E3689DFCF8B53D8724D3A15517D8AB1B4DBBE0CB8BB1C4525F8A3001771FC7E801D3C5986A555E2E9441F1AD6D181356076") {
					return false;
				}
				return true;
			},
			// confirms multiplication is working and BigInteger was created correctly (Pub Key A vs Priv Key B)
			testGetPubHexFromMultiplyingPrivBPubA: function () {
				var keyPub = "0429BF26C0AF7D31D608474CEBD49DA6E7C541B8FAD95404B897643476CE621CFD05E24F7AE8DE8033AADE5857DB837E0B704A31FDDFE574F6ECA879643A0D3709";
				var keyPriv = "7DE52819F1553C2BFEDE6A2628B6FDDF03C2A07EB21CF77ACA6C2C3D252E1FD9";
				var bytes = ninja.publicKey.getByteArrayFromMultiplying(keyPub, new Bitcoin.ECKey(keyPriv));
				var pubHex = ninja.publicKey.getHexFromByteArray(bytes);
				if (pubHex != "04C6732006AF4AE571C7758DF7A7FB9E3689DFCF8B53D8724D3A15517D8AB1B4DBBE0CB8BB1C4525F8A3001771FC7E801D3C5986A555E2E9441F1AD6D181356076") {
					return false;
				}
				return true;
			},

			// Private Key tests
			testBadKeyIsNotWif: function () {
				return !(Bitcoin.ECKey.isWalletImportFormat("bad key"));
			},
			testBadKeyIsNotWifCompressed: function () {
				return !(Bitcoin.ECKey.isCompressedWalletImportFormat("bad key"));
			},
			testBadKeyIsNotHex: function () {
				return !(Bitcoin.ECKey.isHexFormat("bad key"));
			},
			testBadKeyIsNotBase64: function () {
				return !(Bitcoin.ECKey.isBase64Format("bad key"));
			},
			testBadKeyIsNotMini: function () {
				return !(Bitcoin.ECKey.isMiniFormat("bad key"));
			},
			testBadKeyReturnsNullPrivFromECKey: function () {
				var key = "bad key";
				var ecKey = new Bitcoin.ECKey(key);
				if (ecKey.priv != null) {
					return false;
				}
				return true;
			},
			testGetBitcoinPrivateKeyByteArray: function () {
				var key = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var bytes = [41, 38, 101, 195, 135, 36, 24, 173, 241, 218, 127, 250, 58, 100, 111, 47, 6, 2, 36, 109, 166, 9, 138, 145, 210, 41, 195, 33, 80, 242, 113, 139];
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinPrivateKeyByteArray().toString() != bytes.toString()) {
					return false;
				}
				return true;
			},
			testECKeyDecodeWalletImportFormat: function () {
				var key = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var bytes1 = [41, 38, 101, 195, 135, 36, 24, 173, 241, 218, 127, 250, 58, 100, 111, 47, 6, 2, 36, 109, 166, 9, 138, 145, 210, 41, 195, 33, 80, 242, 113, 139];
				var bytes2 = Bitcoin.ECKey.decodeWalletImportFormat(key);
				if (bytes1.toString() != bytes2.toString()) {
					return false;
				}
				return true;
			},
			testECKeyDecodeCompressedWalletImportFormat: function () {
				var key = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var bytes1 = [41, 38, 101, 195, 135, 36, 24, 173, 241, 218, 127, 250, 58, 100, 111, 47, 6, 2, 36, 109, 166, 9, 138, 145, 210, 41, 195, 33, 80, 242, 113, 139];
				var bytes2 = Bitcoin.ECKey.decodeCompressedWalletImportFormat(key);
				if (bytes1.toString() != bytes2.toString()) {
					return false;
				}
				return true;
			},
			testWifToPubKeyHex: function () {
				var key = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getPubKeyHex() != "0478982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB521F054FAD982AF4CC1933AFD1F1B563EA779A6AA6CCE36A30B947DD653E63E44"
						|| btcKey.getPubPoint().compressed != false) {
					return false;
				}
				return true;
			},
			testWifToPubKeyHexCompressed: function () {
				var key = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var btcKey = new Bitcoin.ECKey(key);
				btcKey.setCompressed(true);
				if (btcKey.getPubKeyHex() != "0278982F40FA0C0B7A55717583AFC99A4EDFD301A2729DC59B0B8EB9E18692BCB5"
						|| btcKey.getPubPoint().compressed != true) {
					return false;
				}
				return true;
			},
			testBase64ToECKey: function () {
				var key = "KSZlw4ckGK3x2n/6OmRvLwYCJG2mCYqR0inDIVDycYs=";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinBase64Format() != "KSZlw4ckGK3x2n/6OmRvLwYCJG2mCYqR0inDIVDycYs=") {
					return false;
				}
				return true;
			},
			testHexToECKey: function () {
				var key = "292665C3872418ADF1DA7FFA3A646F2F0602246DA6098A91D229C32150F2718B";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinHexFormat() != "292665C3872418ADF1DA7FFA3A646F2F0602246DA6098A91D229C32150F2718B") {
					return false;
				}
				return true;
			},
			testCompressedWifToECKey: function () {
				var key = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinWalletImportFormat() != "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S"
						|| btcKey.getPubPoint().compressed != true) {
					return false;
				}
				return true;
			},
			testWifToECKey: function () {
				var key = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinWalletImportFormat() != "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb") {
					return false;
				}
				return true;
			},
			testBrainToECKey: function () {
				var key = "bitaddress.org unit test";
				var bytes = Crypto.SHA256(key, { asBytes: true });
				var btcKey = new Bitcoin.ECKey(bytes);
				if (btcKey.getBitcoinWalletImportFormat() != "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb") {
					return false;
				}
				return true;
			},
			testMini30CharsToECKey: function () {
				var key = "SQE6yipP5oW8RBaStWoB47xsRQ8pat";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinWalletImportFormat() != "5JrBLQseeZdYw4jWEAHmNxGMr5fxh9NJU3fUwnv4khfKcg2rJVh") {
					return false;
				}
				return true;
			},
			testGetECKeyFromAdding: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "SQE6yipP5oW8RBaStWoB47xsRQ8pat";
				var ecKey = ninja.privateKey.getECKeyFromAdding(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "5KAJTSqSjpsZ11KyEE3qu5PrJVjR4ZCbNxK3Nb1F637oe41m1c2") {
					return false;
				}
				return true;
			},
			testGetECKeyFromAddingCompressed: function () {
				var key1 = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var key2 = "L1n4cgNZAo2KwdUc15zzstvo1dcxpBw26NkrLqfDZtU9AEbPkLWu";
				var ecKey = ninja.privateKey.getECKeyFromAdding(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "L3A43j2pc2J8F2SjBNbYprPrcDpDCh8Aju8dUH65BEM2r7RFSLv4") {
					return false;
				}
				return true;
			},
			testGetECKeyFromAddingUncompressedAndCompressed: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "L1n4cgNZAo2KwdUc15zzstvo1dcxpBw26NkrLqfDZtU9AEbPkLWu";
				var ecKey = ninja.privateKey.getECKeyFromAdding(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "5KAJTSqSjpsZ11KyEE3qu5PrJVjR4ZCbNxK3Nb1F637oe41m1c2") {
					return false;
				}
				return true;
			},
			testGetECKeyFromAddingShouldReturnNullWhenSameKey1: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var ecKey = ninja.privateKey.getECKeyFromAdding(key1, key2);
				if (ecKey != null) {
					return false;
				}
				return true;
			},
			testGetECKeyFromAddingShouldReturnNullWhenSameKey2: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var ecKey = ninja.privateKey.getECKeyFromAdding(key1, key2);
				if (ecKey != null) {
					return false;
				}
				return true;
			},
			testGetECKeyFromMultiplying: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "SQE6yipP5oW8RBaStWoB47xsRQ8pat";
				var ecKey = ninja.privateKey.getECKeyFromMultiplying(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "5KetpZ5mCGagCeJnMmvo18n4iVrtPSqrpnW5RP92Gv2BQy7GPCk") {
					return false;
				}
				return true;
			},
			testGetECKeyFromMultiplyingCompressed: function () {
				var key1 = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var key2 = "L1n4cgNZAo2KwdUc15zzstvo1dcxpBw26NkrLqfDZtU9AEbPkLWu";
				var ecKey = ninja.privateKey.getECKeyFromMultiplying(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "L5LFitc24jme2PfVChJS3bKuQAPBp54euuqLWciQdF2CxnaU3M8t") {
					return false;
				}
				return true;
			},
			testGetECKeyFromMultiplyingUncompressedAndCompressed: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "L1n4cgNZAo2KwdUc15zzstvo1dcxpBw26NkrLqfDZtU9AEbPkLWu";
				var ecKey = ninja.privateKey.getECKeyFromMultiplying(key1, key2);
				if (ecKey.getBitcoinWalletImportFormat() != "5KetpZ5mCGagCeJnMmvo18n4iVrtPSqrpnW5RP92Gv2BQy7GPCk") {
					return false;
				}
				return true;
			},
			testGetECKeyFromMultiplyingShouldReturnNullWhenSameKey1: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var ecKey = ninja.privateKey.getECKeyFromMultiplying(key1, key2);
				if (ecKey != null) {
					return false;
				}
				return true;
			},
			testGetECKeyFromMultiplyingShouldReturnNullWhenSameKey2: function () {
				var key1 = "5J8QhiQtAiozKwyk3GCycAscg1tNaYhNdiiLey8vaDK8Bzm4znb";
				var key2 = "KxbhchnQquYQ2dfSxz7rrEaQTCukF4uCV57TkamyTbLzjFWcdi3S";
				var ecKey = ninja.privateKey.getECKeyFromMultiplying(key1, key2);
				if (ecKey != null) {
					return false;
				}
				return true;
			},
			testGetECKeyFromBase6Key: function () {
				var baseKey = "100531114202410255230521444145414341221420541210522412225005202300434134213212540304311321323051431";
				var hexKey = "292665C3872418ADF1DA7FFA3A646F2F0602246DA6098A91D229C32150F2718B";
				var ecKey = new Bitcoin.ECKey(baseKey);
				if (ecKey.getBitcoinHexFormat() != hexKey) {
					return false;
				}
				return true;
			},

			// EllipticCurve tests
			testDecodePointEqualsDecodeFrom: function () {
				var key = "04F04BF260DCCC46061B5868F60FE962C77B5379698658C98A93C3129F5F98938020F36EBBDE6F1BEAF98E5BD0E425747E68B0F2FB7A2A59EDE93F43C0D78156FF";
				var ecparams = EllipticCurve.getSECCurveByName("secp256k1");
				var ecPoint1 = EllipticCurve.PointFp.decodeFrom(ecparams.getCurve(), Crypto.util.hexToBytes(key));
				var ecPoint2 = ecparams.getCurve().decodePointHex(key);
				if (!ecPoint1.equals(ecPoint2)) {
					return false;
				}
				return true;
			},
			testDecodePointHexForCompressedPublicKey: function () {
				var key = "03F04BF260DCCC46061B5868F60FE962C77B5379698658C98A93C3129F5F989380";
				var pubHexUncompressed = ninja.publicKey.getDecompressedPubKeyHex(key);
				if (pubHexUncompressed != "04F04BF260DCCC46061B5868F60FE962C77B5379698658C98A93C3129F5F98938020F36EBBDE6F1BEAF98E5BD0E425747E68B0F2FB7A2A59EDE93F43C0D78156FF") {
					return false;
				}
				return true;
			},
			// old bugs
			testBugWithLeadingZeroBytePublicKey: function () {
				var key = "5Je7CkWTzgdo1RpwjYhwnVKxQXt8EPRq17WZFtWcq5umQdsDtTP";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinAddress() != "1M6dsMZUjFxjdwsyVk8nJytWcfr9tfUa9E") {
					return false;
				}
				return true;
			},
			testBugWithLeadingZeroBytePrivateKey: function () {
				var key = "0004d30da67214fa65a41a6493576944c7ea86713b14db437446c7a8df8e13da";
				var btcKey = new Bitcoin.ECKey(key);
				if (btcKey.getBitcoinAddress() != "1NAjZjF81YGfiJ3rTKc7jf1nmZ26KN7Gkn") {
					return false;
				}
				return true;
			}
		},

		asynchronousTests: {
			//https://en.bitcoin.it/wiki/BIP_0038
			testBip38: function (done) {
				var tests = [
				//No compression, no EC multiply
					["6PRVWUbkzzsbcVac2qwfssoUJAN1Xhrg6bNk8J7Nzm5H7kxEbn2Nh2ZoGg", "TestingOneTwoThree", "5KN7MzqK5wt2TP1fQCYyHBtDrXdJuXbUzm4A9rKAteGu3Qi5CVR"],
					["6PRNFFkZc2NZ6dJqFfhRoFNMR9Lnyj7dYGrzdgXXVMXcxoKTePPX1dWByq", "Satoshi", "5HtasZ6ofTHP6HCwTqTkLDuLQisYPah7aUnSKfC7h4hMUVw2gi5"],
				//Compression, no EC multiply
					["6PYNKZ1EAgYgmQfmNVamxyXVWHzK5s6DGhwP4J5o44cvXdoY7sRzhtpUeo", "TestingOneTwoThree", "L44B5gGEpqEDRS9vVPz7QT35jcBG2r3CZwSwQ4fCewXAhAhqGVpP"],
					["6PYLtMnXvfG3oJde97zRyLYFZCYizPU5T3LwgdYJz1fRhh16bU7u6PPmY7", "Satoshi", "KwYgW8gcxj1JWJXhPSu4Fqwzfhp5Yfi42mdYmMa4XqK7NJxXUSK7"],
				//EC multiply, no compression, no lot/sequence numbers
					["6PfQu77ygVyJLZjfvMLyhLMQbYnu5uguoJJ4kMCLqWwPEdfpwANVS76gTX", "TestingOneTwoThree", "5K4caxezwjGCGfnoPTZ8tMcJBLB7Jvyjv4xxeacadhq8nLisLR2"],
					["6PfLGnQs6VZnrNpmVKfjotbnQuaJK4KZoPFrAjx1JMJUa1Ft8gnf5WxfKd", "Satoshi", "5KJ51SgxWaAYR13zd9ReMhJpwrcX47xTJh2D3fGPG9CM8vkv5sH"],
				//EC multiply, no compression, lot/sequence numbers
					["6PgNBNNzDkKdhkT6uJntUXwwzQV8Rr2tZcbkDcuC9DZRsS6AtHts4Ypo1j", "MOLON LABE", "5JLdxTtcTHcfYcmJsNVy1v2PMDx432JPoYcBTVVRHpPaxUrdtf8"],
					["6PgGWtx25kUg8QWvwuJAgorN6k9FbE25rv5dMRwu5SKMnfpfVe5mar2ngH", Crypto.charenc.UTF8.bytesToString([206, 156, 206, 159, 206, 155, 206, 169, 206, 157, 32, 206, 155, 206, 145, 206, 146, 206, 149])/*UTF-8 characters, encoded in source so they don't get corrupted*/, "5KMKKuUmAkiNbA3DazMQiLfDq47qs8MAEThm4yL8R2PhV1ov33D"]];

				// running each test uses a lot of memory, which isn't freed
				// immediately, so give the VM a little time to reclaim memory
				function waitThenCall(callback) {
					return function () { setTimeout(callback, 10000); }
				}

				var decryptTest = function (test, i, onComplete) {
					ninja.privateKey.BIP38EncryptedKeyToByteArrayAsync(test[0], test[1], function (privBytes) {
						if (privBytes.constructor == Error) {
							document.getElementById("asyncunittestresults").innerHTML += "fail testDecryptBip38 #" + i + ", error: " + privBytes.message + "<br/>";
						} else {
							var btcKey = new Bitcoin.ECKey(privBytes);
							var wif = !test[2].substr(0, 1).match(/[LK]/) ? btcKey.setCompressed(false).getBitcoinWalletImportFormat() : btcKey.setCompressed(true).getBitcoinWalletImportFormat();
							if (wif != test[2]) {
								document.getElementById("asyncunittestresults").innerHTML += "fail testDecryptBip38 #" + i + "<br/>";
							} else {
								document.getElementById("asyncunittestresults").innerHTML += "pass testDecryptBip38 #" + i + "<br/>";
							}
						}
						onComplete();
					});
				};

				var encryptTest = function (test, compressed, i, onComplete) {
					ninja.privateKey.BIP38PrivateKeyToEncryptedKeyAsync(test[2], test[1], compressed, function (encryptedKey) {
						if (encryptedKey === test[0]) {
							document.getElementById("asyncunittestresults").innerHTML += "pass testBip38Encrypt #" + i + "<br/>";
						} else {
							document.getElementById("asyncunittestresults").innerHTML += "fail testBip38Encrypt #" + i + "<br/>";
							document.getElementById("asyncunittestresults").innerHTML += "expected " + test[0] + "<br/>received " + encryptedKey + "<br/>";
						}
						onComplete();
					});
				};

				// test randomly generated encryption-decryption cycle
				var cycleTest = function (i, compress, onComplete) {
					// create new private key
					var privKey = (new Bitcoin.ECKey(false)).getBitcoinWalletImportFormat();

					// encrypt private key
					ninja.privateKey.BIP38PrivateKeyToEncryptedKeyAsync(privKey, 'testing', compress, function (encryptedKey) {
						// decrypt encryptedKey
						ninja.privateKey.BIP38EncryptedKeyToByteArrayAsync(encryptedKey, 'testing', function (decryptedBytes) {
							var decryptedKey = (new Bitcoin.ECKey(decryptedBytes)).getBitcoinWalletImportFormat();

							if (decryptedKey === privKey) {
								document.getElementById("asyncunittestresults").innerHTML += "pass cycleBip38 test #" + i + "<br/>";
							}
							else {
								document.getElementById("asyncunittestresults").innerHTML += "fail cycleBip38 test #" + i + " " + privKey + "<br/>";
								document.getElementById("asyncunittestresults").innerHTML += "encrypted key: " + encryptedKey + "<br/>decrypted key: " + decryptedKey;
							}
							onComplete();
						});
					});
				};

				// intermediate test - create some encrypted keys from an intermediate
				// then decrypt them to check that the private keys are recoverable
				var intermediateTest = function (i, onComplete) {
					var pass = Math.random().toString(36).substr(2);
					ninja.privateKey.BIP38GenerateIntermediatePointAsync(pass, null, null, function (intermediatePoint) {
						ninja.privateKey.BIP38GenerateECAddressAsync(intermediatePoint, false, function (address, encryptedKey) {
							ninja.privateKey.BIP38EncryptedKeyToByteArrayAsync(encryptedKey, pass, function (privBytes) {
								if (privBytes.constructor == Error) {
									document.getElementById("asyncunittestresults").innerHTML += "fail testBip38Intermediate #" + i + ", error: " + privBytes.message + "<br/>";
								} else {
									var btcKey = new Bitcoin.ECKey(privBytes);
									var btcAddress = btcKey.getBitcoinAddress();
									if (address !== btcKey.getBitcoinAddress()) {
										document.getElementById("asyncunittestresults").innerHTML += "fail testBip38Intermediate #" + i + "<br/>";
									} else {
										document.getElementById("asyncunittestresults").innerHTML += "pass testBip38Intermediate #" + i + "<br/>";
									}
								}
								onComplete();
							});
						});
					});
				}

				document.getElementById("asyncunittestresults").innerHTML += "running " + tests.length + " tests named testDecryptBip38<br/>";
				document.getElementById("asyncunittestresults").innerHTML += "running 4 tests named testBip38Encrypt<br/>";
				document.getElementById("asyncunittestresults").innerHTML += "running 2 tests named cycleBip38<br/>";
				document.getElementById("asyncunittestresults").innerHTML += "running 5 tests named testBip38Intermediate<br/>";
				ninja.runSerialized([
					function (cb) {
						ninja.forSerialized(0, tests.length, function (i, callback) {
							decryptTest(tests[i], i, waitThenCall(callback));
						}, waitThenCall(cb));
					},
					function (cb) {
						ninja.forSerialized(0, 4, function (i, callback) {
							// only first 4 test vectors are not EC-multiply,
							// compression param false for i = 1,2 and true for i = 3,4
							encryptTest(tests[i], i >= 2, i, waitThenCall(callback));
						}, waitThenCall(cb));
					},
					function (cb) {
						ninja.forSerialized(0, 2, function (i, callback) {
							cycleTest(i, i % 2 ? true : false, waitThenCall(callback));
						}, waitThenCall(cb));
					},
					function (cb) {
						ninja.forSerialized(0, 5, function (i, callback) {
							intermediateTest(i, waitThenCall(callback));
						}, cb);
					}
				], done);
			}
		}
	};
})(ninja);
// run unit tests
if (ninja.getQueryString()["unittests"] == "true" || ninja.getQueryString()["unittests"] == "1") {
	ninja.unitTests.runSynchronousTests();
	ninja.translator.showEnglishJson();
}
// run async unit tests
if (ninja.getQueryString()["asyncunittests"] == "true" || ninja.getQueryString()["asyncunittests"] == "1") {
	ninja.unitTests.runAsynchronousTests();
}
// change language
if (ninja.getQueryString()["culture"] != undefined) {
	ninja.translator.translate(ninja.getQueryString()["culture"]);
}
// testnet, check if testnet edition should be activated
if (ninja.getQueryString()["testnet"] == "true" || ninja.getQueryString()["testnet"] == "1") {
	document.getElementById("testnet").innerHTML = ninja.translator.get("testneteditionactivated");
	document.getElementById("testnet").style.display = "block";
	document.getElementById("detailwifprefix").innerHTML = "'9'";
	document.getElementById("detailcompwifprefix").innerHTML = "'c'";
	Bitcoin.Address.networkVersion = 0x6F; // testnet
	Bitcoin.ECKey.privateKeyPrefix = 0xEF; // testnet
	ninja.testnetMode = true;
}
// if users does not move mouse after random amount of time then generate the key anyway.
setTimeout(ninja.seeder.forceGenerate, ninja.seeder.seedLimit * 20);
