class PaymentRequestsController < ApplicationController
  # before_action :set_payment_request, only: [:show, :status]
  # before_action :set_request_struct, only: [:show, :status]

  # GET /payment_requests
  # def index
  # end

  # # GET /payment_requests/1
  # def show
  #   if @request_struct.state == "PENDING"
  #     @qr = RQRCode::QRCode.new(@request_struct.payment_qr_link, size: 6)
  #   else
  #     redirect_to payment_request_status_path(btc_address: @payment_request.btc_address)
  #   end
  # end

  # GET /payment_requests/new
  def new
    @payment_request = PaymentRequest.new
  end

  def create
    @payment_request = PaymentRequest.find_or_initialize_by(payment_request_params)
    if @payment_request.new_record?
      ## Creating a new bop payment_request and save the importand values in
      #  @payment_request
      bop_api_call = BitsOfProof::Payment.init(@payment_request.btc_address)
      @payment_request.bop_payment_id = bop_api_call["id"]
    end
    if @payment_request.save
      redirect_to  status_request_url(@payment_request.btc_address)
    else
      render action: 'new'
    end
  end

  def status
    unless @payment_request
      redirect_to root_url(@paymen_request), notice: "Your paperwallet address is invalid"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_payment_request
    #   @payment_request = PaymentRequest.find_by(btc_address: params[:btc_address])
    # end

    # def set_request_struct
    #   if @payment_request
    #     @request_struct = PaymentRequest.get_payment_request_struct(@payment_request.bop_payment_id)
    #   end
    # end

    # Only allow a trusted parameter "white list" through.
    def payment_request_params
      params.require(:payment_request).permit(:btc_address)
    end
end
