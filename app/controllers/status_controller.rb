class StatusController < ApplicationController
  before_action :set_payment_request, only: [:show]
  before_action :set_verify_payment_request, only: [ :verify ]
  before_action :set_request_struct, only: [:show, :verify]

  def show
    if @request_struct.state == "PENDING"
      @qr = RQRCode::QRCode.new(@request_struct.payment_qr_link, size: 6)
    end
  end

  def verify
    if @payment_request
      redirect_to status_request_url(btc_address: @payment_request.btc_address)
    else
      redirect_to status_index_url, notice: "Your ticket address is invalid"
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_request
      @payment_request = PaymentRequest.find_by(btc_address: params[:btc_address])
    end

    def set_verify_payment_request
      @payment_request = PaymentRequest.find_by(verify_params)
    end

    def set_request_struct
      if @payment_request
        @request_struct = PaymentRequest.get_payment_request_struct(@payment_request.bop_payment_id)
      end
    end

    def verify_params
      params.require(:payment_request).permit(:btc_address)
    end
end

