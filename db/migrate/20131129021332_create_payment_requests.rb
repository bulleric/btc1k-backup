class CreatePaymentRequests < ActiveRecord::Migration
  def change
    create_table :payment_requests do |t|
      t.string :btc_address

      t.timestamps
    end
  end
end
