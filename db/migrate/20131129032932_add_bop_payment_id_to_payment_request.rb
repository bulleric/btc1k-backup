class AddBopPaymentIdToPaymentRequest < ActiveRecord::Migration
  def change
    add_column :payment_requests, :bop_payment_id, :string
  end
end
